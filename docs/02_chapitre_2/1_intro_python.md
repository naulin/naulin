---
author: Votre nom
title: Une Histoire de l'informatique
---

Cette rubrique se veut transversale et se déclinera dans chacune des sept autres.
Au fur et à mesure des chapitres nous découvrirons les événements clés de
l’histoire de l’informatique. 
L'objectif final est d'être capable de situer dans le temps les
principaux événements de l’histoire de l’informatique et leurs protagonistes.

Pour commencer une vidéo en deux parties sur les événements marquants :

|[![texte alternatif de l'image](http://img.youtube.com/vi/dJdiSN9q5QE/0.jpg)](https://www.youtube.com/watch?v=dJdiSN9q5QE "Titre de la video") | [![texte alternatif de l'image](http://img.youtube.com/vi/NNxAKALRePo/0.jpg)](https://www.youtube.com/watch?v=NNxAKALRePo "Titre de la video") |
|----------- |  ----------|
|Première partie | Deuxième partie |

## Introduction :

Le mot informatique a été créé en 1962 par Philippe Dreyfus. Il s’agit d’un néologisme de la langue française fait de la contraction des deux mots “automatique” et “information”. Pour parler du traitement automatique de l’information, les anglo-saxons utilisent les termes de “computer science” ou de “data-processing”.

## I. Paragraphe 1 : 

texte 1

### 1. Sous paragraphe 1

Texte 1.1

### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 : 

texte 1

### 1. Sous paragraphe 1

Texte 2.1

### 2. Sous paragraphe 2

Texte 2.2
