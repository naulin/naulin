---
author: Jean-Marc Naulin
title: Représentation des données, types construits 
---
!!! info "Objectifs du cours :"

    * Définir la notion de liste et de p-uplet.
    * Identifier les différences entre une liste et un p-uplet.


Points clés

* Une liste est un objet qui peut contenir d’autres objets de n’importe quel type.
* On repère un objet à l’intérieur d’une liste par rapport à sa place, en n’oubliant pas qu’en informatique le premier est à l’indice 0.
* Un p-uplet est une liste particulière.
* Une liste est une séquence modifiable tandis qu’un p-uplet est une séquence non modifiable.


Prérequis pour bien comprendre

Types de base : entiers, nombres flottants, caractères

## I Les listes

!!! tip "Listes : "
    Une liste est un objet qui peut contenir différents objets de n’importe quel type. On repère un objet à l’intérieur d’une liste par rapport à sa place, en n’oubliant pas qu’en informatique le premier est à l’indice 0.


Remarque : en utilisant le langage Python, il faut utiliser les [ ] pour définir une liste.

Exemples :

* Liste des notes d’un élève : [10, 11.5, 13.1]
* Liste de courses : Course=[“tomates”, ”salade”, ”beurre”]
* Course contient 3 éléments, on dit que la liste est de longueur 3. Course[0]=”tomates”
* Liste composite : [2,10,”salade”,[11,12]]

Remarque :

Pour séparer les éléments, on utilise une virgule. Pour écrire un nombre décimal, on utilise l’écriture à l'anglo-saxonne en utilisant un point.
Pour reconnaitre un caractère ou une chaine de caractères, il faut utiliser les “ ” en Python.


!!! warning "À retenir : "
    Une liste est un objet modifiable.


Utilisation :

Un des gros avantages d'une liste est que vous pouvez appeler ses éléments par leur position. Ce numéro est appelé indice (ou index) de la liste.

Soyez très attentif au fait que les indices d'une liste de n éléments commencent à 0 et se terminent à n-1. Voyez l'exemple suivant :

???+ question "Tester le programme"

    {{ IDE('listeutil') }}

Par conséquent, si on appelle l'élément d'indice 4 de notre liste, Python renverra un message d'erreur :

???+ question "Tester le programme"

    {{ IDE('listeerreur') }}

N'oubliez pas ceci ou vous risquez d'obtenir des bugs inattendus !

Opération sur les listes :

Tout comme les chaînes de caractères, les listes supportent l'opérateur + de concaténation, ainsi que l'opérateur * pour la duplication :

???+ question "Tester le programme"

    {{ IDE('listeoper') }}


L'opérateur + est très pratique pour concaténer deux listes.

Vous pouvez aussi utiliser la méthode .append() lorsque vous souhaitez ajouter un seul élément à la fin d'une liste.

Dans les exemples suivants nous allons créer une liste vide, puis lui ajouter deux éléments, l'un après l'autre, avec  concaténation puis la méthode .append():

???+ question "Tester le programme"

    {{ IDE('listeajoute') }}

Dans l'exemple ci-dessus, nous ajoutons des éléments à une liste en utilisant l'opérateur de concaténation + ou la méthode .append(). Nous vous conseillons dans ce cas précis d'utiliser la méthode .append() dont la syntaxe est plus élégante.

 Fonction len()

 L'instruction len() vous permet de connaître la longueur d'une liste, c'est-à-dire le nombre d'éléments que contient la liste. Voici un exemple d'utilisation :

???+ question "Tester le programme"

    {{ IDE('listelen') }}

Les fonctions range() et list()

L'instruction range() est une fonction spéciale en Python qui génère des nombres entiers compris dans un intervalle. Lorsqu'elle est utilisée en combinaison avec la fonction list(), on obtient une liste d'entiers. Par exemple :

???+ question "Tester le programme"

    {{ IDE('listerangel') }}

La commande list(range(10)) a généré une liste contenant tous les nombres entiers de 0 inclus à 10 exclu. Nous verrons l'utilisation de la fonction range() toute seule dans le chapitre 5 Boucles et comparaisons.

Dans l'exemple ci-dessus, la fonction range() a pris un argument, mais elle peut également prendre deux ou trois arguments, voyez plutôt :

???+ question "Tester le programme"

    {{ IDE('listerange') }}

 Listes de listes

 Pour finir, sachez qu'il est tout à fait possible de construire des listes de listes. Cette fonctionnalité peut parfois être très pratique. Par exemple :

???+ question "Tester le programme"

    {{ IDE('listeliste') }}



Exercices :

* Créer une liste 'semaine' contenant les jours de la semaine.
    Comment accéder à jeudi? dimanche?

* Créez 4 listes hiver, printemps, ete et automne contenant les mois correspondants à ces saisons. Créez ensuite une liste saisons contenant les listes hiver, printemps, ete et automne. Prévoyez ce que renvoient les instructions suivantes, puis vérifiez-le dans l'interpréteur :

saisons[2]  
saisons[1][0]  



???+ question "Tester le programme"

    {{ IDE('listevide') }}

Une liste peut donc être modifiée par une méthode. On peut aussi modifier l’un de ses éléments par affectation.


???+ question "Tester le programme"

    {{ IDE('listemodif') }}

Ceci oblige à une grande attention en particulier dans la création de copie d’une liste. Observons le code suivant :


???+ question "Tester le programme"

    {{ IDE('listecopy') }}

Dans cet exemple, une même liste a deux noms et liste2 n’est pas une copie de liste1. Pour obtenir une copie, il faut créer
une nouvelle liste.  

Par exemple :  
liste2 = list(liste1)


**TP dur les listes**

[Lien TP](listes-boucles.pdf) 


[Lien TP corrigé](listes-bouclesCorr.pdf)



## II Les p-uplets

!!! tip "P-uplets : "
    Un p-uplet ou « tuple » est une liste non modifiable. Les objets du p-uplet peuvent être de type différent.


Remarque : en utilisant le langage Python, il faut utiliser les ( ) pour définir et utiliser un p-uplet.

Comme pour les listes, l'accès aux éléments d’un p-uplet se fait avec leurs indices, en n’oubliant pas que le premier indice est 0.

Exemples :

* A=(3, 5, 1) est un p-uplet de 3 entiers.
* B=(“salade”, ”tomate”) est un p-uplet de 2 mots.
* C=(5, ”livre”, 2.5) est un p-uplet qui contient 3 objets de type différent.

???+ question "Tester le programme"

    {{ IDE('listeuplet') }}

Remarque
En Python, il existe une fonction qui permet de transformer une liste en un p-uplet, il s’agit de la fonction « tuple ».


**Exercices** (Source Codingame www.codingame.com)

La fameuse course de module de Tatooine bat son plein et trois concurrents sont en course. La position de chaque concurrent est stockée dans un tuple de taille 3, mais la course est mouvementée:

A cause d'une panne moteur : 

* le premier concurrent passe à la dernière position.  
* Le second concurrent accélère et prend la tête de la course.  
* Le dernier concurrent sauve l'honneur et dépasse le second module sur la ligne d'arrivée.  


Compléter la fonction **panne_moteur**, prenant en argument un tuple de taille 3, et renvoyant un nouveau tuple où le premier module est passé dernier, le second premier et le dernier second.  

Compléter la fonction **passe_en_tete**, prenant en argument un tuple de taille 3, et renvoyant un nouveau tuple où le premier module est passé second et le second premier.

Compléter la fonction **sauve_honneur**, prenant en argument un tuple de taille 3, et renvoyant un nouveau tuple où le second module est passé dernier et le dernier second.

Bonus: Essayer d'écrire la fonction **sauve_honneur** sans manipuler les tuples, mais seulement en faisant appel aux fonctions **panne_moteur** et **passe_en_tete**.


???+ question "Now, this is Podracing! Whoopee!"

    {{ IDE('tatooine') }}


Une année s'est écoulée et la nouvelle édition de la course de module de Tatooine est encore plus captivante. Cette année, la position de chaque concurrent est stockée dans une liste.

Parmi les moments phares ce cette édition, il y a:

* Une panne moteur fait passer le premier concurrent à la dernière position.
* Le second concurrent accélère et prend la tête de la course.
* Le dernier concurrent sauve l'honneur et dépasse l'avant dernier module de la course.
* Un tir de blaster élimine le module en tête de la course.
* Dans un spectaculaire retournement de situation, un module qu'on pensait éliminé fait son grand retour à la dernière position.

Compléter la fonction **panne_moteur**, modifiant la liste passée en argument de manière à ce que le premier module passe dernier, le deuxième premier et ainsi de suite.

Compléter la fonction **passe_en_tete**, modifiant la liste passée en argument de manière à ce que le premier module passe deuxième et le deuxième premier.

Compléter la fonction **sauve_honneur**, modifiant la liste passée en argument de manière à ce que le dernier module passe avant-dernier et l'avant-dernier dernier.

Compléter la fonction **tir_blaster**, enlevant le premier concurrent de la liste passée en argument.

Compléter la fonction **retour_inatendu**, ajoutant un concurrent à la fin de la liste passée en argument.


???+ question "Looks like a few Tusken Raiders have camped out on the canyon dune turn!"

    {{ IDE('tatooine2') }}


## III Les dictionnaires

Un dictionnaire (dictionnary ou, en abrégé en Python, dict) est une collection qui associe une clé à une valeur. Par exemple, il est possible d’associer la clé "nom" à un nom et la clé "prenom" à un prénom.

!!! info "Prudence"
    Pour qu’une donnée puisse être utilisée comme une clé dans un dictionnaire, il faut qu’elle puisse produire une valeur de hachage (hash en anglais). Il s’agit d’une valeur numérique qui permet d’identifier la clé sans forcément lui être unique. Par défaut en Python, les nombres, les chaînes de caractères et les valeurs booléennes peuvent produire une valeur de hachage. Ils peuvent donc être utilisés comme clé dans un dictionnaire. On peut aussi utiliser comme clé un tuple d’éléments produisant une valeur de hachage.

Créer un dictionnaire  

Pour créer un dictionnaire, on associe une clé à une valeur en les séparant par :, le tout entre accolades {} :


???+ question "Création d'un dictionnaire :"

    {{ IDE('Dict1') }}

Accéder aux éléments  

Pour accéder à un élément d’une liste, il faut utiliser les crochets et préciser la valeur de la clé.


???+ question "Création d'un dictionnaire :"

    {{ IDE('Dict2') }}

Attention, si un programme veut accéder à une valeur à partir d’une clé qui n’existe pas, l’erreur KeyError est produite.

???+ question "Erreur Appel :"

    {{ IDE('Dict3') }}

Il est possible de changer la valeur pour une clé donnée ou ajouter une nouvelle valeur pour une nouvelle clé :


???+ question "Changer valeur :"

    {{ IDE('Dict4') }}

Les opérations sur les dictionnaires  
Connaître la taille d’un dictionnaire  
Pour connaître le nombre de valeurs dans un dictionnaire, il faut utiliser la fonction len() (qui est la contraction de length) :  

???+ question "Taille :"

    {{ IDE('Dict5') }}

Supprimer une clé d’un dictionnaire  
Pour supprimer une clé dans un dictionnaire, on utilise le mot-clé del :  


???+ question "Del :"

    {{ IDE('Dict6') }}

Savoir si une clé est présente dans un dictionnaire  
Pour savoir si une clé est présente dans un dictionnaire, on peut utiliser le mot-clé in :  

???+ question "In :"

    {{ IDE('Dict7') }}

Comparer deux dictionnaires  
On peut tester si deux dictionnaires sont ou non identiques.  

???+ question "Egalité :"

    {{ IDE('Dict8') }}


Quelques méthodes pour les dictionnaires  
get(key[, default])  
Retourne la valeur associée à la clé fournie en paramètre. Cette méthode diffère de l’utilisation des crochets [] car elle retourne la valeur du paramètre default (par défaut None) si la clé n’est pas présente dans le dictionnaire.

???+ question "get :"

    {{ IDE('Dict9') }}

clear()  
Supprime tous les éléments du dictionnaire.  

items()  
Retourne une vue du dictionnaire sous la forme d’une séquence itérable de tuples contenant le couple clé, valeur. Les modifications réalisées sur la vue sont répercutées sur le dictionnaire.  

???+ question "items :"

    {{ IDE('Dict10') }}

keys()  
Retourne une vue du dictionnaire sous la forme d’une séquence itérable contenant les clés. Les modifications réalisées sur la vue sont répercutées sur le dictionnaire.

???+ question "keys :"

    {{ IDE('Dict11') }}

values()  
Retourne une vue du dictionnaire sous la forme d’une séquence itérable contenant les valeurs. Les modifications réalisées sur la vue sont répercutées sur le dictionnaire.  


???+ question "values :"

    {{ IDE('Dict12') }}

copy()  
Crée une copie d’un dictionnaire.  

Parcourir un dictionnaire avec for  
Le dictionnaire est une structure de données très courante en Python. On peut peut parcourir un dictionnaire à partir de ses clés, de ses valeurs ou encore les deux à la fois :  

???+ question "Exemple :"

    {{ IDE('Dict13') }}

Exercice :

On définit un QCM comme une liste de questions. Une question est un dictionnaire avec les clés :  

libelle  
qui donne le libellé de la question.  

choix  
qui est un tableau des différents choix de réponse.  

reponse  
qui est un nombre correspondant à l’index du choix qui correspond à la bonne réponse.  

Complétez le programme suivant :  

???+ question "A compléter :"

    {{ IDE('Dict14') }}


## IV Exercices

### Exercice 1 : Tuples et Listes

1. Parmi les affirmations suivantes lesquelles sont vraies ?  
    a. Une liste peut contenir plusieurs éléments.  
    b. On peut ajouter des éléments à un tuple.  
    c. On peut modifier les éléments d'une liste.  
    d. Un tuple peut contenir à la fois des nombres et des chaînes de caractères.  

2. Si liste est la liste [1, 3, 5], quelles sont les opérations valides?  
    a. liste.append(4)  
    b. liste[0]  
    c. liste[0]=4  
    d. liste[4]=7  
    e. liste = [1, 3, 10, 7, 3]  

3. Si triplet est le tuple (1, 3, 5), quelles sont les opérations valides?  
    a. triplet.append(4)  
    b. triplet[0]  
    c. triplet[0]=4  


<details>
  <summary>Cliquez pour afficher la solution</summary>
    <p>1. a,c,d</p>
    <p>2. a,b,c,e</p>
    <p>3. b</p>
</details>

### Exercice 2 : Dictionnaires 

```python
dico ={"a":True, "b": False, "c": False}  
```

1. Quelle est la valeur de dico[1]  
    a. "a"  
    b. True  
    c. "b"  
    d. False  
    e. Rien car l'expression n'est pas valide  

2. Quelle est la valeur de dico["a"]?  
    a. True  
    b. False  
    c. Rien car l'expression n'est pas valide  

3. Quelle instruction permet de modifier le dictionnaire de façon à ce que sa nouvelle valeur soit {"a":True, "b": False, "c": False, "e": True}  
    a. dico["e"]=True  
    b. dico.append("e")  
    c. dico.append("e", True)  
    e. ce n'est pas possible car un dictionnaire n'est pas modifiable.  

4. Quels sont les affichages possibles lors de l'exécution du code suivant ?  
    ```python
    for cle in dico.keys() :  
        print(cle, end="")  
    ```
    a. a,b,c  
    b. (a, True) (b, False) (c, False)  
    c; True False False  

<details>
  <summary>Cliquez pour afficher la solution</summary>
    <p>1. e</p>
    <p>2. a</p>
    <p>3. a</p>
    <p>4. a</p>
</details>


### Exercice 3 : Compréhensions et structures imbriquées

1. Si liste désigne la liste [1, [2,3], [4,5], 6, 7], que vaut len(liste)?  
    a. 1  
    b. 3  
    c. 5  
    d. 7  

2. Que vaut [2*n for n in range(5)]?  
    a. [0, 2, 4, 6, 8]  
    b. [0, 2, 4, 6, 8, 10]  
    c. [0, 2, 4]  
    d. Autre chose  

3. Supposons que liste = [ -5, 2, 3, -7, 42, 7]    
Que vaut [n for n in liste if n > 0]  
    a. [ -5, 2, 3, -7, 42, 7]   
    b. [ 2, 3, 42, 7]  
    c. [False, True, True, False, True, True]  
    d; Autre chose.  

<details>
  <summary>Cliquez pour afficher la solution</summary>
    <p>1. c</p>
    <p>2. a</p>
    <p>3. b</p>
</details>

### Exercice 4 : Comprendre les tuples et les listes

On donne le script Python suivant :    

```python
premiers = [2, 3, 5, 7]  
couple = (7, 4)  
i = 3  
a = premiers[i]  
```

1. Quelle est la veleur de premiers[2]?  
    a. 1  
    b. 5  
    c. 7  

2. Après l'exécution de couple.append(1), quelle sera la valeur de couple?  
    a. (7, 4, 4)
    b. (7, 4, 7)
    c. (7, 4, 1)
    d. La valeur ne change pas car l'exécution provoque une erreur.  

3. Après l'instruction premiers[3] = 11, quelle sera la valeur de premiers?  
    a. [2, 3, 5, 7, 11]  
    b. [2, 3, 5, 11]  
    c. [2, 11, 5, 7]    
    d. [2, 3, 11, 7]  

<details>
  <summary>Cliquez pour afficher la solution</summary>
    <p>1. b</p>
    <p>2. d</p>
    <p>3. b</p>
</details>

### Exercice 5 : Modéliser des Pokémons avec des dictionnaires et des tuples

On modélise des informations (nom, taille et poids) sur des pokémons de la façon
suivante :  

```python
exemple_pokemons = {  
    'Bulbizarre' : (70, 7),  
    'Herbizarre' : (100, 13),  
    'Abo' : (200, 7),  
    'Jungko' : (170, 52)}  
```

Par exemple, Bulbizarre est un pokémon qui mesure 70cm et pèse 7kg.  

1. Quel est le type de exemple_pokemons?  
2. Quelle instruction permet d'ajouter à cette structure de données le Pokémon Goupix
qui mesure 60cm et qui pèse 10kg?  
3. On donne le code suivant :  
```python
def le_plus_grand(pokemons) :  
    grand = None  
    taille_max = None  
    for (nom, (taille, poids)) in pokemons.items() :  
        if taille_max is None or taille > taille_max:  
            taille_max =  taille  
            grand = nom  
    return (grand, taille_max)  
```

    a. Quelle est la valeur de le_plus_grand(exemple_pokemons) ?    
    b. Ecrire le code d'une fonction "le_plus_leger" qui prend des Pokémons en
    paramètre et qui renvoie un tuple dont la première composante est le nom du
    Pokémon le plus léger et la deucième composante est son poids.  
    c. Ecrire le code d'une fonction "taille" qui prend en paramètre un dictionnaire de Pokémons ainsi que le nom d'un pokemon, et qui renvoie la taille de ce pokémon.  


<details>
  <summary>Cliquez pour afficher la solution</summary>
    <p>1. exemple_pokemons est un dictionnaire, dont les clés sont des chaînes de caractère (str) et les valeurs, des tuples de deux entiers.</p>
    <p>2. Dans le dictionnaire, il faut ajouter la clé "Goupix" associé à la valeur 
    (60, 10) : exemple_pokemons["Goupix] = (60, 10)
    </p>
    <p>3. On parcourt le dictionnaire en conservant, dans les variables locales "grand" et "taille_max", le nom et la taille du Pokémon leplus grand rencontré jusqu'à présent.  
    La fonction renvoie un tuple contenant le nom et la taille du Pokémon le plus
    grand du dictionnaire (le premier rencontré en cad d'égalité).</p>
</details>

### Exercice 6 : La conjecture de Collatz

  
[![texte alternatif de l'image](http://img.youtube.com/vi/svYKQU-ExmA/0.jpg){: .center}](https://youtu.be/svYKQU-ExmA "Titre de la video")

Ecrire une fonction "collatz" qui prend en premier paramètre un nombre de départ pour
la suite et en deuxième paramètre le nombre d'itérations de la suite de collatz.  
Vérifier la conjecture avec votre fonction.  
