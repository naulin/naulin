---
author: Votre nom
title: Interaction homme-machine IHM
---

 
**auteur: Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac**

## I. Présentation

### Modèle client-serveur

<div class="centre"><center><iframe src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

### Page WEB
<div class="centre"><center>
<iframe src="https://player.vimeo.com/video/138623756?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

### HTML, CSS et JS
<div class="centre"><center><iframe src="https://player.vimeo.com/video/138623826?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

!!! abstract "Contenu et style"

    &#128083; Une page **HTML** est écrite en caractères lisibles mais codée.  
    Il n'est pas question d'étudier l'ensemble des possibilités du langage HTML, seulement les bases.

    &#128087;&#128086; / &#128221; On sépare le **style**(mise en page, couleurs, taille, type de polices de caractère, couleur de fond de page, ou des blocs,
    etc... du **contenu**.  
    &#128073; Le style est généralement défini dans un fichier séparé (ce n'est pas obligatoire, mais c'est très recommandé, et c'est ainsi que nous allons travailler).

    &#127797; Encore faut-il bien comprendre ce qu'on entend par le style et le contenu ! 
   
## II. Structure générale d'une page HTML

!!! abstract "Un début"

    ```html 
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
        </body>
    </html>
    ```


!!! abstract "Structure"

    Un document **HTML** est constitué d'un **document** contenant une **en-tête**, suivi d'un **corps**. 

    Repérer les balises permettant de délimiter :  

    * Le document

    ??? success "Solution"

        ```html
        <html>
        </html>
        ```

    * L'en-tête

    ??? success "Solution"

        ```html
        <head>
        </head>
        ```

    * Le corps

    ??? success "Solution"

        ```html
        <body>
        </body>
        ```

## III. Un premier exemple : 

Suivre ce lien, puis recopier dans la partie HTML le code ci-dessous.
Pour visualiser le rendu, cliquer sur Run.

[jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

!!! example "Exemple à recopier"

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       Il peut évidement y en avoir plusieurs.
                </p>         
        </body>
        </html>
        ```

Nous allons étudier ceci plus en détail.

😥 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." n'est pas écrite à la ligne comme sur le code.

👉 HTML ne prend pas en compte les espaces ou les sauts de lignes. Pour créer un saut de ligne, il faudra par exemple utiliser une balise `<br>`

## IV. Le concept de balises ouvrantes et fermantes, ou orphelines

### 1. Balises en paires

!!! abstract "Blocs"

    Comme vous pouvez commencer à le voir, dans l'exemple ci-dessus, une page <b>html</b> est organisée en blocs, 
    imbriqués les uns dans les autres. Le <b class="blue">header</b> est inclus dans le <b class="blue">html</b >, et le <b class="blue">title</b> est inclus dans
    le <b class="blue">header</b>


    Les blocs sont délimités par deux **balises**.  
    Par exemple :
    
    * **`<head>`** indique le début du header. C'est une **balise ouvrante**.
    * **`</head>`** indique la fin du header. C'est une **balise fermante**.


!!! abstract "Paires"

    On parle ici de balises en paire, avec une <b>balise ouvrante</b> et une <b>balise fermante</b>. 

!!! warning "&#128546; Et si on oublie une balise ?"

    
    Les navigateurs sont en général très tolérants, et une balise incorrecte (par exemple, il manque une fermante, ou il y a une fermante mais pas d'ouvrante...)
    sera ignorée. Dans les cas des balises ouvrantes sans fermante, cela engendrera le plus souvent un affichage incorrect et peu lisible. Dans
    le cas des balises fermantes sans ouvrante, l'effet est en général simplement nul : le navigateur ignore cette balise qui n'est 
    pas compréhensible. Toutefois, certains navigateurs, ou certaines balises, provoqueront des affichages totalement différents
    de ce qui est attendu et très souvent illisibles. C'est la responsabilité de l'auteur du document de veiller à respecter la syntaxe 
    du html.

??? tip "Astuce"

    &#128161; Il est conseillé, lorsqu'on écrit une balise ouvrante, d'instantanément écrire la balise fermante correspondante, et d'écrire ce que l'on souhaite entre les deux.

    😊 Pour s'y retrouver, il est recommandé d'utiliser des indentations.

!!! danger "Important"

    &#128073; Toutes ces balises indiquent **seulement** ce que contiennent les blocs. 
    Cela ne précise en aucun cas comment il faut les afficher dans la page. 
    Toutes les indications concernant **le style** seront
    fournies dans un fichier à part : la feuille de style **CSS**


### 2. Balises orphelines

Certaines balises ne servent pas à désigner un contenu, mais servent à ajouter un élément.

!!! example ""Exemples"

    * <b class="blue">&lt;br></b> indique un saut de ligne.  
    * La balise <b class="blue">&lt;hr></b> introduit une ligne horizontale dans toute la largeur de la page.  
    * <b class="blue">La balise &lt;img></b> qui sert à insérer une image est également une <b>orpheline</b>, mais 
    son utilisation est un peu différente et nous en reparlerons.

!!! example "Exemple à recopier"

    Lien pour visualiser : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>I. Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       <br> Il peut évidement y en avoir plusieurs.
                </p> 
                <hr>
                <h1>II. Titre de niveau 1 : </h1>
                    
        </body>
        </html>
        ```

😊 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." est bien écrite avec un saut de ligne grâce à la balise `<br>`


### 3. Quelques balises usuelles 

Aller sur ce site : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

#### a) Ecrire un nouveau document HTML complet. Dans le corps du document, c'est à dire entre les balises `<body>` et `</body>`, insérer le contenu suivant :

!!! abstract "Ma recette de cuisine"

    ```html linenums="1"
    <h1> La recette du carry de poulet </h1>

        <h2> Les ingrédients </h2>
            <ul>
                <li> un poulet découpé en morceaux </li>
                <li> 3 oignons </li>
                <li> 1 tomate </li>
                <li> 5 gousses d'ail </li>
            </ul>
        
        <h2> La préparation </h2>
            <p>Dans de l'huile chaude, faire revenir le poulet</p>
    ```

    ??? success "Solution"

        ```html 
        <!DOCTYPE html>
        <html>
            <head>
                <title>Titre de la page</title>
            </head>


            <body>
                <h1> La recette du carry de poulet </h1>

                <h2> Les ingrédients </h2>
                    <ul>
                        <li> un poulet découpé en morceaux </li>
                        <li> 3 oignons </li>
                        <li> 1 tomate </li>
                        <li> 5 gousses d'ail </li>
                    </ul>
        
                <h2> La préparation </h2>
                    <p>Dans de l'huile chaude, faire revenir le poulet</p>
            </body>
        </html>
        ```


#### b) Observer le résultat obtenu dans l'affichage et en déduire le rôle des balises suivantes :  

* `<ul>` et `</ul>`
* `<li>` et `</li>`

??? success "Solution"

    * `<ul>` représente une liste d'éléments non numérotés. Elle est souvent représenté par une liste à puces.
    * `<li>` est utilisé pour représenter un élément dans une liste. Celui-ci doit être contenu dans un élément parent : une liste ordonnée (`<ol>`), une liste non ordonnée (`<ul>`) ou un menu (`<menu>`). 

#### c) Faites les modifications suivantes :

* Ajouter un sous-titre 'Accompagnements' (titre de niveau 2) dans la page Web.
* Dans le sous-titre créer une liste à puces avec deux éléments : "riz blanc et grains", "riz jaune".
* Ajouter un paragraphe au début de la recette dans lequel on écrira "Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion"

??? success "Solution"

    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>

            <h1> La recette du carry de poulet</h1>

                <p>
                    Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion
                </p>

            <h2> Les ingrédients </h2>
                <ul>
                    <li> un poulet découpé en morceaux </li>
                    <li> 3 oignons </li>
                    <li> 1 tomate </li>
                    <li> 5 gousses d'ail </li>
                </ul>

            <h2> La préparation </h2>
                <p> Dans de l'huile chaude, faire revenir le poulet</p>

            <h2> Accompagnements</h2>
                <ul>
                    <li> riz blanc et grains </li>
                    <li> riz jaune </li>
                </ul>
        </body>
    </html>


    ```

* Ajouter le paragraphe suivant : 

??? success "A ajouter"

    ```html
    <p>
    <a href="https://fr.wikipedia.org/wiki/La_R%C3%A9union">île de la Réunion</a>
    </p>
    ```

Que s'est-il passé ?

??? success "Solution"

    Si l'on clique sur "île de la Réunion", on ouvre le lien donné sur l'île de la réunion.

* Ajouter un lien dans votre page Web sur les mots "3 oignons" qui permet d'accéder à l'adresse :  
 `https://fr.wikipedia.org/wiki/Oignon`

??? success "Solution"

    ```html
    <li> <a href=" https://fr.wikipedia.org/wiki/Oignon">3 oignons</a> </li>
    ```

!!! Important
    Les balises HTML permettent de **structurer** le contenu d'une page web, en définissant les titres, les paragraphes, ...

    &#127797; Pour modifier l'**apparence** d'une page, on a recours au **css** (**c**ascadind **s**tyle **s**heet) qui permet de modifier l'apparence du contenu de la page.
    
    😊 Nous verrons cela dans la leçon suivante.

## V. Insérer un lien

Nous avons vu un premier exemple avec la recette de cuisine.

!!! info "Insérer un lien"

    Voyons deux types de liens :

    * Les liens vers une pages externe :  
    Par exemple :  
    `<a href="https://fr.wikipedia.org/wiki/Anatomie_des_l%C3%A9pidopt%C3%A8res"> anatomie des lépidoptères.</a>`.

    * Les liens vers une autre page du même site :  
    Par exemple  :  
    `<a href="page2.html">Les lépidoptères</a>`  
    Dans ce cas le fichier peut être indiqué par un chemin relatif ou absolu. Nous verrons un exemple dans le paragraphe VII.


## VI. Insérer une image


!!! info "Insérer une image"

    La syntaxe pour insérer une image est la suivante :

    `<img src="source de l'image" alt="descritption de l'image">`

    👉 Les deux attributs src et alt sont obligatoires dans la norme HTML5.

!!! info "Source de l'image"

    Le source de l'image peut être :

    * Un nom de fichier : papillon.jpg. Dans ce cas le fichier contenant l'image doit se trouver dans le même répertoire que le fichier html
    * Un nom de fichier avec chemin relatif : ../images/papillon.jpg.
    * Un nom de fichier avec chemin absolu : /images/papillon.jpg. Dans ce cas, le fichier contenant l'image doit se trouver à l'endroit spécifié en partant de la racine du site.
    * Une adresse web : http://data.ba-bamail.com/Images/2014/9/10/934e8182-ea3f-4b3d-b1bf-dbda4fba9c3c.jpg

    👉 La première image est un fichier image, enregistré dans le même dossier que la page html. La seconde image est une image située sur le web, en l’occurrence, dans une page de wikipédia.

    👉 Notez bien qu'à ce stade on ne s'intéresse qu'au contenu. La façon dont l'image doit être présentée sera traitée ailleurs (centrée ou non, dans le texte ou séparée du texte, taille de l'image, etc..). Toutefois, si on veux des retours à la ligne avant et après l'image, on pourra placer la balise <img..> dans une paragraphe, comme ceci :

    ```html
    <p>
        <img src="image_locale.jpg" alt="une chenille">
    </p>
    ```

???+ note dépliée "Les chemins"

    Si l'image n'est pas dans le même dossier que le fichier html, il faudra utiliser un "chemin".

    👉 Voir le paragraphe "compléments"

## VII. Ecrire et visualiser une page écrite en HTML

!!! info "Editeur de code HTML"

    Il existe de nombreux éditeurs de code HTML, comme Notepad++, ou Sublime Text. Ils vous aideront à ne pas faire de faute de syntaxe.

???+ question "Créer un fichier HTML"

    Ouvrir l'éditeur dont vous disposez, copiez-collez le code ci-dessous, puis enregistrez votre fichier sous le nom : `essai_HTML.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Apprendre</title>
        </head>


        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```



!!! info "Visualiser du code HTML"

    * Pour le moment, nous avons utilisé le site en ligne `https://jsfiddle.net/`, très pratique pour de petits essais.  
    * 👉 A partir de maintenant nous allons tout simplement ouvrir un fichier HTML avec un navigateur.


???+ question "Visualiser un fichier HTML"

    * Utiliser votre explorateur de fichier, pour retrouver votre fichier.
    * Faire un clic droit sur votre fichier, puis "ouvrir avec", puis sélectionner "Firefox", ou "Google Chrome", ou "Microsoft Edge" par exemple.
    * Faire de même avec "Internet explorer" s'il est encore installé sur votre ordinateur.

    Que se passe-t-il ?

    ??? success "Vous pouvez obtenir quelque chose qui ressemble à ceci :"

        * Avec un navigateur "récent" : 

        ![avec Firefox](images/firefox.png){ width=35% .center}

        * Avec Internet Explorer : 

        ![avec Internet Explorer](images/intern_explor.png){ width=35% .center}


!!! info "Importance du `<head>`"

    😥 Comme vous le voyez, les navigateurs n'affichent pas toujours la même chose. C'est pourtant le même ficher, mais chaque navigateur peut interpréter les choses à sa façon.
    
    Ici c'est un problème d'encodage des accents qui provoque cette différence, et pour assurer un affichage correct dans tous les navigateurs, il faut ajouter une information dans le head.

    👉 Nous ajouterons dorénavant toujours dans `<head>` cette ligne : `<meta charset="UTF-8">`.

    😊 Ceci nous évitera le problème des accents, nous indiquons au navigateur que le texte est codé dans la norme utf-8 qui permet d'intégrer des accents dans le texte.


???+ question "Visualiser un fichier HTML avec le `head` complété"

    Recommencer l'expérience précédante avec le code suivant : 

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Apprendre</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```


## VIII. Un site à plusieurs pages

Nous allons voir comment cela fonctionne avec un site composé d'une page d'accueil et de deux autres pages.

!!! info "index"

    Il faut commencer par créer la page **obligatoirement** nommée `index.html`

    Voici un exemple plus détaillé que ce que nous avons déjà étudié.  
    Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `index.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <meta charset="utf-8">
                <title> Home </title>
            </head>
            <body>
                <header>
                    <h1> Mon en-tête : Accueil </h1>
                    <nav> 
                        <a href="page1.html"> page 1</a>
                        <br>
                        <a href="page2.html"> page 2</a>
                    </nav>
                </header>
                <main>
                    <section>
                    <h2> Section 1 </h2>
                    <p> Bla bla bla </p>
                    </section>

                    <section>
                    <h2> Section 2 </h2>
                    <p> Blo blo blo </p>
                    </section>
                </main>
                <footer> 
                    Mon pied de page
                </footer>
            </body>
        </html>
        ```


!!! info "Page 1"

    Vous devez maintenant écrire votre page 1.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page1.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 1</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 1</p>       
        </body>
        </html>
        ```


!!! info "Page 2 "

    Vous devez maintenant écrire votre page 2.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page2.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 2</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 2</p>       
        </body>
        </html>
        ```

!!! info "Visualiser votre tout petit site"

    Les trois fichiers doivent se trouver dans le même dossier.  
    Ouvrir le fichier `index.html` avec votre navigateur.



## IX. Le fichier CSS : principe général

!!! info

	Un document **html** est presque toujours accompagné d'au moins un fichier **CSS** qui décrit la mise en page,
	les formats (polices, couleurs, styles des éléments...). Un même fichier **CSS** peut être utilisé dans plusieurs pages d'un
	même site, ce qui permet, en le modifiant, de changer la présentation de toutes les pages (sinon il faudrait les modifier
	une à une). Mais ceci n'est qu'un aspect de l'intérêt des feuilles de styles. 

	👉 Essentiellement, elles ont pour rôle de permettre
	de séparer le contenu (qui est dans le html) de la forme (qui est dans le css)
	


### Sélecteur / Propriété / Valeur


Le CSS permet d'appliquer des styles sur différents éléments sélectionnés dans un document HTML. Par exemple, 
on peut sélectionner tous les éléments d'une page HTML qui sont paragraphes et afficher leurs textes en rouge avec 
ce code CSS :

!!! example "Exemple"

	```html
	p {
  		color: red;
	}
	```

!!! info

	Il y a 3 éléments dans la syntaxe :

	* **`p`** est le **sélecteur** : indique à quels éléments doit on applique le style. Ici, ce seront tout les éléments **`p`**, donc tous les paragraphes.
	* **`color`**est la **propriété** : indique (pour les éléments sélectionnés), quelle propriété on veut modifier.
	* **`red`** est la **valeur** qu'on attribue à la propriété pour les éléments sélectionnés.


### Où mettre le code CSS ?

!!! abstract "En bref"

    Il est possible de l'inclure dans la page html mais il est recommandé de mettre le code CSS dans un autre fichier,
    ou même parfois dans plusieurs fichiers, chacun contenant des instructions de formatage spécifiques. 


### Comment inclure le CSS dans la page ?

!!! info "`<link>`"

	  Etant donné qu'on place les instructions de style dans un fichier annexe, il est nécessaire de l'indiquer dans la page html.  
	  On utilise pour cela l'instruction `<link>` entre `<head>` et `<\head>`

!!! abstract "Un exemple"

    ```html 
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="mon_styles.css" type="text/css">
        <title>Titre de l'onglet</title>
    </head>

    <body>

    </body>
    </html>
    ```


???+ note "Précisions"

    * **`rel=`** définit le type de relation, ici stylesheet indique un lien vers une feuille de style.
	* **`href=`** donne le nom du fichier, qui peut être un chemin absolu, relatif, ou une adresse web.
	* **`type=`** définit le type de contenu, pour les feuilles de styles, c'est text/css.</li>


## X. Quelques propriétés fréquentes

Nous allons donner ici quelques syntaxes usuelles.
Il en existe beaucoup d'autres.  
Vous pourrez approfondir plus tard en suivant ce lien (on peut choisir comme langue le français) : [w3 schools](https://www.w3schools.com/css/default.asp){ .md-button target="_blank" rel="noopener" }

### 1. Pour les textes

!!! info "color : couleur du texte "

    Pour mettre les titres de niveau 1 en rouge

    ![titre rouge](../01_chapitre_1/images/titre_rouge.png){ width=100% .center}


!!! info "font-family : pour utiliser différentes polices "

    Pour mettre les paragraphes en police Monotype Corsiva .

    ![police](../01_chapitre_1/images/police.png){ width=100% .center}


!!! info "font-size : pour modifier la taille du texte"

    La taille en `1em` est la taille normale utilisée sur le client. `2em` sera **2 fois** plus grand.

    ![taille caractères](../01_chapitre_1/images/taille_font.png){ width=100% .center}


!!! info "font-style : pour mettre par exemple en italique"

    La valeur peut être : normal, italic ou oblique

    ![font style](../01_chapitre_1/images/font_style.png){ width=100% .center}


!!! info "text-align : pour centrer/justifier à gauche ou droite"

    La valeur peut être : center, left ou right

    ![alignement](../01_chapitre_1/images/alignement.png){ width=100% .center}


### 2. Pour la la page

!!! info "background : couleur du fond"

    Pour mettre un fond gris :

    ![fond](../01_chapitre_1/images/fond.png){ width=100% .center}


### 3. Pour une image

!!! info "width : largeur de l'image (en % ou en px)"

    Dans l'exemple suvant l'image occupe 10 % de la largeur de la page. Elle est donc très petite !
    
    ![taille image](../01_chapitre_1/images/taille.png){ width=100% .center}


### 4. Pour un lien ou un menu

!!! abstract "Un exemple"

    Considérons le code html suivant :

    ```html
    <nav>
      <ul>
      <li><a href="accueil.html">Accueil<a></li>
      <li><a href="Projets.html">Projets<a></li>
      <li><a href="quisommesnous.html">qui sommes nous</a></li>
      </ul>
    </nav>
    ```

L'apparence sans CSS est la suivante : 

![menu sans css](../01_chapitre_1/images/menu_sans_css.png){ width=15% .center}

!!! abstract "Ajoutons ceci dans notre fichier css :"

    ```html
    nav {
      width: 200px;
      list-style: none;
      margin: 0;
      padding: 0;
    }
    nav li {
      background: #c00 ;
      color: #fff ;
      border: 1px solid #600 ;
      margin-bottom: 1px ;
    }
    nav li a {
      display: block ;
      background: #c00 ;
      color: #fff ;
      font: 1em "Trebuchet MS",Arial,sans-serif ;
      line-height: 1em ;
      text-align: center ;
      text-decoration: none ;
      padding: 4px 0 ;
    }
    ```

L'apparence **avec** CSS est la suivante : 

![menu avec css](../01_chapitre_1/images/avec_css.png){ width=30% .center}

## XI. Exercice

???+ question "Modifier le style"

    Enregistrer le fichier suivant : `Le rougail de saucisses` : ["Clic droit", puis "Enregistrer la cible du lien sous"](telechargement/rougailsaucisses.html)

    1. Créer une feuille de style dans un fichier séparé, modifier l'apparence de cette page web de sorte que:

        * Le fond de la page soit de couleur bleu ciel (lightblue).
        * Les caractères de la page en bleu
        * Les titres de niveau 1 soient en vert.
        * Les titres de niveau 2 soient en violet.

        ??? success "Le fichier mon_style.css"

            ```html
            body {
	            background : lightblue;
	            color : blue;
            }

            h1 {
	            color : green;
            }

            h2 {
	            color : purple;
            }
            ```

    2. Créer le lien dans le fichier html vers la feuille de style.

        ??? success "Le lien"

            Début du fichier : 

            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="mon_style.css" type="text/css">
                <title>A la gloire du rougail saucisses</title>
            </head>
            ```

    3. Vérifiez le rendu avec votre navigateur.



???+ question "2. Réaliser un mini-site"

    Réaliser  un mini site *Web*, en utilisant des fichiers html et **une** feuille de style CSS. Le sujet du site est au choix, par exemple : vos films préférés, un site de recette de cuisines, un site sur un sport ou une de vos passions, ou sur une célébrité (sportif, acteur, chanteur ...) . 
    
    👉 Respecter le cahier des charges suivant :

    * au moins 5 pages reliées entre elles par des liens internes.
    * au moins 2 images, attention à utiliser des images **libres de droits** ou à créer vos propres illustrations pour votre site.
    * Il y aura un lien vers un site extérieur.
    * L'apparence du site sera uniformisée (c'est à dire que d'une page à l'autre on retrouvera les mêmes couleurs et la même présentation). Vous devrez pour cela utiliser **obligatoirement** une feuille de style css dans un fichier séparé.




## XII. Les formulaires

<center>
<iframe width="635" height="357" src="https://www.youtube.com/embed/XZ_sarpdgOE"  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


### 1. Qu’est-ce qu’un formulaire ?

Un formulaire créé une interface homme-machine (IHM) permettant une interaction avec l'utilisateur

![formulaire](https://mon.lyceeconnecte.fr/workspace/document/87a20845-bc05-46aa-bcb8-7743da1a235a?thumbnail=1600x0&){ width=40%; : .center }

### 2. Créer un formulaire simple

![prenom](./images/form_prenom.jpg){ width=20%; align=right }
Nous allons créer le formulaire suivant : 

Une fois exécuté par Alice, elle a obtenu l’affichage suivant :

![toto](./images/alice.png){ width=30% }
![toto](./images/bonjour_alice.png){ width=30% }

???+ "Créer le fichier `formulaire_Bonjour.html` suivant"

    ```html
	<!DOCTYPE html>
	<html>
	 <head>
	  <meta charset="utf-8"/>
	    <title> Hello </title>  
	 </head>
	<body>
	  <form action="bonjour.php" method="post">
	  	Entrez votre prénom : <br>
	  	<input type="text" name="prenom"> <br>
	  	<input type="submit" value="Envoyer">
	  </form>
	</body>
	</html>
	```

???+ question "Pour créer l'intéraction"

    L'attribut `action` du fichier `formulaire_Bonjour.html` précédant, donne l'url du programme PHP destiné à traiter les données validées. 

	Créer le fichier `bonjour.php` suivant 

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Hello</title>
	</head>
	<body>
		<?php
			$prenom = $_POST["prenom"];
			echo "Bonjour ".$prenom;
		?>
	</body>
	</html>
	```

???+ question "Utiliser le serveur UwAmp"

	* Recopier ces deux fichiers dans votre répertoire (du style C:\UwAmp\www\Dupond)
	* Exécuter le fichier formulaire_Bonjour.html

!!! info "Mon info"

    `$_POST` est un tableau associatif. Il contient tous les couples clé (les variables) / valeur transmis par la méthode `post`.
	

???+ question "Modification du formulaire "

	Reprendre l’exemple précédent, en rajoutant la saisie de l’âge.
    
    Vous enregistrerez les fichiers sous les noms `formulaire_bonjour_age.html` et `bonjour_age.php`

	Syntaxe pour saisir un nombre :
	`input type="number"`

	Votre programme devra afficher par exemple après exécution :  
	  
	Bonjour Alice  
	Vous avez 17 ans  

	??? success "Solution"

		```html
		<!DOCTYPE html>
		<html>
		<head>
		<meta charset="utf-8"/>
			<title> Hello </title>
		</head>
		<body>
		<form action="bonjour_age.php" method="post">
			Entrez votre prénom : <br>
			<input type="text" name="prenom"> <br><br>
			Entrez votre âge : <br>
			<input type="number" name="age"> <br>
			<input type="submit" value="Envoyer">
		</form>
		</body>
		</html>
		```

		et

		```html
		<!DOCTYPE html>
		<html>
		<head>
			<title>Hello</title>
		</head>
		<body>
			<?php
				$prenom = $_POST["prenom"];
				$age = $_POST["age"];
				echo "Bonjour " . $prenom . "<br>";
				echo "Vous avez " . $age . " ans"."<br>";
			?>
		</body>
		</html>
		```

## XIII. Méthode post et méthode get

!!! info "Mon info"

	Il existe deux méthodes pour récupérer les variables : la méthode **post** et la méthode **get**.

	La méthode **post** crée le tableau associatif $_POST, et la méthode **get** crée le tableau associatif $_GET.

### 1. Méthode get

???+ question "Tester get"

    Reprendre les fichiers formulaire_bonjour_age.html et bonjour_age.php et les enregistrer respectivement sous formulaire_bonjour_age_get.html et bonjour_age_get.php

	* Dans formulaire_bonjour_age_get.html rectifier action=, et remplacer method= " post " par method= " get "
	* Dans bonjour_age_get.php remplacer $_POST par $_GET

	Tester successivement formulaire_bonjour_age.html et formulaire_bonjour_age_get.html en observant l’adresse url de la barre de navigation. 

	Que constatez-vous ?

    ??? success "Solution"

        La méthode get fait circuler les informations du formulaire en clair dans la barre d'adresse.

???+ question "Que fait google ?"

	Dans un navigateur, par exemple Firefox, utiliser la barre de recherche google avec un mot : par exemple koala

	Quelle est la méthode utilisée par google ?

	??? success "Solution"

		On observe dans la barre d'adresse : https://www.google.com/search?client=firefox-b-d&q=koala

		google utilise donc la méthode get.
		

### 2. Comparaison des méthodes get et post

!!! info "Mon info"

	La méthode POST, transmet les informations du formulaire de manière masquée mais non cryptée. Le fait de ne pas afficher les données ne signifie en rien qu'elles sont cryptées. Rappelons nous d'ailleurs que ces informations utilisent le protocole HTTP et non HTTPS qui lui crypte les données.

	Quelle est la meilleure méthode à adopter alors ? Cela dépend. Le choix de l'une ou de l'autre se fera en fonction du contexte. Si par exemple, nous souhaitons mettre en place un moteur de recherches alors nous pourrons nous contenter de la méthode GET qui transmettra les mots-clés dans l'url. Cela nous permettra aussi de fournir l'url de recherches à d'autres personnes. C'est typiquement le cas des URLs de Google .

	La méthode POST est préférée lorsqu'il y'a un nombre important de données à transmettre ou bien lorsqu'il faut envoyer des données sensibles comme des mots de passe. Dans certains cas, seule la méthode POST est requise : un upload de fichier par exemple.

	[Source](https://apprendre-php.com/tutoriels/tutoriel-12-traitement-des-formulaires-avec-get-et-post.html)




##  Découverte Javascript  

[Vidéo Lumni](https://www.lumni.fr/video/notions-de-web-et-d-interface-homme-machine){ .md-button target="_blank" rel="noopener" }

🌐 Vous pouvez télécharger le document correspondant ici : [Document Lumni](https://medias2ftv.akamaized.net/videosread/education/PDF/NSI_Web_support.pdf){ .md-button target="_blank" rel="noopener" }


L'objectif de ce cours n'est pas de faire de vous des codeurs de JavaScript experts, mais simplement de vous donner les clés qui vous permettront d'explorer ce langage.

!!! info "JavaScript"

	JavaScript est un langage de programmation coté client (tout se passe sur votre machine) à contrario de php qui lui est déporté coté serveur.

	C'est un langage qui va nous permettre de créer de l'interaction entre l'utilisateur et le client.


![client-serveur](https://i.ibb.co/YpCxNNY/dyndns.png){: .center}  


Jusqu'à présent, la page web envoyée par le serveur est :

1. identique quel que soit le client.
2. statique après réception sur l'ordinateur du client.

Le JavaScript va venir régler le problème n°2 : il est possible de fabriquer une page sur laquelle le client va pouvoir agir **localement**, sans avoir à redemander une nouvelle page au serveur.

Inventé en 1995 par [Brendan Eich](https://fr.wikipedia.org/wiki/Brendan_Eich){:target="_blank"} pour le navigateur Netscape, le langage JavaScript s'est imposé comme la norme auprès de tous les navigateurs pour apporter de l'interactivité aux pages web.


### A vous de jouer 1

???+ question

    Dans votre éditeur de texte (Notepad++ ou dans Sublime Text ou autre), recopier le fichier suivant (enregistré par exemple sous bouton_change_fond.html):

    ```html
    <!DOCTYPE html>
	<html>
		<head>
			<title>Exemple changer fond</title>
			<meta charset="utf-8">
		</head>


		<body>
			<h1> Mon bouton pour changer le fond </h1>
			<input type="button" value="Changer de couleur" onclick="changeCouleur()" />
		</body>
	</html>
	```
Comment exécuter ce programme ?

Que se passe-t-il ?

??? success "Solution"

    * Comment exécuter ce programme ?  
	Il suffit de l'enregistrer en format html en l'appelant "n'importe_quel_nom.html"
	* Que se passe-t-il ?  
	Une erreur est détectée car la fonction `changeCouleur` n'est pas définie.

??? question "Compléter"

    Compléter de la façon suivante, avec du code JavaScript, qui se trouve entre les balises `<script>` et `</ script>`

    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Exemple changer fond </title>
            <meta charset="utf-8">
            <script>
                function changeCouleur() {
                document.body.style.backgroundColor="orange"
                }
            </script>
        </head>


        <body>
            <h1> Mon bouton pour changer le fond </h1>
            <input type="button" value="Changer de couleur" onclick="changeCouleur()" />
        </body>
    </html>
    ```   
Que se passe-t-il ?

??? success "Solution"  

    Lorsque l'on exécute ce programme, et que l'on clique sur le bouton "Changer de couleur", le fond du site devient orange.

### A vous de jouer 2

Nous pouvons aussi écrire du code JavaScript dans un fichier séparé. C'est ce qui est fait très souvent.

!!! abstract "Exemple de couple ```html``` / ```javascript``` minimal"
    Notre fichier ```index.html``` fait référence, au sein d'une balise ```<script>```, à un fichier externe ```script.js``` qui contiendra notre code JavaScript.   

    Suivre ce lien  [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }, puis recopier dans la partie HTML le code HTML, et dans la partie JavaScript le code JavaScript donnés ci-dessous.

	Pour visualiser le rendu, cliquer sur Run.

	

    - fichier ```index.html``` : 
    ```html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>un peu d'action</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
      </head>
      <body>
        <script src="script.js"></script>
        <p>
        <h2>Une page web extrêmement dynamique</h2>
        </p>
        <div>

            <label>Changez la couleur d'arrière-plan:</label>

            <button type="button" onclick="choix('yellow');">jaune</button>

            <button type="button" onclick="choix('green');">vert</button>

            <button type="button" onclick="choix('purple');">violet</button> 
        </div>
        <div>
          <p>
          En JavaScript, le nom de la couleur choisie est :
          </p>
          <p id="resultat"></p>
        </div>
      </body>
    </html>
    ```


    - fichier ```script.js``` :
    ```javascript
    function choix(color){
        document.body.style.background = color;
        document.getElementById("resultat").innerHTML=color;
    }
    ```


!!! info "Les boutons"


	- Au sein du bouton déclaré par la balise ```button```, l'attribut  ```onclick``` reçoit le nom d'une fonction déclarée à l'intérieur du fichier ```script.js```, ici la fonction ```choix()```.
	- Cette fonction nous permet de modifier à la fois l'aspect esthétique de la page (changement de la couleur de background) mais aussi le contenu de cette page, en faisant afficher le nom de la couleur.

	La puissance du JavaScript permet de réaliser aujourd'hui des interfaces utilisateurs très complexes au sein d'un navigateur, équivalentes à celles produites par des logiciels externes (pensez à Discord, par ex.). 


## II. ☝️ Où placer le code JavaScript ?

!!! info "A savoir"

	Le JavaScript peut se placer à trois endroits différents :

	* Dans l’élément head d’une page HTML ;
	* Dans l’élément body d’une page HTML ;
	* Dans un fichier portant l’extension .js séparé.

Nous avons vu au-dessus un code écrit dans un fichier séparé. Nous allons tester les deux autres possibilités.

Vous pouvez utiliser le site Suivre ce lien  [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

???+ question "Tester"

	Recopier le code suivant et le tester.

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Un peu de JS</title>
		<meta charset="UTF-8">
		<script> alert("J'ai besoin d'aide !!");</script>
	</head>


	<body>	
		<h1>Où placer le code JavaScript ?</h1>	
		<p>
			Le JavaScript peut se placer à trois endroits différents :
			<ul>
				<li>Dans l’élément head d’une page HTML ;</li>
				<li>Dans l’élément body d’une page HTML ;</li>
				<li>Dans un fichier portant l’extension .js séparé </li>
			</ul>
		</p>
		    
	</body>
	</html>
	```

???+ question "Tester"

	Recopier le code suivant et le tester.

	```html
	<!DOCTYPE html>
	<html>
	<head>
		<title>Un peu de JS</title>
		<meta charset="UTF-8">		
	</head>


	<body>	
		<script> alert("J'ai besoin d'aide !!");</script>
		<h1>Où placer le code JavaScript ?</h1>	
		<p>
			Le JavaScript peut se placer à trois endroits différents :
			<ul>
				<li>Dans l’élément head d’une page HTML ;</li>
				<li>Dans l’élément body d’une page HTML ;</li>
				<li>Dans un fichier portant l’extension .js séparé </li>
			</ul>
		</p>
		    
	</body>
	</html>
	```


## III. Un exemple : la méthode `getElementById`

Nous avons déjà utilisé cette méthode. Nous allons l'étudier un peu plus précisément.

!!! info "Syntaxe"

	La méthode `getElementById` permet de récupérer les informations d'une balise identifiée par son id.
	
	informations à récupérer = document.getElementById("nom");


???+ question "Tester"

    Recopier le code suivant, l'enregistrer en fichier html, puis l'exécuter avec Mozilla Firefox

    ```html
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<script>
			function changeCouleur(nouvelleCouleur) {
				document.getElementById("paragraphe").style.color = nouvelleCouleur;
			}
			</script>
		</head>
		<body>
			<p id="paragraphe"> 
			informations à récupérer = document.getElementById("nom");
			</p>
			<p>
				<ul>
	    		<li>document représente la page HTML, plus précisément l'intérieur de la fenêtre du navigateur : 
				la zone d'affichage sans la barre d'adresse et les boutons de navigation.</li>
	    		<li>nom désigne la valeur de l'attribut id d'une balise unique située dans la page.</li>
	    		</ul>
			</p>
			<button onclick="changeCouleur('blue');">blue</button>
			<button onclick="changeCouleur('red');">red</button>
		</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur.

	Vous lisez le code correspondant à la page affichée.

	Oberver la ligne `<p id="paragraphe">`

	Cliquer sur les boutons, et observer le code html. Que se passe-t-il ?
	Rafraichissez la page. Que se passe-t-il ?

    ??? success "Solution"

        * Lorsque l'on clique sur le bouton "blue", le paragraphe devient écrit en bleu, et la ligne de code html a été modifiée :  
        `<p id="paragraphe" style="color: blue;">`
        * Lorsque l'on clique sur le bouton "red", le paragraphe devient écrit en rouge, et la ligne de code html a été modifiée :  
        `<p id="paragraphe" style="color: red;">`
        * Lorsque l'on rafraichit la page, elle redevient à son état initial, et la ligne de code html redevient :
        `<p id="paragraphe">`


!!! info "Syntaxe"

	document.getElementById('exemple').innerHTML = "Code HTML à écrire";

	La propriété innerHTML permet de remplacer complètement le contenu d’une balise identifiée par son attribut id. Pour faire simple, elle permet d’écrire à un endroit précis de la page, sans tout effacer.


???+ question "Tester"

    Recopier le code suivant, l'enregistrer en fichier html, puis l'exécuter avec Mozilla Firefox.

    ```html
    <!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>
		<input type="button" value="clic" onclick="changetexte()">
		<script>
		function changetexte() {
			document.getElementById("exemple").innerHTML = "Texte changé !";
		}
		</script>
	</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur.

	Oberver la ligne `<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>`

	Cliquer sur le bouton, et observer le code html. Que se passe-t-il ?
	Rafraichissez la page. Que se passe-t-il ?

	??? success "Solution"

		Lorsque l'on clique sur le bouton, la ligne :  
		`<p id="exemple">Cliquez sur le bouton et ce texte changera.</p>`  
		est remplacée par la ligne :  
		`<p id="exemple">Texte changé !</p>`  


???+ question "Tester"

	```html
	<!DOCTYPE html>
	<html>
		<head>
			<title>JavaScript</title>
			<meta charset="utf-8">
			<script>
				function AfficheDate()
				{
					document.getElementById("date").innerHTML = Date();
				}
				function Produit(a, b)
				{
					var p = a*b;
					document.getElementById("resultat").innerHTML = p;

				}
			</script>
		</head>
		<body>
			<p id="date">Date ?</p>
			<button type="button" onclick="AfficheDate()">Cliquer pour avoir la date</button>
			<p>3*4= <span id="resultat">???</span></p>
			<button type="button" onclick="Produit(3, 4)">Cliquer pour avoir le résultat</button>		
		</body>
	</html>
	```

	Appuyer sur la touche F12 de votre clavier, puis sélectionner l'onglet Inspecteur. Observez bien les modifications du code html


!!! abstract "Résumé"

    * JavaScript s'exécute côté client
    * JavaScript modifie le code html qui est exécuté par le navigateur pour afficher la page.


## IV. Exercice

🌵 Ecrivez votre propre petite page web, avec un peu de code JavaScript ...

???+ question "Exemple d'input"

	```html
	<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Récupérer la valeur d'un input texte</title>
    </head>
    <body>
    <input type="text" placeholder="Entrez une valeur ici" id="in">
    <button type="button" onclick="getValue();">Récupérer la valeur</button>
    
    <script>
      // Mettez le code javascript ici.
	  function getValue() {
		// Sélectionner l'élément input et récupérer sa valeur
		var input = document.getElementById("in").value;
		// Afficher la valeur
		alert(input);
		}
    </script>
    </body>
    </html>
	```

[Exercices JAVASCRIPT](https://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/html/cours_js_nsi/formulaires/form_js.html)


## V. Crédits

Auteurs : Fabrice Nativel, Gilles Lassus, Jean-Louis Thirot, Valérie Mousseaux, Mireille Coilhac

