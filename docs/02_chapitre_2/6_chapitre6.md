---
author: Votre nom
title: Architectures matérielles et systèmes d’exploitation
---

## I. Systèmes d'exploitation : 


### Historique

[![texte alternatif de l'image](http://img.youtube.com/vi/Uq5ddTUtDkM/0.jpg){: .center}](https://youtu.be/Uq5ddTUtDkM "Titre de la video")



### Découverte de Linux et de ses commandes

![morse](https://i.ibb.co/b3YrntZ/BO.png){ width=80%; : .center }

à partir du jeu Terminus

![morse](https://i.ibb.co/5np0dMW/terminus.png){ width=30%; : .center }

Rendez-vous à l'adresse [Jeu Terminus](http://luffah.xyz/bidules/Terminus/)

Laissez-vous guider par le jeu, mais attention ! - vous devez noter sur un papier chaque nouvelle commande que vous apprenez. Vous pouvez par exemple construire un tableau de ce type :

![morse](https://i.ibb.co/tMXjg6H/term-1.png){ width=80%; : .center }


je vous conseille très fortement d'écrire un plan du jeu au fur et à mesure que vous avancez dans votre quête. Par exemple :


![morse](https://i.ibb.co/R2FTsFp/term-2.png){ width=80%; : .center }


### Permissions et droits sous linux

Les systèmes de type "UNIX" sont des systèmes multi-utilisateurs, plusieurs utilisateurs peuvent donc partager un même ordinateur, chaque utilisateur possédant un environnement de travail qui lui est propre.

Chaque utilisateur possède certains droits lui permettant d'effectuer certaines opérations et lui en interdisant d'autres.
Le système d'exploitation permet de gérer ces droits.  
Un utilisateur un peu particulier possède tous les droits : ce "super utilisateur" est appelé root.

C'est l'utilisateur root qui pourra attribuer ou retirer certains droits aux autres utilisateurs.

Au lieu de gérer les utilisateurs un par un, il est possible de créer des groupes d'utilisateurs. L'administrateur attribue des droits à un groupe au lieu d'attribuer des droits particuliers à chaque utilisateur.  
Comme nous venons de le voir, chaque utilisateur possède des droits qui lui ont été octroyés par le "super utilisateur". Nous nous intéresserons ici uniquement aux droits liés aux fichiers, mais vous devez savoir qu'il existe d'autres droits liés aux autres éléments du système d'exploitation (imprimante, installation de logiciels...).  

[Lien du cours](http://iamjmm.ovh/NSI/permissions/site/index.html)
