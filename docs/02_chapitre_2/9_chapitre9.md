---
author: Votre nom
title: Algorithmique
---

## I. Les tris : 

[![texte alternatif de l'image](http://img.youtube.com/vi/ra79TDfotno/0.jpg){: .center}](https://youtu.be/ra79TDfotno "Titre de la video")



Les algorithmes de tri permettent la compréhension de notions fondamentales en informatique. Ça tombe aussi très souvent en entretien d’embauche ! Ça prend quelques minutes à comprendre et ça va vraiment te servir un jour ou l’autre.


Oui, ça va te servir
Tu n’utiliseras pas vraiment tous les jours dans un vrai boulot. Dans la vraie vie, les algorithmes de tri, en particulier ceux d’aujourd’hui, sont déjà implémentés dans les langages de haut niveau.

Mais un beau jour tu vas débarquer en entretien de façon nonchalante en te disant que tu sais tout. Et là, on va te demander d’implémenter un tri. Tu vas te sentir seul à regarder l’écran et ne plus savoir quoi faire.

source : https://www.jesuisundev.com/comprendre-les-algorithmes-de-tri-en-7-minutes/

Nous allons apprendre deux algorithmes de Tri dans ce chapitre : leurs fonctionnements, leurs implémentations et leurs complexités.

Lien du TD  : [TD](https://capytale2.ac-paris.fr/web/c/5ff1-2313726)

Codes des algorithmes :  

Dans l’algorithme de tri par sélection, nous cherchons l’élément le plus petit et on le met au bon endroit. Nous échangeons l’élément en cours avec le prochain élément le plus petit.  

![morse](images/selection.png){ width=50%; : .center }

???+ question "Tri par séléction :"

    {{ IDE('selection') }}


Conclusion
Le tri par sélection fonctionne mieux avec un petit nombre d’éléments. La complexité d’exécution du tri par sélection dans le pire des cas est o(n2) pareil à celle des tri par insertion et par bulle.


L’algorithme du tri par insertion n’est utile que pour les petits éléments, car il nécessite plus de temps pour trier un grand nombre d’éléments. Voici comment le processus fonctionne :

![morse](images/insertion.gif){ width=50%; : .center }

???+ question "Tri par insertion :"

    {{ IDE('insertion') }}

Dans l’algorithme de tri à bulle, le tableau est parcouru du premier au dernier élément. Ici, l’élément courant est comparé à l’élément suivant. Si l’élément en cours est supérieur à l’élément suivant, il est échangé. Voici comment le processus fonctionne :

![morse](images/bulle.gif){ width=50%; : .center }

???+ question "Tri par bulle : (pas au programme)"

    {{ IDE('bulle') }}

Nous allons étudier (c'est un travail important mais difficile) ce que l'on appelle la complexité (en temps) de
ces algorithmes. Si on trace des courbes en fonction du nombre de valeurs des tableaux à trier, nous obtenons
quelque chose comme ceci :

![morse](images/complexite.png){ width=50%; : .center }

Si l'on compare ces différentes méthodes de tri, on peut s'apercevoir que le tri bulle est le moins efficace en terme de temps d'exécution. Viennent ensuite le tri par sélection puis celui par insertion. La différence est grande entre le tri bulle et ces deux tris. Ainsi, pour trier un tableau de 30000 éléments aléatoires, le tri bulle met trois fois plus de temps que les deux autres.


!!! info "A savoir :"
    Nous allons surtout étudier la complexité des algorithmes en rapport avec le temps. Mais la complexité d’un algorithme peut également être calculée en rapport avec toutes les ressources qu’il utilise, par exemple des ressources d’espace en mémoire ou de consommation d’énergie.


!!! info "Théorème 1 :"

    La complexite du tri par sélection est quadratique, c’est-à-dire de l’ordre de \(n^2\). On dit qu’elle est en \(O(n^2)\).

![morse](images/Demo.png){ width=100%; : .center }


!!! info "Théorème 2 :"

    La complexite du tri par insertion est :  
    lineaire dans le meilleur des cas, c’est-à-dire de l’ordre de \(n\). On dit qu’elle est en \(O(n)\).  
    quadratique dans le pire des cas, c’est-à-dire de l’ordre de \(n^2\). On dit qu’elle est en \(O(n^2)\).


![morse](images/Demoinsert.png){ width=100%; : .center }

**Exercices**

[lien](tris_cours_exercices.pdf)

**Exercice (pour aller plus loin)**

Un tableau tab contient n boules qui peuvent être bleues ou rouges. Ecrire un algorithme qui permet de trier le tableau en mettant
les boules bleues à gauche et les boules rouges à droite (sans utiliser de tableau annexe et en ne testant qu'une seule fois chaque boule).  

Même question avec des boules bleues, blanches et rouges (Drapeau français de Dijkstra).

## II. Algorithme glouton 

[![texte alternatif de l'image](http://img.youtube.com/vi/HvAK6HxZ190/0.jpg){: .center}](https://youtu.be/HvAK6HxZ190 "Titre de la video")

La méthode glouton est une méthode de recherche de solution locale qui va apporter une solution globale non nécessairement optimale. Le problème se ramène à chaque étape à un problème plus simple et chaque étape selectionne l'une des meilleures possibilités et ne remet jamais en cause les choix précédents.

Parmis les exemples classiques, le rendu de monnaie: on veut rendre la monnaie de façon optimale cad avec le moins de pièces possible.

Quelle est la stratégie la plus simple à adopter? Aller au plus pressé: rendre la plus grande pièce possible. Par exemple, en euros, si je dois rendre 195 ct, je rends en premier 1€ (100ct), puis 50 ct puis 20ct puis 20ct puis 5ct. Cette stratégie fonctionne avec les euros. Pouvez vous fabriquer un jeu de pieces pour lequel la stratégie glouton n'est pas optimale. Dans cet exercice, si la méthode glouton aboutit, elle est optimal puisqu'elle utilise les pièces les plus grandes en premier mais dans certains cas, la méthode Glouton ne trouve pas de solution alors qu'il en existe une. Pour d'autres problèmes (voir les maisons plus haut), la méthode n'est pas optimale car elle donne bien une solution mais pas la meilleure.


Lien du TD  : [TD](https://capytale2.ac-paris.fr/web/c/0c9c-2424702)

Correction : [TD](./Algorithme%20Glouton%20Sac%20à%20dos%20Correction.pdf))

## III. Recherche Dichotomique

## ^^Introduction : un petit jeu^^

Certains d'entre vous ont programmé ce petit jeu :


{{IDE("jeumystere_REM")}}


Vous pouvez y rejouer en copiant le code dans un IDE ou dans [une console Basthon](https://console.basthon.fr){target="_blank"}.

Quelle stratégie vous semble la plus pertinente pour trouver le nombre le plus rapidement possible ?

## ^^1- Rappel : Recherche séquentielle^^

Le tableau est une structure de données extrêmement utilisée en informatique.

Une des questions que l'on peut se poser est de savoir si une valeur appartient, ou non, à ce tableau.  

Nous avons déjà vu l'algorithme de **recherche séquentielle**.

![RechercheSequentielle.gif](images/RechercheSequentielle.gif)

!!! done "Algorithme de recherche séquentielle"
    {{ IDE('recherche_sequentielle') }}

L'algorithme précédent est __correct__ : on peut prouver qu'il fonctionne correctement dans tous les cas.

Nous avons admis que, comme l'algorithme de calcul de moyenne ou celui de calcul d'un minimum/maximum, il est de complexité linéaire : le nombre moyen d'instructions effectuées et le temps moyen de calcul sont proportionnels à la taille du tableau.

!!! done "Vérification expérimentale de la performance de la recherche séquentielle"
    On peut vérifier expérimentalement que si la taille d'un tableau est multipliée par 100, alors le temps de recherche séquentielle d'une valeur dans ce tableau est aussi multiplié par 100 (_approximativement_).

    _De préférence_, copier et exécuter ce code dans un IDE.

    {{ IDE('perf_sequentielle') }}


## ^^2 - Principe de la recherche dichotomique^^

Le fait qu'un tableau soit trié, par exemple par ordre croissant, facilite de nombreuses opérations. 

Le fait de disposer d'un tableau déjà trié se produit par exemple quand on dispose d'enregistrements chronologiques, comme ci-dessous dans le tableau des décollages d'avions dans un aéroport.

![tab_aeroport.jpg](./images/tab_aeroport.jpg){: .center}



```python
tab_departs = [(12.39, 'Londres'), (12.57,'Zurich'), (13.08,'Dublin'), (13.21,'Casablanca'), 
               (13.37,'Amsterdam'),(13.48,'Madrid'),(14.19,'Berlin'), (14.35,'New York'),
              (14.54, 'Rome'), (15.10,'Stockholm')]
```

Supposons que l'on cherche à savoir si un décollage est prévu à 14h00 pile. 

On peut lancer une recherche séquentielle du nombre `14.00` dans ce tableau.

Mais on peut __tirer profit du fait que le tableau est déjà ordonné, déjà trié__. Voici un procédé possible :

1. On commence par comparer la valeur recherchée (ici `14.00`) avec la valeur située au milieu du tableau (ici `13.37`) :

* Si la valeur recherchée est plus petite, on peut restreindre la recherche à la première moité du tableau.
* Sinon, on la restreint à la seconde moitié du tableau.

2. Et on recommence, mais avec une partie du tableau deux fois plus petite. À chaque étape, on divise la zone de recherche par deux à chaque étape :  très rapidement, on parviendra soit à la valeur recherchée, soit à un intervalle vide.

On appelle ceci une **recherche dichotomique** (en anglais : *binary search*).


!!! tip "Définition du mot _dichotomie_ du dictionnaire Larousse"
    Division de quelque chose en deux éléments que l'on oppose nettement.
    Exemple : Dichotomie entre la raison et la passion.
    
Dans notre procédé de recherche par dichotomie, on fait une **division du tableau trié en deux parties** : d'un côté la première moitié (composée des petites valeurs), de l'autre la seconde moitié (composée des grandes valeurs).

## ^^3 - La méthode dichotomique, par l'exemple^^

Supposons que l'on recherche si la valeur __35__ est présente ou non dans le tableau trié suivant : `[5, 7, 12, 14, 23, 27, 35, 40, 41, 45]`

Les étapes de recherches sont schématisées ci-dessous, avec :

- __debut__ qui marque le __début de la zone de recherche__.
- __fin__ qui marque la __fin de la zone de recherche__.
- __milieu__, la __valeur médiane entre le début et la fin de la zone__ de recherche. Cette valeur médiane `milieu` est le résultat du calcul de la moyenne entre `debut` et `fin`, _arrondie si besoin à l'entier inférieur_ : `milieu = (debut + fin) // 2`

![recherche_dichotomique.png](images/recherche_dichotomique.png)

Si vous préférez, voici une autre __représentation de cette recherche, sous la forme d'un arbre__ :

![recherche_dichotomique_arbre.png](images/recherche_dichotomique_arbre.png)

!!! question "Exercice"
    Représenter les différentes étapes de la recherche dichotomique de la valeur 7 dans le tableau `[0, 1, 1, 2, 3, 6, 8, 12, 21]` 


## ^^4 - Algorithme de recherche dichotomique d'un tableau trié^^

Remarques préliminaires :

* en entrée de l'algorithme, on fournit un tableau `tab` qui est supposé **trié par ordre croissant**, ainsi qu'une valeur `val` à rechercher ;
* en sortie, l'algorithme doit renvoyer `True` si la valeur `val` est dans le tableau `tab` et `False` sinon.

Voici en pseudo-code l'algorithme de recherche par dichotomie :

    FONCTION Recherche_Dichotomique(tab,val)
        debut ← 0  # choix de numéroter à partir de zéro les cases du tableau
        fin ← longueur(tab) - 1
        TANT_QUE debut ⩽ fin
            milieu ← (debut + fin) // 2
            SI tab[milieu] = val ALORS
                 RENVOYER VRAI
            SINON
                SI val > tab[milieu] ALORS
                    debut ← milieu + 1
                SINON
                    fin ← milieu - 1
                FIN_SI
            FIN_SI
        FIN_TANT_QUE
        RENVOYER FAUX


!!! question "Exercice"
    Écrire le code Python de cet algorithme de recherche dichotomique.


{{ IDE('trameTriAlgo')}}

__Testons alors cet algorithme :__


{{ IDE('testTriAlgo')}}


## ^^5 - Efficacité, complexité de la recherche dichotomique^^

L'algorithme de recherche élaboré ci-dessus applique le principe ***"diviser pour régner"*** : à chaque étape (si la valeur n'a pas encore été trouvée), la charge de travail est **divisée par deux**.

Inversement, si on multiplie par 2 la taille du tableau, il n'y aura besoin que d'une étape supplémentaire pour y effectuer la recherche d'une valeur. 

Puisqu'il ne faut qu'une étape pour faire une recherche dans un tableau de taille 2, on peut dresser le tableau suivant :

<figure markdown>
| Taille du tableau | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
|---------------------------------------------------|---|---|---|----|----|----|-----|-----|
| Nombre maximal d'étapes  | 1 | 2 | 3 | 4  | 5  | 6  | 7   | 8   |
</figure>

On voit ainsi apparaitre le lien mathématique entre ces deux quantités : la taille $t$ du tableau est égale à $ 2^n$ où $n$ est le nombre de d'étapes de l'algorithme. 

Or ce que l'on cherche à connaitre, c'est la relation "inverse", à savoir le nombre d'étapes $n$ en fonction de la taille $t$. 
En mathématiques, on parle de "fonction réciproque" et la fonction réciproque de la fonction ***"2 à la puissance n***" s'appelle la fonction ***logarithme de base 2***.

Elle se note $\log_2$ (en mathématiques) et `log2` en Python (avec le module `math`) : on a donc $\log_2(8) = 3$, $\log_2(16) = 4$, etc. 

{{ IDE("calcul_log2")}}

!!! tip "Complexité logarithmique"
    Le nombre maximal d'étapes de l'algorithme de recherche par dichotomie s'écrit alors $n = \log_2(t)$, où $t$ est la taille du tableau. On dit que la recherche dichotomique est un algorithme de **complexité logarithmique**. 
    
    Il est donc __extrêmement efficace__, bien plus efficace qu'un algorithme de complexité linéaire.  

    ![graph_test_dichotomie-2.png](images/graph_test_dichotomie.png)
    
    On rencontrera parfois l'expression "complexité en $O(\log_2(t))$".

    
!!! warning ""
    Mais n'oublions pas que l'utilisation d'une recherche dichotomique nécessite que le tableau soit trié, et que le tri d'un tableau est une opération coûteuse (en temps). ***On ne doit donc utiliser cet algorithme que dans le cas où l'on dispose d'un tableau déjà trié.***


## ^^6 - À retenir^^

1. Une recherche dichotomique ne peut se faire que sur un tableau trié.

2. Une recherche dichotomique consiste à systématiquement découper la zone de recherche en deux jusqu'à trouver (ou non) la valeur cherchée :

    - La zone de recherche est délimitée par un indice de début et un indice de fin.
    - On teste si la valeur médiane de cette valeur de recherche est égale à la valeur cherchée.
    - Tant que l'on n'a pas trouvé la valeur cherchée, on restreint la zone de recherche en déplaçant l'indice de début ou l'indice de fin.
    - Si, à l'issue de ces redécoupages successifs, la zone de recherche se réduit à une seule valeur et qu'on a toujours pas trouvé la valeur cherchée, c'est que la valeur est absente du tableau.
  
  
## ^^7 - Exercices^^


!!!question "Exercice 1"
    Adapter le programme de recherche dichotomique pour écrire une fonction `recherche_comptage(tab,val)` qui :

    - prend en entrées un tableau `tab` trié par ordre croissant et d'une valeur `val` 
    - renvoie un couple (_tuple_) `(trouvé,nb_tours)` composé 

        - d'un booléen `trouvé` qui vaut `True` si la valeur `val` appartient au tableau et `False` sinon
        - du nombre entier `nb_tours` égal au nombre de tours de la boucle `while` qu'il a fallu effectuer dans cette recherche dichotomique.
    
    Ne pas oublier de tester votre fonction.

    {{ IDE() }}

!!! question "Exercice 2 : recherche dichotomique dans un tableau de couples"

    Adapter l'algorithme de recherche dichotomique pour pouvoir traiter le tableau de couples de la partie 2, où le premier élément des couples est l'heure de décollage d'un avion et où le tableau est déjà trié par ordre croissant d'horaire.

    {{ IDE('./triDichoCouples_REM') }}



!!! question "Exercice 3 : une autre application de la dichotomie"

    Quand on trace la fonction $f$ définie par la formule $f(x) = x^3+x-4$, on constate que :

    - $f(1) < 0$ et $f(2) >0$
    - la fonction s'annule une fois entre 1 et 2.

    ![graph_fonction.png](./images/graph_fonction.png)

    On veut trouver __une valeur approchée, au millionième près, de ce nombre inconnu__ $x_0$ qui vérifie $f(x_0) = 0$.

    1°) On commence par définir en Python la fonction `f` :


    ```python
    def f(x):
        y = x**3 + x-4
        return y
    ```

    2°) Puis on va procéder par dichotomie : 

    On définit les variables :

    - __debut__ , qui marque le début de la zone de recherche, est égal à 1 au départ ;
    - __fin__ , qui marque la fin de la zone de recherche, est égal à 2 au départ ;
    - __milieu__ est la moyenne entre `debut` et `fin` : `(debut + fin) / 2`

    Et on applique l'algorithme suivant : 

        TANT_QUE fin - debut > 1e-6 # précision demandée de un millionième
            milieu ← (debut + fin) / 2
            SI f(milieu) = 0 ALORS
                AFFICHER milieu
            SINON
                SI f(milieu) < 0 ALORS
                    debut ← milieu
                SINON
                    fin ← milieu
        FIN_TANT_QUE
        AFFICHER milieu
    
    Programmer cet algorithme en Python et déterminer la valeur approchée à $10^{-6}$ de ce nombre $x_0$. 
    
    Combien de tours de boucle `TANT QUE` ont été nécessaires pour obtenir cette valeur de $x_0$ ?

    {{ IDE() }}




## ^^8 - QCM^^

#### Question 1 

Pour pouvoir utiliser un algorithme de recherche par dichotomie dans une liste, quelle précondition doit être vraie ?

{{qcm(["la liste doit être triée","la liste ne doit pas comporter de doublons", "la liste doit comporter uniquement des entiers positifs", "la liste doit être de longueur inférieure à $2^{10}$"],[1]) }}


#### Question 2

On considère le code suivant de recherche d'une valeur dans une liste :
```python
def search(x, y):
    # x est la valeur à chercher
    # y est une liste de valeurs
    for i in range(len(y)):
        if x == y[i]:
            return i
    return None
```

Quel est le coût de cet algorithme ?

{{qcm(["constant", "logarithmique", "linéaire", "quadratique"],[3], shuffle = False)}}


??? danger "Explication"
    On reconnait ici l'algorithme de recherche séquentielle, qui a un coût proportionnel à la taille du tableau : on parle alors d'un coût ***linéaire***.


#### Question 3

La fonction ci-dessous permet d'effectuer une recherche par dichotomie de l'index `m` de l'élément `x` dans un tableau `L` de valeurs distinctes et **triées**.

```python
def dicho(x,L):
    g = 0
    d = len(L)-1
    while g <= d:
        m = (g+d)//2
        if L[m] == x:
            return m
        elif L[m] < x:
            g = m+1
        else:
            d = m-1
    return None
```

Combien de fois la cinquième ligne du code de la fonction (`m = (g+d)//2`) sera-t-elle exécutée dans l'appel `dicho(32, [4, 5, 7, 25, 32, 50, 51, 60])` ?

{{qcm(["1 fois", "2 fois","3 fois","4 fois"],[3], shuffle = False)}}


#### Question 4

On décide d'effectuer une recherche dans un tableau trié contenant 42000 valeurs. 

On procède par dichotomie. Le nombre maximal d'itérations de l'algorithme sera :

{{qcm(["21000 car une recherche dichotomique divise le nombre de tests maximal par deux", "42000 car la valeur recherchée pourrait très bien être la dernière du tableau", "41999 car si on n'a pas trouvé l'élément recherché à l'avant-dernière position du tableau, il n'est plus utile d'effectuer de test pour la dernière position", "16 car à chaque itération, le nombre d'éléments à examiner est divisé par deux et que $2^{15} \leqslant 42000 < 2^{16}$"],[4])}}



#### Question 5

Un algorithme de recherche dichotomique dans une liste triée de taille $n$ nécessite, dans le pire des cas, exactement $k$ comparaisons.

Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille $2\times n$ ?

{{qcm(["$k$", "$k+1$", "2 × $k$", "2 ×$(k+1)$"],[2], shuffle = False)}}


## K plus proches voisins

L’algorithme des k plus proches voisins appartient à la famille des algorithmes d’apprentissage automatique (machine learning) qui constituent le poumon de l'intelligence artificielle actuellement.

Pour simplifier, l'apprentissage automatique part souvent de données (data) et essaye de dire quelque chose des données qui n'ont pas encore été vues : il s'agit de généraliser, de prédire.

On va utiliser l'algorithme des k plus proches voisins pour résoudre un problème de classification : prédire la classe d'une donnée inconnue à partir de la classe des données connues.

[lien vers travail Capytal](https://capytale2.ac-paris.fr/web/c/572c-3214250/mlc)