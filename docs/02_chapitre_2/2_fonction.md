---
author: Jean-Marc Naulin
title: Représentation des données, types et valeurs de base
---
[Lien du site à télécharger pour la classe](scripts/NSI.url)


Toute machine informatique manipule une représentation des données dont l’unité minimale
est le bit 0/1, ce qui permet d’unifier logique et calcul. 
Dans ce chapitre nous allons voir comment sont codées les données de base qui sont
représentées selon un codage dépendant de leur nature : 
entiers, flottants, caractères et chaînes de caractères. 
Le codage conditionne la taille des différentes valeurs en mémoire. 

## I. Introduction aux Bases Numériques et Écriture d'un Entier Positif


!!! info "Objectifs du cours :"

    * Comprendre les concepts de base des systèmes de numération.
    * Apprendre à convertir des nombres d'une base à une autre.
    * Maîtriser l'écriture d'un entier positif dans une base b≥2.
    * Appliquer ces connaissances à des problèmes du monde réel.

### 1. Introduction aux bases numériques :

Dans le domaine des mathématiques et de l'informatique, les bases numériques (ou systèmes de numération) sont des méthodes pour représenter les nombres en utilisant des symboles ou des chiffres. Chaque base numérique est caractérisée par sa propre collection de symboles et par les règles de notation pour représenter les valeurs numériques. Cette section du cours se concentre sur l'explication des bases numériques, leur importance et leurs applications.

#### 1.1. Définition des bases numériques :

Une base numérique est un système utilisé pour représenter des nombres en utilisant une combinaison de symboles.
Chaque base a un nombre fixe de symboles distincts appelés "chiffres" qui sont utilisés pour représenter les différentes valeurs.
Le choix de la base détermine comment les nombres sont écrits, lus, et comment les opérations mathématiques sont effectuées.

#### 1.2. Bases numériques courantes :

- Base décimale (base 10) : Utilise les chiffres de 0 à 9. C'est le système de numération le plus couramment utilisé dans la vie quotidienne.
- Base binaire (base 2) : Utilise les chiffres 0 et 1. Fondamental en informatique et en électronique, car il représente l'état binaire des composants électroniques.
- Base octale (base 8) : Utilise les chiffres de 0 à 7. Utilisée parfois en programmation et en informatique.
- Base hexadécimale (base 16) : Utilise les chiffres de 0 à 9 et les lettres A à F pour représenter les valeurs de 10 à 15. Fréquemment utilisée en informatique pour représenter des valeurs binaires de manière plus compacte.

#### 1.3. Avantages des bases numériques :

Flexibilité de représentation : Les différentes bases offrent des moyens variés de représenter des nombres, ce qui peut être utile pour diverses applications.
Compression de données : Certaines bases, comme la base hexadécimale, permettent de compresser efficacement de grandes valeurs binaires en une forme plus concise.
Facilité de conversion : La connaissance des bases permet de convertir des nombres d'un système à un autre, ce qui est important dans les domaines tels que la programmation et l'ingénierie.

#### 1.4. Applications des bases numériques :


!!! tag "Applications :"

    - Informatique : Les ordinateurs utilisent la base binaire pour stocker et traiter l'information.
    - Cryptographie : Les systèmes de cryptage utilisent des opérations sur des nombres dans différentes bases pour assurer la sécurité des données.
    - Sciences et ingénierie : Les bases numériques sont utilisées dans divers calculs scientifiques et ingénierie, notamment en électronique, en traitement du signal et en mathématiques discrètes.
    
En comprenant les bases numériques et en étant capable de convertir entre différentes bases, vous serez mieux équipé pour comprendre le fonctionnement interne des systèmes numériques et pour résoudre des problèmes pratiques dans divers domaines. Cette section posera les bases pour la suite du cours, où nous explorerons en détail la conversion entre différentes bases et l'écriture d'entiers positifs dans une base donnée.



### 2. Systèmes de numération

Dans cette section, nous plongerons plus profondément dans les différents systèmes de numération et apprendrons comment convertir des nombres d'une base à une autre.

### 2.1. Notation positionnelle

- La notation positionnelle est le principe selon lequel la valeur d'un chiffre dans un nombre dépend de sa position dans le nombre.
- Chaque position représente une puissance de la base. Par exemple, dans le nombre décimal "425", le chiffre "5" est dans la position des unités, "2" est dans la position des dizaines et "4" est dans la position des centaines.

### 2.2. Comparaison des bases

- Les bases numériques ont des nombres différents de chiffres, ce qui affecte la manière dont les nombres sont écrits et lus.
- La valeur de chaque position est multipliée par une puissance de la base. Par exemple, en base 10, la valeur d'une position est \(10^{\text{position}}\).

### 2.3. Conversion entre les bases décimale et binaire

Regardons d'abord dans la console Python ce que l'on peut faire :

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from=https://naulin.forge.apps.education.fr/naulin/scripts/EntierPositif.ipynb"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


Apprenons à convertir à la main :


|[![texte alternatif de l'image](http://img.youtube.com/vi/T2bdGEggkIY/0.jpg){: .center}](https://youtu.be/T2bdGEggkIY "Titre de la video")


- Pour convertir un nombre décimal en binaire, divisez le nombre par 2 successivement et notez les restes. Le résultat binaire est obtenu en lisant les restes de bas en haut.

  Exemple : Convertir \(19_{10}\) en binaire :

  \(19 \div 2 = 9 \text{ reste } 1\) <br>

  \(9 \div 2 = 4 \text{ reste } 1\) <br>

  \(4 \div 2 = 2 \text{ reste } 0\) <br>

  \(2 \div 2 = 1 \text{ reste } 0\) <br>
  
  \(1 \div 2 = 0 \text{ reste } 1\) <br>
  
  En lisant les restes de bas en haut, \(19_{10} = 10011_2\).

#### Exercice 1

Convertir \(47_{10}\) en binaire.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>101111</p>
</details>

Convertir \(124_{10}\) en binaire.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>1111100</p>
</details>

Convertir \(1025_{10}\) en binaire.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>10000000001</p>
</details>

Convertir \(4536_{10}\) en binaire.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>1000110111000</p>
</details>

### 2.4. Conversion d'un nombre binaire en une autre base (10 et 16)


Vers base 10 :

C'est plus simple dans ce sens-là que dans l'autre.

Prenons un nombre au hasard, tel que 11010011. Il s'étale sur 8 rangs, et comme dit précédemment, chaque rang correspond à une puissance de deux.
Le premier rang (en partant de la droite) est le rang 0, le second est le 1, etc.

Pour convertir le tout en décimale, on procède de la manière suivante : on multiplie par \(2^0\) la valeur du rang 0, par \(2^1\) la valeur du rang 1, par \(2^2\) la valeur du rang 2, [...], par \(2^{10}\) la valeur du rang 10, etc.

Après ça, il ne reste plus qu'à remplacer les puissances de 2 par leurs valeurs et de calculer la somme : (Attention à bien partir de la droite !)
11010011 binaire =1∗1+1∗2+0∗4+0∗8+1∗16+0∗32+1∗64+1∗128=211 décimal

À faire vous-même :

Convertir \(10011001_2\) en base 10

 <details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>153</p>
</details>

Vers base 16 :

Le binaire, c'est bien pratique : on peut coder des nombres uniquement avec des 0 et des 1. C'est bien pour les signaux électriques et tout le bazar, mais dans la vie de tous les jours c'est pas bien facile d'utilisation. On utilise couramment la base 10. Le problème c'est qu'en informatique, tout est basé sur le binaire, et étant une base d'indice 2, c'est plus aisé d'encoder les informations sur un nombre multiple de 2. On utilise donc souvent la base 16, appelé système hexadécimal (hexa = 6, déci = 10, 16 = 6 + 10) car 16 est un multiple de 2, et qu'il permet de représenter 8 bits avec seulement 2 chiffres. Ça paraît simple, mais il y a un autre problème : en base 10, on utilise 10 chiffres. En base 2 (binaire) on utilise seulement 2 chiffres : 0 et 1. Mais du coup, en base 16, il faut 16 chiffres. OK, 0 1 2 3 4 5 6 7 8 9.. quoi après ? On prend des lettres de l'alphabet.
Ce qui donne :
0 1 2 3 4 5 6 7 8 9 A B C D E F On peut établir une liste de correspondances entre la base 10 et la base 16 (voire même la base 2) :

![morse](images/Hexadecimal.png){ width=20%; : .center }

On effectue le remplacement de droite à gauche de 4 bits par le chiffre hexadecimal correspondant.<br>

Si le nombre de bits n'est pas un multiple de 4, on complète à gauche par des 0.


  Exemple : Convertir \(1101_2\) en base 16 :

  \(1101_2 = 1101_2=D_{16}\) <br>


#### Exercice 2

Convertir \(101101_2\) en base 16.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>2D</p>
</details>


Convertir \(132_{10}\) en base binaire, puis en base hexadécimale.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>10000100 en binaire et 84 en hexadecimal</p>
</details>


Convertir \(5A_{16}\) en base binaire.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>1011010 en binaire</p>
</details>

Programmons en Python :

<div class="centre" markdown="span">
<iframe 
src="https://notebook.basthon.fr/?from=https://naulin.forge.apps.education.fr/naulin/scripts/Convertisseur.ipynb"
width="900" height="600" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>




Petit memo pour les conversions : 

![morse](images/Conversions.png){ width=100%; : .center }

!!! info "Objectifs du cours :"

    * Évaluer le nombre de bits nécessaires à l’écriture en base 2 d’un entier.
    * Utiliser le complément à 2.
    * Représentation binaire d’un entier relatif.


## II. Nombre de bits nécessaires pour représenter un entier et addition

Pour trouver le nombre de bits minimum nécessaires pour écrire en base 2 un entier positif, il faut
trouver la plus petite puissance de 2 qui soit strictement supérieure à l'entier à écrire.

Exemple :

\(2^7=128_{10}\) et  \(2^8=256_{10}\), on a \(2^7<200_{10}<2^8\)
donc 8 bits sont nécessaires pour écrire 200 en binaire.

### addition :

Pour additioner deux entiers positifs en base binaire, l'algorithme est le même
qu'en base décimale, en utilisant la table d'addition ci-dessous :

\(0_2+0_2=0_2\)

\(0_2+1_2=1_2\)
    
\(1_2+1_2=0_2\) et 1 de retenue

![morse](images/calculbinaire.png){ width=100%; : .center }


## II. Représentation binaire d’un entier relatif



|[![texte alternatif de l'image](http://img.youtube.com/vi/35MRaZNOl2U/0.jpg){: .center}](https://www.youtube.com/watch?v=35MRaZNOl2U "Titre de la video")




La première idée qui pourrait nous venir à l'esprit est, sur un nombre comportant n bits, d'utiliser 1 bit pour représenter le signe et n-1 bit pour représenter la valeur absolue du nombre à représenter. Le bit de signe étant le bit dit "de poids fort" (c'est à dire le bit le plus à gauche), ce bit de poids fort serait à 0 dans le cas d'un nombre positif et à 1 dans le cas d'un nombre négatif.

un exemple : on représente l'entier 5 sur 8 bits par 00000101, -5 serait donc représenté par 10000101

Il existe un énorme inconvénient à cette méthode : l'existence de deux zéros, un zéro positif (00000000) et un zéro négatif (10000000) !

Ce problème est, pour plusieurs raisons qui ne seront pas développées ici, rédhibitoire. Nous allons donc devoir utiliser une autre méthode : le complément à deux

### Le complément à 2

Avant de représenter un entier relatif, il est nécessaire de définir le nombre de bits qui seront utilisés pour cette représentation (souvent 8, 16 , 32 ou 64 bits)

Prenons tout de suite un exemple : déterminons la représentation de -12 sur 8 bits

- Commençons par représenter 12 sur 8 bits (sachant que pour représenter 12 en binaire seuls 4 bits sont nécessaire, les 4 bits les plus à gauche seront à 0) : 00001100

- Inversons tous les bits (les bits à 1 passent à 0 et vice versa) : 11110011

- Ajoutons 1 au nombre obtenu à l'étape précédente :  les retenues sont notées en rouge

![morse](images/calcul.png){ width=10%; : .center }

- La représentation de -12 sur 8 bits est donc : 11110100

Comment peut-on être sûr que 11110100 est bien la représentation de -12 ?

Nous pouvons affirmer sans trop de risque de nous tromper que 12 + (-12) = 0, vérifions que cela est vrai pour notre représentation sur 8 bits.


![morse](images/calcul1.png){ width=10%; : .center }

Dans l'opération ci-dessus, nous avons un 1 pour le 9e bit, mais comme notre représentation se limite à 8 bits, il nous reste bien 00000000.

À faire vous-même 2 :

En utilisant le complément à 2, représentez -15 (représentation sur 8 bits)

l faut noter qu'il est facile de déterminer si une représentation correspond à un entier positif ou un entier négatif : si le bit de poids fort est à 1, nous avons affaire à un entier négatif, si le bit de poids fort est à 0, nous avons affaire à un entier positif.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>11110001 en binaire</p>
</details>



À faire vous-même 3 :

Représentez sur 8 bits l'entier 4 puis représentez, toujours sur 8 bits, l'entier -5. Additionnez ces 2 nombres (en utilisant les représentations binaires bien évidemment), vérifiez que vous obtenez bien -1.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>11111011 en binaire est -5</p>
  <p>00000100 en binaire est 4</p>
  <p> on obtient 11111111 en binaire par addition qui est la représentation de -1 </p>
</details>

[Lien du Test du 25/09/2023](https://genumsi.inria.fr/qcm.php?h=ce6412620dd60f20419348f8fad0cbef)



## III. Représentation approximative des nombres réels : notion de nombre flottant

[![texte alternatif de l'image](http://img.youtube.com/vi/QebXGpNHF7I/0.jpg){: .center}](https://youtu.be/QebXGpNHF7I "Titre de la video")


Nous avons vu comment représenter les entiers, naturels ou relatifs. Intéressons nous maintenant
aux nombres réels, que nous appelons flottants en informatique.

### Ecriture d’un nombre flottant en base deux

En base 10 , le nombre 61,154 est sous forme décomposée \(6*10^1+1*10^0+1*10^{-1}+5*10^{-2}+4*10^{-3}\)

De même, en base deux 1101,101 signifie \(1*2^3+1*2^2+0*2^1+1*2^0+1*2^{-1}+0*2^{-2}+1*2^{-3}=13,625\)

Il est plus difficile de passer de la base 10 à la base 2.

Exemple : Ecrire 61,154 en base 2.

Nous n’avons aucun problème à écrire 61 en base deux : 111101

Comment exprimer 0,154 en base 2 ?

|   successivement         | on garde   |
| ------ | ------ |
|0,154 *2 =0.308| 0 | 
|0.308*2=0.616| 0 | 
|0.616*2=1.232| 1 | 
|0.232*2=0.464| 0 | 
|0.464*2=0.928| 0 | 
|0.928*2=1.856 |1 | 
|0.856*2=1.712 |1 | 
|0.712*2=1.424| 1  |
|0.424*2=0.848| 0  | 
...

On obtient donc 61,154 =111101,00100111…. en base 2

Exercices  
  1) Trouver la représentation décimale de 1101101,011  
  2) Trouver la représentation binaire de 24,625  
  Remarques importantes : En base 10, \(61,154 = 6,154*10^1\)  et \(0.0061154= 6.154*10^{-3} \) 

Il en va de même en base 2 : \(1101,1101= 1,1011101*2^{11}\), l’exposant 11 correspondant à un décalage
de 3 vers la droite de la virgule.  
De même \(0.0011=1,1*2^{-11}\), l’exposant -11 correspondant à un décalage de 3 vers la gauche.  


!!! warning "Attention :"
    Un nombre à développement décimal fini ne l’est pas forcément en base 2.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Pour convertir le nombre binaire 1101101,011 en décimal, vous devez comprendre la position de chaque chiffre binaire dans la notation décimale. La partie entière est à gauche de la virgule, et la partie fractionnaire est à droite de la virgule.  

Partie entière : 1101101  
Partie fractionnaire : 011  
Commençons par convertir la partie entière en décimal :  

Partie entière (1101101) en décimal :  

1 * 2^6 + 1 * 2^5 + 0 * 2^4 + 1 * 2^3 + 1 * 2^2 + 0 * 2^1 + 1 * 2^0  
= 64 + 32 + 0 + 8 + 4 + 0 + 1  
= 109  

Maintenant, convertissons la partie fractionnaire en décimal. Chaque chiffre binaire dans la partie fractionnaire représente une puissance de 2 négative, en commençant par -1 pour le chiffre le plus à gauche et en diminuant d'une unité pour chaque chiffre suivant.  

Partie fractionnaire (011) en décimal :  

0 * 2^(-1) + 1 * 2^(-2) + 1 * 2^(-3)  
= 0 + 0.25 + 0.125  
= 0.375  

Maintenant, additionnons la partie entière et la partie fractionnaire pour obtenir le nombre décimal complet :  

109 (partie entière) + 0.375 (partie fractionnaire) = 109.375  

Donc, la représentation décimale du nombre binaire 1101101,011 est 109.375.  

  </p>
  <p>Pour trouver la représentation binaire de 24,625 en utilisant la notation à virgule flottante, vous pouvez suivre les étapes suivantes :    

Divisez le nombre en sa partie entière et sa partie fractionnaire :    

Partie entière : 24    
Partie fractionnaire : 0,625    
Convertissez la partie entière en binaire en utilisant la méthode de la division successive. Voici comment vous pouvez le faire pour 24 en base 10 :    

24 ÷ 2 = 12 avec un reste de 0    
12 ÷ 2 = 6 avec un reste de 0    
6 ÷ 2 = 3 avec un reste de 0    
3 ÷ 2 = 1 avec un reste de 1    
1 ÷ 2 = 0 avec un reste de 1 (on s'arrête lorsque le quotient est 0)    
En lisant les restes de bas en haut, la partie entière en binaire est 11000.    

Pour la partie fractionnaire, convertissez-la en binaire en multipliant par 2 de manière répétée et en enregistrant la partie entière à chaque étape.     Arrêtez-vous lorsque la partie fractionnaire atteint zéro ou lorsque vous avez obtenu le nombre de chiffres binaires souhaité.    

0,625 * 2 = 1,25 (partie entière : 1)    
0,25 * 2 = 0,5 (partie entière : 0)    
0,5 * 2 = 1 (partie entière : 1)    
0 (partie entière : 0)    
En lisant les parties entières de haut en bas, la partie fractionnaire en binaire est 101.    

Maintenant, combinez la partie entière et la partie fractionnaire avec un point entre elles pour obtenir la représentation binaire complète :    

Partie entière (en binaire) : 11000    
Partie fractionnaire (en binaire) : 101    
Donc, la représentation binaire de 24,625 est 11000,101. </p>
</details>

Exercices    
  1. Montrer que 0.1 n’admet pas de développement fini en base 2.    
  2. Déterminer un décimal avec trois chiffres après la virgule qui admet un développement fini
  en base 2. Donner ce développement.   

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>0.125 en base 10 est équivalent à 0.001 en base 2. Ce nombre a trois chiffres après la virgule en base 2 et admet un développement fini.</p>
</details>






### Représentation en machine d’un nombre flottant

Les machines utilisent une norme pour coder les nombres réels : La Norme IEEE 754. Pour cela , le
nombre devra préalablement être écrit sous la forme \(1,XXXXX.2^e\)
(avec e l'exposant)  
  * Le nombre est codé sur 32 bits  
  * Le bit de poids le plus fort sert à coder le signe (0 pour positif, 1 pour négatif)  
  * L’exposant est codé sur les 8 bits consécutifs au signe.  
  * La mantisse est codée sur les 23 bits restants et correspondant aux bits situés après la
virgule.  
La principale difficulté vient du codage de l’exposant : En effet, il nous faut pouvoir coder les
exposants négatifs. Nous avons 8 bits et pouvons alors coder théoriquement 256 valeurs. Deux
valeurs étant réservées, il nous reste 254 valeurs, que l’on fait varier de -126 à 127. Pour que
l’exposant soit toujours positif, on ajoute 127 à la valeur de l’exposant.

![morse](images/mantisse.png){: .center }

Exemple : Codons 1101,10110110.  
Etape 1 : On transforme l’écriture pour se conformer à la norme IEEE 754 : \(1,101101110110*2^{11}\)
Etape 2 : L’exposant à coder est 127+3 = 130 =10000010. Le signe est positif dans le bit le plus à
gauche sera 0.  
On obtient 01000001010110111011000000000000  

Exercices
1) Donner la représentation en machine sur 32 bits de 214,875. De 1/32.  
2) Quelle est l’écriture décimale de 11011010101101111000000000000000 ? Quelle est l’écriture
hexadécimale de ce nombre ?  


Remarque : Le codage sur 32 bits est appelé « simple précision ». Il existe aussi le codage sur 64 bits
appelé « double précision ». L’exposant est alors codé sur 11 bits et le décalage est de 1023 et non
plus 127.


**Les enjeux du codage des nombres :**

!!! warning "Ariane 5 en 1996"
    Le 4 juin 1996, la fusée Ariane 5 a été détruite en vol 37 s après son lancement, suite à un problème
    dans la centrale inertielle. Déjà utilisée pour Ariane 4, le système n'était plus adapté à la puissance
    de la nouvelle fusée.

[![texte alternatif de l'image](http://img.youtube.com/vi/5tJPXYA0Nec/0.jpg){: .center}](https://youtu.be/5tJPXYA0Nec "Titre de la video")

!!! warning "Bourse de Vancouver en 1982"
    Au début de l'année 1982, la bourse de Vancouver avait lancé un indice boursier électronique initialement fixé à une valeur de 1.000 points. En deux ans, il était tombé à la moitié de sa valeur initiale, ce qui était une tendance déroutante dans le marché haussier du début des années 1980.

    Une enquête révéla que les calculs de l'indice étaient erronés à cause d'une simple commande, utilisant floor() au lieu de round(). Cela signifiait qu'au lieu d'être arrondie à la troisième décimale, la valeur était tronquée (les ordinateurs ont nécessairement une résolution finie, ce qui nécessite d'arrondir ou de tronquer). Ainsi, si l'indice était calculé à 532,7528, il était enregistré à 532,752 au lieu d'être arrondi à 532,753.

    Le problème fut qu'à raison de plusieurs milliers de calculs par jour, cette différence d'apparence minime (il s'agissait essentiellement d'arrondis à la baisse) finit par entraîner une perte de valeur considérable. L'erreur de programmation fut finalement corrigée en novembre 1983, après une fermeture un vendredi avec un indice à environ 500. Lors de la réouverture, le lundi, il était à plus de 1.000, la valeur perdue ayant été rétablie.


[![texte alternatif de l'image](http://img.youtube.com/vi/RdDIANVl7dc/0.jpg){: .center}](https://youtu.be/RdDIANVl7dc "Titre de la video")

!!! warning "Missile antipatriot 1991 Irak"
    Le 25 février 1991, pendant la Guerre du Golfe, une batterie américaine de missiles Patriot, à Dharan (Arabie Saoudite), a échoué dans l'interception d'un missile Scud irakien. Le Scud a frappé un baraquement de l'armée américaine et a tué 28 soldats. La commission d'enquête a conclu à un calcul incorrect du temps de parcours, dû à un problème d'arrondi. Les nombres étaient représentés en virgule fixe sur 24 bits. Le temps était compté par l'horloge interne du système en 1/10 de seconde. Malheureusement, 1/10 n'a pas d'écriture finie dans le système binaire : 1/10 = 0,1 (dans le système décimal) = 0,0001100110011001100110011... (dans le système binaire). L'ordinateur de bord arrondissait 1/10 à 24 chiffres, d'où une petite erreur dans le décompte du temps pour chaque 1/10 de seconde. Au moment de l'attaque, la batterie de missile Patriot était allumée depuis environ 100 heures, ce qui avait entraîné une accumulation des erreurs d'arrondi de 0,34 s. Pendant ce temps, un missile Scud parcourt environ 500 m, ce qui explique que le Patriot soit passé à côté de sa cible.

[![texte alternatif de l'image](http://img.youtube.com/vi/_Dbd3z8t9qc/0.jpg){: .center}](https://youtu.be/_Dbd3z8t9qc "Titre de la video")



## IV. Valeurs booléennes : 0,1. Opérateurs booléens : and, or, not. Expressions booléennes


[![texte alternatif de l'image](http://img.youtube.com/vi/0QBV9U4H7p4/0.jpg){: .center}](https://www.dailymotion.com/video/x71hxwp "Titre de la video")

Lien du TD  : [TD](https://capytale2.ac-paris.fr/web/c/8993-2183424)

**Exercices :**


!!! tag "Exercice 1 :"  
    Construire des tables de vérité  

    Produisez les tables de vérité pour les opérations suivantes:  

    NAND : P NAND Q=!(P&Q)  
    NOR :  P NOR Q=!(P∣Q)  

<details>
<summary>Cliquez pour afficher la solution</summary>
<table>
  <thead>
    <tr>
      <th>P</th>
      <th>Q</th>
      <th>P AND Q</th>
      <th>NOT (P AND Q)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>


<table>
  <thead>
    <tr>
      <th>P</th>
      <th>Q</th>
      <th>P OR Q</th>
      <th>NOT (P OR Q)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>


</details>




!!! tag "Exercice 2 :"  
    Expressions équivalentes  

    Montrer que:  P∣(!P&Q)=P∣Q

    Montrer que:  P∣Q&R=(P∣Q)&(P∣R)

<details>
<summary>Cliquez pour afficher la solution</summary>
<!DOCTYPE html>
<html>
<head>
  <style>
    table {
      border-collapse: collapse;
      width: 50%;
    }

    table, th, td {
      border: 1px solid black;
      text-align: center;
    }
  </style>
</head>
<body>

<h2>Démonstration de l'équivalence : P∣(!P&Q)=P∣Q</h2>

<table>
  <thead>
    <tr>
      <th>P</th>
      <th>Q</th>
      <th>¬P</th>
      <th>¬P ∧ Q</th>
      <th>P ∣ (¬P ∧ Q)</th>
      <th>P ∣ Q</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>


<p>Comme le montre la table de vérité, P∣(!P&Q) est équivalent à P∣Q pour toutes les combinaisons possibles de valeurs de vérité de P et Q.</p>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <style>
    table {
      border-collapse: collapse;
      width: 60%;
    }

    table, th, td {
      border: 1px solid black;
      text-align: center;
    }
  </style>
</head>
<body>

<!DOCTYPE html>
<html>
<head>
  <style>
    table {
      border-collapse: collapse;
      width: 60%;
    }

    table, th, td {
      border: 1px solid black;
      text-align: center;
    }
  </style>
</head>
<body>

<h2>Démonstration de l'équivalence : P∣Q&R=(P∣Q)&(P∣R)</h2>

<table>
  <thead>
    <tr>
      <th>P</th>
      <th>Q</th>
      <th>R</th>
      <th>Q ∧ R</th>
      <th>P ∣ (Q ∧ R)</th>
      <th>P ∣ Q</th>
      <th>P ∣ R</th>
      <th>(P ∣ Q) ∧ (P ∣ R)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>

<p>Comme le montre la table de vérité, P∣Q&R est équivalent à (P∣Q)&(P∣R) pour toutes les combinaisons possibles de valeurs de vérité de P, Q et R.</p>

</body>
</html>

</details>


!!! tag "Exercice 3 :"  
    Recherche d’expressions équivalentes (Plus dur)  

    Exprimer sous forme simplifiée l'expression suivante:  

    (P&Q&R)∣(P&Q&S)  
   

<details>
<summary>Cliquez pour afficher la solution</summary>


<h2>Explication de la Simplification de (P&Q&R)∣(P&Q&S)  </h2>

<p>1. <strong>Factorisation :</strong> L'expression initiale est (P&Q&R)∣(P&Q&S)  .</p>

<p>2. <strong>Regroupement :</strong> Factorisez les termes communs P&Q :</p>
<blockquote>
   (P&Q&R)|(P&Q&S)
</blockquote>

<p>3. <strong>Utilisation de la distributivité :</strong> Appliquez la distributivité de & sur | :</p>
<blockquote>
   (P&Q)&(R|S)

<p>4. <strong>Conclusion :</strong> Ainsi, l'expression est équivalente à   (P&Q)&(R|S).</p>



</details>

**Associativité :**  

A+(B+C)=(A+B)+C (associativité pour la disjonction)  
A⋅(B⋅C)=(A⋅B)⋅C (associativité pour la conjonction)  

**Distributivité :**  

A⋅(B+C)=(A⋅B)+(A⋅C) (distributivité de la conjonction sur la disjonction)  
A+(B⋅C)=(A+B)⋅(A+C) (distributivité de la disjonction sur la conjonction)  

**Loi de De Morgan :**  

¬(A⋅B)=¬A+¬B (De Morgan pour la conjonction)  
¬(A+B)=¬A⋅¬B (De Morgan pour la disjonction)  

Ces règles sont fondamentales pour simplifier des expressions booléennes, démontrer des équivalences logiques, et pour concevoir et analyser des circuits logiques. Elles forment la base de l'algèbre de Boole, qui est largement utilisée dans la conception des systèmes logiques et informatiques.


vous pouvez vérifier vos résultats en Python en utilisant la bibliothèque sympy qui propose la fonction simplify_logic  
(Attention la syntaxe utilisée est: ET: &, OU: | et NON:~`).

???+ question "Exemple :"

    {{ IDE('scripts/sympy') }}

### Une application : Les additionneurs

Nous allons dans ce chapitre voir comment est en principe construit un circuit pour l'addition de 2 nombres en binaire.
Ce circuit étant assez complexe, nous allons le réaliser en plusieurs étapes :

* Le demi additionneur fera une simple addition de deux bits.
* Le plein additionneur devra ajouter à cette addition celle d'un report précédent
* Enfin nous assemblerons n additionneurs pour faire l'addition de nombres de n bits

<html>
<h2 id="DemiAdd">Le demi additionneur - <em>half adder</em></h2>
			<p>Addition de 2 bits </p>
			<div style="margin: 12px 0px 8px 6ex;">
				<table>
					<tr>
						<td width="287">
							<!-- <img src="Logique/DemiAdd.png" width="220" height="180" alt="" /> -->

							<form name="FDemiAdd" style="position: relative; width: 250px;" action="">
								<script type="text/javascript" language="javascript">
									// <!--
									function sw_DemiAdd(entree) {
										document.getElementById(entree).value = (document.getElementById(entree).value == 0) ? 1 : 0;
										var A1 = document.getElementById("A_1").value;
										var B1 = document.getElementById("B_1").value;
										document.getElementById("S_1").value = A1 ^ B1;
										document.getElementById("R_1").value = A1 & B1;
									}
									function set_DemiAdd(A1, B1) {
										document.getElementById("A_1").value = A1;
										document.getElementById("B_1").value = B1;
										document.getElementById("S_1").value = A1 ^ B1;
										document.getElementById("R_1").value = A1 & B1;
									}
									// -->	
								</script>
								<img src="../images/DemiAdditionneur.png" width="250" height="180" alt="Circuit demi additionneur" title="Cliquez sur les entrées pour les modifier" />

								<div style="position: absolute; left: 10px; top: 20px; width: 60px;">A
									<input id="A_1" class="in" size="1" value="0" onclick="sw_DemiAdd('A_1')" />
								</div>
								<div style="position: absolute; left: 10px; top: 50px; width: 60px;">B
									<input id="B_1" class="in" size="1" value="0" onclick="sw_DemiAdd('B_1')" />
								</div>

								<div style="position: absolute; left: 210px; top: 35px; width: 60px; color: #0033FF">
									S
									<input id="S_1" class="out" size="1" value="0" disabled="disabled" />
								</div>
								<div style="position: absolute; left: 210px; top: 125px; width: 60px; color: #FF0000">
									R
									<input id="R_1" class="out" size="1" value="0" disabled="disabled" />
								</div>

								<div style="position: absolute; left: -4px; top: 123px; width: 60px;">
									<img src="../images/Mouse.gif" width="24" height="24" align="left" alt=""
										onmouseover="overlib('Cliquez sur les entrées pour les faire basculer',
					WIDTH,'250',HAUTO,VAUTO,FGCOLOR,'#FFFFCC',BORDER,'1');"
										onmouseout="nd();" />
								</div>
							</form>
							<!-- // FDemiAdd -->

						</td>
						<td width="116">S = A xor B<br />
							&nbsp;&nbsp;&nbsp;est la somme<br />
							<br />
							R = A and B<br />
							&nbsp;&nbsp;&nbsp;est le report</td>
						<td width="123">
							<img src="../images/TblV_DemiAdd.png" width="120" height="80" align="middle"
								name="TblVDemiAdd" usemap="#MapTblVDemiAdd" border="0" alt="" />
							<map name="MapTblVDemiAdd" id="MapTblVDemiAdd">
								<area shape="rect" coords="0,16,120,32" alt="" href="javascript:void(0)" onclick="set_DemiAdd(0,0)" />
								<area shape="rect" coords="0,32,120,48" alt="" href="javascript:void(0)" onclick="set_DemiAdd(0,1)" />
								<area shape="rect" coords="0,48,120,64" alt="" href="javascript:void(0)" onclick="set_DemiAdd(1,0)" />
								<area shape="rect" coords="0,64,120,80" alt="" href="javascript:void(0)" onclick="set_DemiAdd(1,1)" />
							</map>
						</td>
					</tr>
				</table>
			</div>
			<p>
				Le demi additionneur effectue la somme de deux bits. S est la somme et R 
		le report. (<i>carry</i>)<br />
				Ce schéma n'est cependant pas suffisant pour réaliser la somme de nombres 
				de plusieurs bits. Il faut alors tenir compte du report de l'addition des bits précédents.
			</p>
</html>

<html>
<h2 id="PleinAdd">Le plein additionneur - <em>Full adder</em></h2>

			<p>Addition de 2 bits en tenant compte du report précédent </p>

			<table>
				<tr>
					<td valign="top">
						<p>
							Exemple : 	&nbsp; Calculons 1 + 3	.
				  &nbsp; &nbsp; En&nbsp;binaire&nbsp;cela&nbsp;donne&nbsp;:&nbsp;&nbsp;0001&nbsp;+&nbsp;0011
						</p>
					</td>
					<td>
						<img src="../images/Addition4bits.png" alt="" /></td>
				</tr>
			</table>

			<p>
				L&#39;addition des bits de droite est une addition de deux bits,
			elle peut être réalisée avec le demi additionneur. 
			Pour les bits suivants par contre, il faut tenir compte d&#39;un éventuel report.
			<br />
				Ainsi dès le deuxième bit de notre exemple (en comptant les bits de droite à gauche) il a fallu faire 2 additions (<span class="Style2"> 1</span> + 0 + 1 = 10  &nbsp; &quot; on pose <span class="balise">0</span> et on reporte <span class="Style2">1</span>&quot;)
			</p>


</html>

[Le plein additionneur](https://courstechinfo.be/MathInfo/Additionneur.html)

## V. Représentation d’un texte en machine

Un ordinateur est uniquement capable de traiter des données binaires.

### I. ASCII

Avant 1960 de nombreux systèmes de codage de caractères existaient, ils étaient souvent incompatibles entre eux. En 1960, l'organisation internationale de normalisation (ISO) décide de mettre un peu d'ordre dans ce bazar en créant la norme ASCII (American Standard Code for Information Interchange).
Cette norme ne définissait que 27 = 128 codes (une longueur 7 bits, codes de 0000 0000 à 0111 1111).

La norme ASCII convient bien à la langue anglaise, mais pose des problèmes dans d'autres langues, par exemple le français. En effet l'ASCII ne prévoit pas d'encoder les lettres accentuées.

![morse](https://i.ibb.co/H2hgpyt/ascii.png){ width=80%; : .center }

33 caractères correspondent à des caractères de contrôle et 95 caractères sont imprimables :

* les chiffres de 0 à 9,
* les lettres minuscules de a à z et les majuscules de A à Z,
* des symboles mathématiques et de ponctuation.
La représentation binaire du caractère A majuscule est (1000001)2 ou (65)10 et sa représentation hexadécimale est (41)16 .

Entraînement 1 :

Quel est le code binaire du "a" minuscule en ASCII?  
Coder vote prénom (en commençant avec une majuscule) à l’aide du code ASCII.  


<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>01100001</p>
 
</details>




### II. ISO-8859-1

La norme ISO-8859-1, également connue sous le nom de Latin-1, est une norme de codage de caractères qui définit un ensemble de 256 caractères utilisables dans les ordinateurs. Cette norme est principalement utilisée pour représenter les caractères alphabétiques européens, tels que les lettres avec accents, les caractères de ponctuation et les symboles mathématiques.

La norme ISO-8859-1 est souvent utilisée dans les applications informatiques, les pages web et les documents électroniques en Europe et en Amérique du Nord. Elle est également souvent utilisée comme codage de caractères par défaut pour les systèmes d'exploitation et les logiciels.

Les caractères de la norme ISO-8859-1 sont codés sur un octet, c'est-à-dire qu'ils sont représentés par 8 bits. Chaque caractère a un code unique qui est compris entre 0 et 255. Les premiers 128 caractères sont identiques aux caractères ASCII, ce qui signifie qu'ils sont compatibles avec le codage ASCII.

La norme ISO-8859-1 est donc une norme importante dans le monde de l'informatique et est utilisée pour représenter de nombreux caractères dans différents langages.

### III. Unicode

L'Unicode est un standard informatique qui permet de représenter de manière unique tous les caractères utilisés dans les différentes écritures du monde entier, qu'il s'agisse de caractères latins, cyrilliques, arabes, chinois, etc.

Le but de l'Unicode est de fournir une méthode universelle pour encoder et représenter tous les caractères, afin de faciliter la communication entre les systèmes informatiques, les applications et les personnes utilisant différentes langues.

Comment fonctionne l'Unicode ?

Dans l'Unicode, chaque caractère est représenté par un point de code unique appelé "code point". Le code point est un nombre entier qui identifie de manière unique un caractère dans l'ensemble de caractères Unicode.

Il existe actuellement plus de 143 000 code points dans l'ensemble de caractères Unicode, chacun représentant un caractère spécifique. Les premiers 128 code points sont réservés aux caractères ASCII, tandis que les autres sont utilisés pour représenter des caractères de langues et de scripts du monde entier.

### Conclusion

En conclusion, l'Unicode est un standard informatique important qui permet de représenter de manière unique tous les caractères utilisés dans les différentes écritures du monde entier. Il est utilisé dans de nombreuses technologies de l'information, y compris les systèmes d'exploitation, les applications, les sites web, les réseaux sociaux et les plates-formes de messagerie.  

Pour un élève de première NSI, comprendre l'Unicode est important pour travailler avec des textes multilingues, éviter les problèmes d'encodage et de manipulation de chaînes de caractères, et développer des applications et des sites web efficaces et fonctionnels.  

Entraînement 2 :  
Quel est le code binaire du "b" minuscule Unicode codé avec UTF-8 ?  

Python permet d’afficher un caractère à partir de son code avec la commande chr(valeur du caractère). Il permet aussi de donner le code d’un caractère avec la commande ord("a") par exemple. Le programme suivant permet donc d’afficher la table ASCII

Copier le code  

for i in range(128) :  
	   print(i,chr(i))