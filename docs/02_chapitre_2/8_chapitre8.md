---
author: Votre nom
title: Clash of Code NSI
---

!!! info "Principe et But :"
    QUEL EST LE PRINCIPE DU JEU ?
    Clash of Code est de la programmation sous forme de jeu pour améliorer ses compétences en programmation en résolvant des puzzles courts en même temps que d'autres joueurs.  
    Tous les joueurs peuvent partager leur code à la fin d'un puzzle afin d'aider les autres à s'améliorer.


    QUEL EST LE BUT DU JEU ?  
     ⭐ Améliorer ses compétences en résolution de problème  
     ⭐ Apprendre du code des autres joueurs  
     ⭐ S'entraîner aux tests pratiques de Bac NSI


    On peut chercher des trucs dans le cours. On prend son temps. On s'amuse. Chaque joueur est gagnant, même si 0% des tests passent !!
    (Les puzzles sont choisis parmi la banque d'exercices pratiques du Bac NSI)

## Lundi 16/10/2023 Premiers Clashs


<details>
  <summary>Cliquez pour afficher le premier problème</summary>
  <p>Programmer la fonction verifie qui prend en paramètre un tableau de valeurs
    numériques non vide et qui renvoie True si ce tableau est trié dans l’ordre croissant,
    False sinon.</p>

    Exemples :<br>  
    >>> verifie([0, 5, 8, 8, 9])<br>   
    True <br>  
    >>> verifie([8, 12, 4]) <br>  
    False  <br> 
    >>> verifie([-1, 4]) <br>  
    True <br>  
    >>> verifie([5])  <br> 
    True  <br> 
</details>

5 premiers : ⭐ Yann, Mikel, Omar, Markel


Solution posssible :

???+ question "Solution 1 : Yann"

    {{ IDE('sol1') }}


<details>
  <summary>Cliquez pour afficher le deuxième problème</summary>
  <p>Écrire en python deux fonctions : <br>  
lancer de paramètre n, un entier positif, qui renvoie un tableau de type list de
    n entiers obtenus aléatoirement entre 1 et 6 (1 et 6 inclus) ;<br>   
paire_6 de paramètre tab, un tableau de type list de n entiers entre 1 et
6 obtenus aléatoirement, qui renvoie un booléen égal à True si le nombre de 6
est supérieur ou égal à 2, False sinon. <br>  
On pourra utiliser la fonction randint(a,b) du module random pour laquelle la
documentation officielle est la suivante : <br>  
Renvoie un entier aléatoire N tel que a <=N <= b.</p>

    Exemples : <br>  
    >>> lancer1 = lancer(5) <br>  
    [5, 6, 6, 2, 2]  <br> 
    >>> paire_6(lancer1)<br>   
    True  <br> 
    >>> lancer2 = lancer(5)  <br> 
    [6, 5, 1, 6, 6]  <br> 
    >>> paire_6(lancer2)  <br> 
    True  <br> 
    >>> lancer3 = lancer(3)  <br> 
    [2, 2, 6]  <br> 
    >>> paire_6(lancer3) <br>  
    False  <br> 
    >>> lancer4 = lancer(0) <br>  
    [ ]  <br> 
    >>> paire_6(lancer4)  <br> 
    False  <br> 

</details>

5 premiers : ⭐ Celian, Joseba

Solution posssible :

???+ question "Solution 2 : "

    {{ IDE('sol2') }}

<details>
 <summary>Cliquez pour afficher le troisième problème</summary>
<p>Écrire une fonction moyenne renvoyant la moyenne pondérée d’une liste non vide,
passée en paramètre, de tuples à deux éléments de la forme (valeur,
coefficient) où valeur et coefficient sont des nombres positifs ou nuls.<br>   
Si la somme des coefficients est nulle, la fonction renvoie None, si la somme des
coefficients est non nulle, la fonction renvoie, sous forme de flottant, la moyenne des
valeurs affectées de leur coefficient.</p>

Exemples : <br>  
>>> moyenne([(8, 2), (12, 0), (13.5, 1), (5, 0.5)]) <br>  
9.142857142857142  <br> 
>>> moyenne([(3, 0), (5, 0)])  <br> 
None  <br> 

</details>

5 premiers : ⭐

<details>
<summary>Cliquez pour afficher le quatrième problème</summary>
<p>Programmer la fonction recherche, prenant en paramètre un tableau non vide tab (de
type list) d'entiers et un entier n, et qui renvoie l'indice de la dernière occurrence de
l'élément cherché. Si l'élément n'est pas présent, la fonction renvoie la longueur du
tableau.</p>

Exemples :<br>
 >>> recherche([5, 3], 1)<br>
 2<br>
 >>> recherche([2, 4], 2)<br>
 0<br>
 >>> recherche([2, 3, 5, 2, 4], 2)<br>
 3
</details>

5 premiers : ⭐