# Filmographie 

## Wargames

Wargames ou Jeux de guerre au Québec est un film américain de John Badham, sorti en 1983.

Avec pour acteurs principaux Matthew Broderick, Dabney Coleman, John Wood et Ally Sheedy, le film suit les aventures de David Lightman (Matthew Broderick), un jeune pirate informatique qui accède involontairement à WOPR, un supercalculateur militaire des forces armées américaines, programmé pour prédire les résultats possibles d'une guerre nucléaire. Lightman obtient de WOPR de lancer une simulation de guerre nucléaire, croyant initialement qu'il ne s'agit que d'un jeu informatique. La simulation cause une panique au niveau national et est près de déclencher la Troisième Guerre mondiale.

[![texte alternatif de l'image](http://img.youtube.com/vi/F7qOV8xonfY/0.jpg){: .center}](https://www.youtube.com/embed/F7qOV8xonfY?feature=oembed "Titre de la video") 

## Tron

Avec notamment les acteurs Jeff Bridges, Bruce Boxleitner, David Warner et Cindy Morgan dans les rôles principaux, le film a pour sujet le monde informatique et des jeux vidéo du début des années 1980, et présente à la fois une plongée dans un monde virtuel et le concept de l'intelligence artificielle, alors qu'à l'époque, la souris à boule faisait à peine ses débuts. Le film présente également la particularité d'avoir été le premier long métrage dont la conception fût assistée par ordinateur pour la plus grande partie de ses scènes.

[![texte alternatif de l'image](http://img.youtube.com/vi/3efV2wqEjEY/0.jpg){: .center}](https://www.youtube.com/embed/3efV2wqEjEY?feature=oembed "Titre de la video") 

## Electric Dreams

Une romance avec un vernis geek.  
Même si Electric Dreams n’est pas véritablement un film traitant des jeux vidéo, pas directement, il n’en demeure pas moins intéressant pour plusieurs raisons.  
Réalisé par Steve Barron, grand gourou des débuts du vidéo clip, réalisateur des Tortues Ninjas (1) (fragment de la culture pop dans laquelle s’intègre les jeux vidéo), le film est très représentatif d’une manière de voir, et de penser, les jeux vidéo au cinéma. Entre attraction et répulsion, Electric Dreams balance.  

[![texte alternatif de l'image](http://img.youtube.com/vi/fc8yjOrGUhQ/0.jpg){: .center}](https://www.youtube.com/embed/fc8yjOrGUhQ?feature=oembed "Titre de la video") 

## Les cinglés de l’informatique 1

Ce documentaire retrace l’histoire de l’informatique moderne et des micro-ordinateurs racontée « à l’américaine » de la fin de la seconde guerre mondiale jusqu’en 1995.

[![texte alternatif de l'image](http://img.youtube.com/vi/dOakyAhqiVY/0.jpg){: .center}](https://www.youtube.com/embed/dOakyAhqiVY?feature=oembed "Titre de la video") 

## Les cinglés de l’informatique 2

Ce documentaire retrace l’histoire de l’informatique moderne et des micro-ordinateurs racontée « à l’américaine » de la fin de la seconde guerre mondiale jusqu’en 1995.

[![texte alternatif de l'image](http://img.youtube.com/vi/zywPwbQqshY/0.jpg){: .center}](https://youtu.be/zywPwbQqshY "Titre de la video") 
