---
author: Jean-Marc Naulin
title: QCM
tags:
  - Difficulté **
---

!!! info "Générateur de QCM"

    Le but de ce projet est de créer un générateur de QCM (questionnaire à choix multiples). Vous devez composer vous-même 20 questions et réponses portant sur l'informatique. Le programme doit choisir 10 questions au hasard et les afficher à l'utilisateur. L'utilisateur doit répondre à ces questions et le programme doit afficher le score final.

## Travail à faire : 


!!! warning "Cahier des charges"

    Vous stockerez les questions et réponses dans un (ou deux) tableaux. À vous de choisir comment représenter ces données précisément.

    Quand le programme commence, il doit choisir 10 questions au hasard dans le tableau. Il doit ensuite afficher ces questions à l'utilisateur et lui demander de saisir la réponse. Le programme doit ensuite afficher le score final.

    Attention, une même question ne doit pas être posée deux fois.





!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    1) Un octet correspond à :  
    A. 1 bit  
    B. 4 bits  
    C. 8 bits  
    D. 32 bits  
    Choix : C  
    Correct !  
    2) ...  
    ...  
    10) Une autre question :  
    A. La réponse A  
    B. La réponse B  
    C. La réponse C  
    D. La réponse D  
    Choix : C  
    Incorrect !  
    Résultat : 8/10  

!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Proposer à l'utilisateur de choisir le nombre de questions à afficher.
  * Proposer à l'utilisateur d'ajouter des questions et réponses.
  * Ajouter un système de points. Par exemple, si la question est facile, le joueur gagne 1 point. Si la question est difficile, le joueur gagne 2 points. Si la question est très difficile, le joueur gagne 3 points. Le score final est alors le nombre de points gagnés.
  * Charger les questions et réponses depuis un fichier texte.







