---
author: Jean-Marc Naulin
title: Morpion
tags:
  - Difficulté **
---

!!! info "Morpion"
    Le Morpion, aussi appelé “tic-tac-toe” ou “oxo” en Belgique, est un jeu très courant et facile à jouer. Le principe du jeu est simple. C’est un jeu au tour par tour, où le but est d’aligner un trio de cercles ou de croix en diagonale, horizontalement ou verticalement sur une grille de 3×3 carrés pour obtenir la victoire.  



## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  


## Travail à faire : 


!!! warning "Cahier des charges"
    Le défi de la création de ce jeu consiste principalement à se familiariser avec l’indexation des tableaux en 2D et à comprendre comment vérifier les alignements en diagonale. Une fois ces problèmes résolus, le codage devrait être simplifié.

    Pour aller plus loin, vous pouvez aussi vous amusez à créer une interface graphique avec PyGame ou une autre bibliothèque graphique Python.
      





!!! example "Exemple d'exécution"

![morse](../images/morpion.jpg){ width=50%; : .center }


!!! info "Pour aller plus loin :"

 







