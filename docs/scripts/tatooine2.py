def panne_moteur(classement):
    # A compléter
    pass
    
    
def passe_en_tete(classement):
    # A compléter
    pass

def sauve_honneur(classement):
    # A compléter
    pass
    
def tir_blaster(classement):
    # A compléter
    pass

def retour_inattendu(classement, concurrent):
    # A compléter
    pass

# Tests
assert panne_moteur([8, 4, 6]) == [4,6,8]
assert passe_en_tete([8, 4, 6]) == [4,8,6]
assert sauve_honneur([8, 4, 6]) == [8,6,4]
assert tir_blaster([8, 4, 6]) == [6,4]
assert retour_inattendu([4,6],8) == [4,6,8]