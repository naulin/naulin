---
author: Jean-Marc Naulin
title: Palindrome Numérique
tags:
  - Difficulté *
---

!!! info "Palindromes numériques"

    Un palindrome numérique est un nombre qui se lit de la même façon dans les deux sens. Par exemple, 12321 est un palindrome numérique.

    Le plus grand palindrome numérique obtenu à partir du produit de deux nombres à deux chiffres est 9009 = 91 × 99.

    Vous devez écrire un programme qui trouve le plus grand palindrome numérique obtenu à partir du produit de deux nombres à trois chiffres.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme affiche le plus grand palindrome numérique obtenu à partir du produit de deux nombres à trois chiffres.

    Votre programme doit comporter les deux fonctions suivantes :

    est_un_palindrome(n) - prend un nombre n en paramètre et renvoie True si le nombre est un palindrome numérique et False sinon ;  
    liste_palindromes(nb_chiffres) - renvoie la liste des palindromes numériques à nb_chiffres chiffres.


!!! example "Exemple d'exécution"




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :
  
  * Optimiser la vitesse de calcul.
  * Généraliser à plus de trois chiffres.







