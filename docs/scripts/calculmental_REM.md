---
author: Jean-Marc Naulin
title: Calcul Mental
tags:
  - Difficulté *
---

!!! info "Calcul mental"

    Le but de ce projet est de créer un logiciel éducatif qui permet aux élèves de s'entraîner au calcul mental.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme propose à l'utilisateur de choisir le type d'opération (addition, soustraction, multiplication). Le programme génère ensuite une séquence de 10 opérations aléatoires dont le résultat est demandé à l'utilisateur. Le programme indique ensuite le nombre de bonnes réponses.

    Pour les additions et soustractions, le nombre maximum de chiffres des deux opérandes est de 2. Pour les multiplications, il est de 1. De plus, dans les soustractions, le résultat doit être positif.



!!! example "Exemple d'exécution"

    Quelle opération voulez-vous réviser ?  
    1. Addition  
    2. Soustraction  
    3. Multiplication  
    Choix : 2  
    Combien font 5 - 3 ? 2  
    Combien font 72 - 17 ? 55  
    Combien font 35 - 15 ? 20  
    Combien font 90 - 75 ? 25  
    Erreur : la réponse était 15  
    Combien font 21 - 5 ? 16  
    Combien font 80 - 79 ? 1  
    Combien font 99 - 44 ? 55  
    Combien font 8 - 2 ? 5  
    Erreur : la réponse était 6  
    Combien font 2 - 2 ? 0  
    Combien font 48 - 15 ? 33  

    Votre note est de 8/10  



!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :
  
  * DPermettre de choisir le nombre de questions.
  * Chronométrer le temps de réponse de l'utilisateur et imposer un temps maximum pour répondre.







