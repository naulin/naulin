from sympy import symbols
from sympy.logic import simplify_logic

A, B, C, D, E, F, G, H = symbols('A B C D E F G H')
 
q = ((B&A&C) | (B&A&D))

print(simplify_logic(q))
