---
author: Jean-Marc Naulin
title: Date
tags:
  - Difficulté **
---

!!! info "Calendrier"

    Le but de ce projet est de déterminer le jour de la semaine correspondant à une date.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme demande à l'utilisateur de saisir une date au format jj/mm/aaaa (par exemple 30/07/2012 pour le 30 juillet 2012). Si la date est incorrecte, il demande à l'utilisateur de saisir une nouvelle date. Il affiche ensuite le jour de la semaine correspondant à cette date.

    Vous ne pouvez pas utiliser de bibliothèque extérieure telle que datetime.


!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Saisir une date (jj/mm/aaaa) : 30/02/2021  
    Cette date est incorrecte.  
    Saisir une date (jj/mm/aaaa) : 12/04/2021  
    C'est un lundi  



!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Permettre à l'utilisateur de saisir un mois et une année. Le programme affiche alors un calendrier du mois et de l'année. Par exemple :

    Juin 1970  
    Lun Mar Mer Jeu Ven Sam Dim  
    1   2   3   4   5   6   7    
    8   9   10  11  12  13  14   
    15  16  17  18  19  20  21   
    22  23  24  25  26  27  28   
    29  30      


  * Afficher le calendrier de toute une année.








