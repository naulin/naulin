def recherche_sequentielle(tab, val):
    """
    Entrées : 
        tab est un tableau
        val est une variable de même type que les éléments du tableau
    Sortie : un booléen qui vaut True si val appartient au tableau et False sinon
    """
    for i in range(len(tab)):
        if tab[i] == val:
            return True
    return False

# Test de la recherche séquentielle 
from random import randint

taille = 50
tableau = [randint(1, taille) for k in range(taille)] # un tableau rempli de nombres choisis aléatoirement
print("Tableau :", tableau)
valeur = randint(1, taille) # un nombre choisi aléatoirement
print("Valeur :", valeur)

print(recherche_sequentielle(tableau, valeur))