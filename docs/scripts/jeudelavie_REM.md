---
author: Jean-Marc Naulin
title: Jeu de la vie
tags:
  - Difficulté ***
---

!!! info "Jeu de la vie"
    Je vous propose un mini projet individuel visant à reproduire le jeu de la vie proposé par Conway en 1970. Le jeu de la vie est ce qu'on appelle un automate cellulaire : c'est un « tableau » dont l'état de chaque case ou cellule dépend de l'état des cases voisines. À chaque « tour » de jeu, on met à jour l'ensemble des cases en fonction de leurs voisines et on recommence. 


[![texte alternatif de l'image](http://img.youtube.com/vi/S-W0NX97DB0/0.jpg){: .center}](https://youtu.be/S-W0NX97DB0 "Titre de la video")

## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  


## Travail à faire : 


!!! warning "Cahier des charges"
    
     

    Dans le jeu de la vie, chaque cellule peut avoir deux états :  

    vivante ;  
    morte.  
    L'état d'une cellule au tour suivant dépend de l'état de ses huit voisins directs. Il y a deux règles simples qui s'appliquent :  

    une cellule morte possédant exactement trois voisines vivantes devient vivante : elle naît ;  
    une cellule vivante possédant deux ou trois voisines vivantes le reste, sinon elle meurt.  
    Si vous voulez des précisions ou en savoir plus allez sur la page Wikipédia du jeu de la vie.  





!!! example "Exemple d'exécution"

![morse](../images/Gol-gun.gif){ width=50%; : .center }

Premier "canon" découvert.


![morse](../images/Eden.png){ width=50%; : .center }

Premier "jardin d'Eden" trouvé.



 







