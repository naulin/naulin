---
author: Jean-Marc Naulin
title: Repertoire
tags:
  - Difficulté **
---

!!! info "Répertoire téléphonique"

    Le but de ce projet est de créer un répertoire téléphonique permettant de mémoriser des numéros de téléphone.

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme affiche d'abord un menu avec les options suivantes :

      * Ajouter un numéro - permet d'ajouter un numéro de téléphone dans le répertoire
      * Rechercher un numéro - permet de rechercher un numéro de téléphone dans le répertoire
      * Quitter - permet de quitter le programme

    Lorsque l'utilisateur choisit l'option Ajouter un numéro, le programme lui demande de saisir le nom de la personne et son numéro de téléphone. Le programme mémorise ensuite ces informations dans le répertoire.

    Lorsque l'utilisateur choisit l'option Rechercher un numéro, le programme lui demande de saisir le nom de la personne dont il veut connaître le numéro de téléphone. Le programme affiche ensuite le numéro de téléphone correspondant s'il existe, ou un message d'erreur sinon.

    Lorsque l'utilisateur choisit l'option Quitter, le programme se termine.





!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Choisissez une option :  
    1. Ajouter un numéro  
    2. Rechercher un numéro  
    3. Quitter  
    Choix : 1  
    Nom : Alice  
    Numéro : 0123456789  
    Alice a été ajouté(e) aux contacts !  
    Choisissez une option :  
    1. Ajouter un numéro  
    2. Rechercher un numéro  
    3. Quitter  
    Choix : 2  
    Nom : Alice  
    Son numéro est : 0123456789  
    Choisissez une option :  
    1. Ajouter un numéro  
    2. Rechercher un numéro  
    3. Quitter  
    Choix : 2  
    Nom : Bob  
    Bob ne fait pas partie des contacts !  
    Choisissez une option :  
    1. Ajouter un numéro  
    2. Rechercher un numéro  
    3. Quitter  
    Choix : 3  


!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Vérifier que le numéro de téléphone saisi par l'utilisateur est bien un numéro de téléphone français valide.
  * Permettre de mémoriser plusieurs numéros de téléphone pour une même personne associés à une étiquette (par exemple : Personnel, Professionnel).







