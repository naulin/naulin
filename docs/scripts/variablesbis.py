x=10
y="geeks"
z=20
# si on fait w=z?
# w va faire référence à la même adresse mémoire que z donc w=z=20
# si on fait w=30, alors w renvoie 30
w=z
print(w)
w=30
print(w)

