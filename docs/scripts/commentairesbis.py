"""
Nous rappelons la formule du produit scalaire de deux
vecteurs que nous utilisons ci-dessous :
Soit U(x;y) et V(x';y'), U.V = x * x' + y * y'
"""