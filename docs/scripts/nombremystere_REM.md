---
author: Jean-Marc Naulin
title: Nombre Mystere
tags:
  - Difficulté *
---

!!! info "Le jeu du nombre mystère"

    Le but de ce projet est de créer le jeu du nombre mystère, dans lequel un joueur A choisit un nombre aléatoire entre 1 et 100 et un joueur B doit le retrouver. À chaque essai du joueur B, le joueur A indique si le nombre mystère est plus grand ou plus petit que le nombre proposé.

    Vous devrez programmer deux modes de jeu : le mode où l'ordinateur choisit le nombre et l'utilisateur le cherche, et le mode où l'utilisateur choisit le nombre et l'ordinateur le cherche.

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme demande d'abord à l'utilisateur dans quel mode il veut jouer. Selon le mode choisi, le programme se comporte différemment.

    Si le mode choisi est celui où l'ordinateur choisit le nombre, le programme demande à l'utilisateur de choisir un nombre entre 1 et 100, puis lui indique si le nombre mystère est plus grand ou plus petit et recommence tant que l'utilisateur n'a pas trouvé le nombre. Quand l'utilisateur a trouvé, le programme indique en combien de tours il a trouvé le nombre.

    Si le mode choisi est celui où le joueur choisit le nombre, le programme demande à l'utilisateur d'appuyer sur Entrée quand il a choisi son nombre. Ensuite, l'ordinateur fait une proposition. L'utilisateur doit indiquer si le nombre mystère est plus grand, plus petit ou égal à la proposition. L'ordinateur fait d'autres propositions jusqu'à ce que le nombre mystère soit trouvé. Quand l'ordinateur a trouvé, le programme indique en combien de tours il a trouvé le nombre. Essayez de trouver une méthode efficace pour votre intelligence artificielle !





!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Cas où l'ordinateur choisit :

    Dans quel mode voulez-vous jouer ?  
    1) L'ordinateur choisit le nombre  
    2) L'utilisateur choisit le nombre  
    Choix : 1  
    Entrez un nombre : 43  
    C'est plus grand !  
    Entrez un nombre : 72  
    C'est plus petit !  
    Entrez un nombre : 67  
    Bravo ! Vous avez trouvé en 3 coups !  

    Cas où l'utilisateur choisit le nombre mystère : 

    Dans quel mode voulez-vous jouer ?  
    1) L'ordinateur choisit le nombre  
    2) L'utilisateur choisit le nombre  
    Choix : 2  
    Appuyez sur Entrée quand vous avez choisi un nombre...  
    Je tente ma chance... 50 !  
    Votre réponse :  
    1) C'est ça !  
    2) Plus petit  
    3) Plus grand  
    Choix : 2  
    Je tente ma chance... 25 !  
    Votre réponse :  
    1) C'est ça !  
    2) Plus petit  
    3) Plus grand  
    Choix : 3  
    Je tente ma chance... 37 !  
    Votre réponse :  
    1) C'est ça !  
    2) Plus petit  
    3) Plus grand  
    Choix : 1  
    L'ordinateur a trouvé en 3 coups !





!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Détecter quand l'utilisateur triche dans le deuxième mode (si deux de ses déclarations sont contradictoires).
  * Donner une limite de nombre de coups à l'utilisateur dans le premier mode (sinon il a perdu).
  * Proposer à l'utilisateur de définir le minimum et le maximum du nombre mystère avant de jouer (à n'importe lequel des deux modes).








