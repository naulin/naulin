# -*- coding: utf-8 -*-
"""
Created on Thu Dec 14 11:35:31 2023

@author: jeanmarc.naulin
"""

# libraries
import pygame, sys
from pygame.locals import *
pygame.init()

clock = pygame.time.Clock() 

#Setting up the window
screen_width = 600
screen_height = 400
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Scrolling Background Tutorial")

#Load background image
background_img = pygame.image.load("background1.png")

# Scroll the background
scroll_x = 0
scroll_y = 0
background_x = 0

#Add game objects
player_img = pygame.image.load("mario.png")
player_x = 10
player_y = 100

pygame.key.set_repeat(1,10)
while True:
   
   for event in pygame.event.get():
      if event.type == pygame.QUIT:
         pygame.quit()
      if event.type == KEYDOWN:
         if event.key == K_RIGHT :
             player_x += 5
             if player_x > 250 :
                  scroll_x -= 5
                  background_x -= 5
             if player_x > 550 :
                 player_x = 10
                 scroll_x -= 500
                 background_x -= 500
                 
         if event.key == K_LEFT :
             player_x -= 5
             #scroll_x += 5
             #background_x += 10
      
      
         

   #Scroll the background horizontally
   

   #Draw the background twice to create seamless scrolling effect
   #screen.fill([10,186,181])
   screen.blit(background_img, (scroll_x, scroll_y))
   screen.blit(background_img, (background_x, scroll_y))

   #Reset the background position when it goes off screen
   if scroll_x <= -2200:
      scroll_x = 0#screen_width

   if background_x <= -2200:
      background_x = 0#screen_width

   #Add game objects
   screen.blit(player_img, (player_x, player_y))

   pygame.display.update()