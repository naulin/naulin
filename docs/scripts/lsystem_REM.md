---
author: Jean-Marc Naulin
title: L-system
tags:
  - Difficulté **
---

!!! info "L-system"
    Un L-System (ou système de Lindenmayer) est une grammaire formelle, permettant un procédé algorithmique, inventé en 1968 par le biologiste hongrois Aristid Lindenmayer qui consiste à modéliser le processus de développement et de prolifération de plantes ou de bactéries.  

    Basée sur une forme récursive de grammaire générative, cette grammaire a été approfondie et mise en œuvre graphiquement par Przemyslaw Prusinkiewicz dans les années 1980.  

    Au départ, Lindenmayer avait pensé ce système comme un langage formel qui permettait de décrire le développement d'organismes multicellulaires simples. À cette époque il travaillait sur les levures, les champignons et des algues. Mais l'informatique a permis d'exploiter ce système pour générer graphiquement des calculs de plantes très complexes.  

    Un L-System est un ensemble de règles et de symboles qui modélisent un processus de croissance d'êtres vivants comme des plantes ou des cellules. Le concept central des L-Systems est la notion de réécriture. La réécriture est une technique pour construire des objets complexes en remplaçant des parties d'un objet initial simple en utilisant des règles de réécriture.  

    Pour ce faire, les cellules sont modélisées à l'aide de symboles. À chaque génération, les cellules se divisent, i.e. un symbole est remplacé par un ou plusieurs autres symboles formant un mot.  

[![texte alternatif de l'image](http://img.youtube.com/vi/ShjScrFA5iQ/0.jpg){: .center}](https://youtu.be/ShjScrFA5iQ "Titre de la video")


## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  



## Travail à faire : 


!!! warning "Cahier des charges"
    — compréhension du principe du L-system ;  
    — Utiliser un programme qui génère des mots à partir d’un ensemble de règles;  
    — produire des images qui représentent une plante en deux dimensions ; 

    l'algue de Lindenmayer  
    Voici le premier L-System d'Aristid Lindenmayer qui servait à décrire le développement d'une algue :  

    Alphabet : V = {A, B}    
    Axiome de départ : w = A  
    Régles : (A → AB) ∧ (B → A)  
    Notation :  

    Algue  
    {  
    Axiom A  
    A=AB  
    B=A  
    }  

    Algue est le nom du L-System. En premier on a l'axiome ω, puis chaque règle de P est à la ligne l'une de l'autre. A=AB est à comprendre comme tout symbole A devient un " mot " AB à la génération suivante.  

    Voici le résultat sur six générations :  

    n=0, A  
    n=1, AB  
    n=2, AB A  
    n=3, AB A AB  
    n=4, AB A AB AB A  
    n=5, AB A AB AB A AB A AB  

    

???+ question "Programme de départ :"

    {{ IDE('./Lsystem') }}

!!! example "Exemple d'exécution"

![morse](../images/Lsystem.jpg){ width=50%; : .center }

![morse](../images/Lsystem1.jpg){ width=50%; : .center }


 







