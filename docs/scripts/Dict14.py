qcm = [] # Le contenu du QCM à définir

def poser_question(question) :
    """Une fonction qui affiche la question,
       les choix possibles de réponse et qui demande à
       l'utilisateur son choix.

       La fonction retourne ``True`` si l'utilisateur a choisi
       la bonne réponse
    """
    pass

score = 0
# on parcourt la liste des questions et on calcule le score de l'utilisateur
# ....

# on affiche le score
print("Vous avez obtenu un score de ", score)
