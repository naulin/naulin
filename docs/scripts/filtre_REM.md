---
author: Jean-Marc Naulin
title: Filtre Image
tags:
  - Difficulté ***
---

!!! info "Filtre Image"
    Dans ce projet, vous aller devoir créer des filtres pour des images comme les filtres Instagram.  

    Nous utiliserons la bibliothèque Python PIL (Python Image Library) aussi appelée Pillow :  

    from PIL import Image  

    Cette bibliothèque permet d’accéder simplement aux pixels d’une image.  


## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  

## Explication :

Une image se décompose en une multitude de points appelés pixels. 

![morse](../images/pixel-example.png){ width=50%; : .center } 

Chaque pixel possède sa propre couleur. Cette couleur résulte de la combinaison des trois couleurs primaires : rouge, vert et bleu (Red, Green et Blue : RGB). Ces niveaux peuvent en général prendre toutes les valeurs entre 0 et 255. On représente ainsi la couleur d’un pixel par trois nombres correspondant aux niveaux de rouge, vert et bleu. Par exemple pour un pixel vert on aura (0, 255, 0). Vous pouvez aller sur ce site pour voir le code RGB de toutes les couleurs affichables sur un écran : https://htmlcolorcodes.com/fr/.

Pillow  
La documentation de Pillow est disponible à l’adresse suivante : https://pillow.readthedocs.io/en/stable/reference/Image.html  

Pour vous faciliter la prise en main de cette bibliothèque, voici les commandes les plus utiles :  

![morse](../images/Pillow.png){ width=50%; : .center } 

## Travail à faire : 


!!! warning "Cahier des charges"
    
     

   Vous allez devoir créer différents filtres ainsi qu’une interface en ligne de commande.  

###  Premier filtre : rouge

1) Créez un premier filtre qui ne garde que la composante rouge d’une image. C’est à dire qu’il faut pour chaque pixel mettre le vert et le bleu à zéro et ne pas toucher à la composante rouge. Ce filtre se présentera sous la forme d’une fonction prenant un objet Image en paramètre et retournant un nouvel objet image.


```python
from PIL import Image

image = Image.open("maison.jpg")

image.show()

def filtre_rouge(image):
    # On fait une copie de l'image pour ne pas modifier l'original
    nouvelle = image.copy()
    for y in range(image.height):
        for x in range(image.width):
            r, g, b = image.getpixel((x,y))
            nouvelle.putpixel((x,y), (r,0,0))
    return nouvelle



image_filtree = filtre_rouge(image)
image_filtree.show()
```

### Filtres
Créez les filtres suivants :  

2) Un filtre vert et un filtre bleu basés sur le même principe que le filtre rouge ;  

3) Un filtre gris qui transforme une image couleur en nuance de gris. Pour cela, chaque composante d’un pixel prend la moyenne des trois composantes.  

4) Un filtre noiretblanc qui après le filtre gris met les composantes à 0 si leur valeur est inférieure à 128 et à 255 si leur valeur est supérieure ou égale à 128.  

5) Un filtre miroir qui retourne l’image comme si on la voyait dans un miroir.  


### Interface

6) Créez une interface simple en ligne de commande qui propose de choisir le fichier à traiter, le nom du ficher produit (pas de sauvegarde si rien n’est saisi) puis le filtre avec un numéro. L’interface devra ressembler à ce qu’il y a ci-dessous :  

Quel fichier utiliser ("photo.jpg" par défaut) ?  
Quel nom donner au nouveau fichier (pas de sauvegarde par défaut) ? photo2.jpg  
Voici les filtres disponibles :  
1 – rouge  
2 – vert  
3 – bleu  
4 – gris  
5 – noir_et_blanc  
6 – miroir  
Quel filtre choisissez-vous ? 2  
Filtre vert appliqué  

![morse](../images/im1.png){ width=50%; : .center } 

### Encore des filtres
Créez les filtres suivants :  

7) Un filtre pixel3 qui parcourt l’image par bloc de 3 pixels de large et remplace les couleurs des 9 pixels par une même couleur qui est la moyenne des 9 couleurs. Cela devra faire l’effet d’une image pixelisée.  

8) Un filtre pixel10 qui fait la même chose que le filtre pixel3 mais avec des blocs de 10 pixels de large.  

9) Un filtre lum qui prend en paramètre un nombre entier et qui ajoute cette valeur à toutes les couleurs de chaque pixel. (il n’y a pas besoin de faire attention aux limites 0 et 255, Pillow s’en charge automatiquement)  

10) Un filtre color qui prend en paramètre une des trois couleurs primaire et un nombre entier k. Il ajoute alors ce nombre à la composante correspondante de chaque pixel et retranche k/2 aux deux autres composantes.  

11) Un filtre color512 qui limite à 8 le nombre de valeurs pour chaque composante de façon à donner un effet « vieux jeu vidéo » à l'image. Par exemple si la composante est entre 0 et 31 alors on la remplace par 0, entre 32 et 63 on la remplace par 32… Il est possible de faire cela très simplement. Le rendu pourra être meilleur avec une autre image que vous trouverez sur internet.  

 







