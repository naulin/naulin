# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

# define and move to dataset directory
datasetdir = 'C:/maldives-master/maldives-master/dogs_vs_cats/train/train'
import os
os.chdir(datasetdir)

# import the needed packages
import matplotlib.pyplot as plt
import matplotlib.image as img
from tensorflow import keras
# shortcut to the ImageDataGenerator class
ImageDataGenerator = keras.preprocessing.image.ImageDataGenerator

plt.subplot(1,2,1)
plt.imshow(img.imread('cats/cat.0.jpg'))
plt.show()
plt.subplot(1,2,2)
plt.imshow(img.imread('dogs/dog.0.jpg'))
plt.show()

images = []
for i in range(10):
  im = img.imread('cats/cat.{}.jpg'.format(i))
  images.append(im)
  print('image shape', im.shape, 'maximum color level', im.max())

"""  
bad_dog_ids = [5604, 6413, 8736, 8898, 9188, 9517, 10161, 
               10190, 10237, 10401, 10797, 11186]

bad_cat_ids = [2939, 3216, 4688, 4833, 5418, 6215, 7377, 
               8456, 8470, 11565, 12272]

def load_images(ids, categ):
  '''return the images corresponding to a list of ids'''
  images = []
  dirname = categ+'s' # dog -> dogs
  for theid in ids: 
    fname = '{dirname}/{categ}.{theid}.jpg'.format(
        dirname=dirname,
        categ=categ, 
        theid=theid
    )
    im = img.imread(fname)
    images.append(im)
  return images

bad_dogs = load_images(bad_dog_ids, 'dog')
bad_cats = load_images(bad_cat_ids, 'cat')

def plot_images(images, ids):
    ncols, nrows = 4, 3
    fig = plt.figure( figsize=(ncols*3, nrows*3), dpi=90)
    for i, (img, theid) in enumerate(zip(images,ids)):
      plt.subplot(nrows, ncols, i+1)
      plt.imshow(img)
      plt.show()
      plt.title(str(theid))
      plt.axis('off')


plot_images(bad_dogs, bad_dog_ids)
plot_images(bad_cats, bad_cat_ids)


import glob
import re
import shutil

# matches any string with the substring ".<digits>."
# such as dog.666.jpg
pattern = re.compile(r'.*\.(\d+)\..*')

def trash_path(dirname):
    '''return the path of the Trash directory, 
    where the bad dog and bad cat images will be moved.
    Note that the Trash directory should not be within the dogs/ 
    or the cats/ directory, or Keras will still find these pictures.
    '''
    return os.path.join('../Trash', dirname)


def cleanup(ids, dirname): 
  '''move away images with these ids in dirname
  '''
  os.chdir(datasetdir)
  # keep track of current directory
  oldpwd = os.getcwd()
  # go to either cats/ or dogs/
  os.chdir(dirname)
  # create the trash directory. 
  # if it exists, it is first removed
  trash = trash_path(dirname)
  if os.path.isdir(trash):
    shutil.rmtree(trash)
  os.makedirs(trash, exist_ok=True)
  # loop on all cat or dog files
  fnames = os.listdir()
  for fname in fnames:
    m = pattern.match(fname)
    if m: 
      # extract the id
      the_id = int(m.group(1))
      if the_id in ids:
        # this id is in the list of ids to be trashed
        print('moving to {}: {}'.format(trash, fname))
        shutil.move(fname, trash)
  # going back to root directory
  os.chdir(oldpwd)
  
def restore(dirname):
  '''restores files in the trash
  I will need this to restore this tutorial to initial state for you
  and you might need it if you want to try training the network
  without the cleaning of bad images
  '''
  os.chdir(datasetdir)
  oldpwd = os.getcwd()
  os.chdir(dirname)
  trash = trash_path(dirname)
  print(trash)
  for fname in os.listdir(trash):
    fname = os.path.join(trash,fname)
    print('restoring', fname)
    print(os.getcwd())
    shutil.move(fname, os.getcwd())
  os.chdir(oldpwd)
 
    
cleanup(bad_cat_ids,'cats')
cleanup(bad_dog_ids, 'dogs')

"""

import os
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# Création de l'ImageDataGenerator
gen = ImageDataGenerator()

# Création de l'itérateur avec flow_from_directory
iterator = gen.flow_from_directory(
    os.getcwd(),  # répertoire courant
    target_size=(256, 256),
    classes=('dogs', 'cats')
)

# Utilisation de next(iterator) pour récupérer un batch
batch = next(iterator)

# Affichage des informations sur le batch
print(len(batch))  # Affiche la longueur du batch (nombre d'images par batch)
print(type(batch[0]))  # Type des images dans le batch (array numpy)
print(type(batch[1]))  # Type des labels dans le batch (array numpy)

print(batch[0].shape)  # Affiche la forme de la première image
print(batch[0].dtype)  # Affiche le type de données de la première image
print(batch[0].max())  # Affiche la valeur maximale de la première image
print(batch[1].shape)  # Affiche la forme des labels (par exemple, [batch_size, nb_classes])
print(batch[1].dtype)  # Affiche le type des labels

# Conversion et affichage de la première image dans le batch
plt.imshow(batch[0][0].astype(int))  # Utilisation de int au lieu de np.int
plt.show()

def plot_images(batch):
    imgs = batch[0]
    labels = batch[1]
    ncols, nrows = 4,8
    fig = plt.figure( figsize=(ncols*3, nrows*3), dpi=90)
    for i, (img,label) in enumerate(zip(imgs,labels)):
      plt.subplot(nrows, ncols, i+1)
      plt.imshow(img.astype(int))
      assert(label[0]+label[1]==1.)
      categ = 'dog' if label[0]>0.5 else 'cat'
      plt.title( '{} {}'.format(str(label), categ))
      plt.axis('off')

plot_images( next(iterator))

imgdatagen = ImageDataGenerator(
    rescale = 1/255., 
    validation_split = 0.2,
)

batch_size = 30
height, width = (256,256)

train_dataset = imgdatagen.flow_from_directory(
    os.getcwd(),
    target_size = (height, width), 
    classes = ('dogs','cats'),
    batch_size = batch_size,
    subset = 'training'
)

val_dataset = imgdatagen.flow_from_directory(
    os.getcwd(),
    target_size = (height, width), 
    classes = ('dogs','cats'),
    batch_size = batch_size,
    subset = 'validation'
)


model = keras.models.Sequential()

initializers = {
    
}
model.add( 
    keras.layers.Conv2D(
        24, 5, input_shape=(256,256,3), 
        activation='relu', 
    )
)
model.add( keras.layers.MaxPooling2D(2) )
model.add( 
    keras.layers.Conv2D(
        48, 5, activation='relu', 
    )
)
model.add( keras.layers.MaxPooling2D(2) )
model.add( 
    keras.layers.Conv2D(
        96, 5, activation='relu', 
    )
)
model.add( keras.layers.Flatten() )
model.add( keras.layers.Dropout(0.9) )

model.add( keras.layers.Dense(
    2, activation='softmax',
    )
)

model.summary()

model.compile(loss='binary_crossentropy',
              optimizer = keras.optimizers.Adamax(learning_rate=0.001),  # Utilisation de 'learning_rate'
              #optimizer=keras.optimizers.Adamax(lr=0.001),
              metrics=['acc'])

history = model.fit(
    train_dataset, 
    validation_data = val_dataset,
    #workers=10,
    epochs=20,
)

