---
author: Jean-Marc Naulin
title: Bataille Navale
tags:
  - Difficulté ***
---

!!! info "Bataille navale"
    Le jeu « Bataille navale » (aussi appelé abusivement « Touché coulé ») est un jeu qui voit s'affronter deux joueurs à la tête d'une flotte de navires de guerre. Ces navires ont différentes tailles et sont placés en début de partie par les joueurs sur une grille (cachée de l'autre joueur, de sorte que seul le propriétaire des bateaux connaisse leur position).   

    À tour de rôle, les joueurs vont tirer sur une case de la grille de l'adversaire en donnant ses coordonnées (par exemple « C5 »). Si aucun bateau ne s'y trouve, l'adversaire annonce « plouf ». Si un bateau s'y trouve, l'adversaire marque le bateau comme endommagé sur cette case puis répond :

    « coulé » si toutes les autres cases du bateau sont déjà endommagées ;  
    « touché » sinon.  
    Le jeu se termine quand un des joueurs coule le dernier bateau de son adversaire : il a donc gagné.  

   

![morse](../images/plateau.png){ width=50%; : .center }

## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  


## Travail à faire : 


!!! warning "Cahier des charges"
    Votre programme doit permettre à l'utilisateur de jouer à sens unique contre l'ordinateur : votre programme place secrètement et aléatoirement des bateaux sur la grille et l'utilisateur doit couler tous les bateaux le plus rapidement possible. La taille de la grille doit être stockée dans des constantes (LARGEUR et HAUTEUR) et doit par défaut être de 5x5 (une vraie grille fait 10x10 mais ça rendrait le programme long à tester). Attention : si on modifie les valeurs de LARGEUR et HAUTEUR, votre programme doit s'adapter automatiquement !  

    Pour représenter l'état du jeu, je vous conseille de faire un tableau bateaux contenant la liste des tailles de bateaux et un tableau grille de lignes (chaque ligne étant elle-même un tableau de cases représentées par des entiers). Chaque case de la grille vaut l'indice du bateau présent dans le tableau bateaux (-1 s'il n'y a pas de bateau). Enfin, vous pouvez créer une autre grille (tableau de tableaux de booléens) tirs où chaque case vaut True si le joueur a tiré sur cette case et False sinon.  

    Par défaut, au lancement du jeu, votre programme doit placer un bateau de 4 cases, deux bateaux de 3 cases et deux bateaux de 2 cases. Les bateaux ne doivent bien sûr pas se croiser : une case ne peut contenir qu'un seul bateau. De plus, les bateaux doivent bien tenir dans la grille. Il faut donc choisir aléatoirement les coordonnées de chaque bateau ainsi que leur orientation (horizontale ou verticale) tout en s'assurant qu'ils peuvent bien être placés à l'endroit choisi.  

    Une fois le placement des bateaux effectué, votre programme affiche une grille des tirs effectués (pour l'instant vide) et demande à l'utilisateur d'entrer des coordonnées (à vous de décider sous quelle forme). Votre programme répond alors « Plouf », « Touché » ou « Coulé », affiche la grille des tirs mise à jour et redemande des coordonnées à l'utilisateur. Il continue jusqu'à ce que tous les bateaux aient été coulés. Il affiche alors le nombre de tirs qui ont été nécessaires.  
      


!!! info "Pour aller plus loin :"
    Une fois que vous avez atteint le palier 3 des fonctionnalités, voici quelques idées pour aller plus loin et atteindre le palier 4 :  

    Proposer à l'utilisateur de choisir différentes tailles de grilles et/ou différentes flottes de bateaux.  
    Proposer des pouvoirs spéciaux permettant par exemple, une fois par partie, de tirer sur toute une ligne à la fois, toute une colonne ou un ensemble de cases adjacentes, d'obtenir des informations (mais pas tout) sur les bateaux ennemis, etc.  

 







