civilite = {'M': 'Monsieur', 'Mme': 'Madame', 'Mlle': 'Mademoiselle'}

# On parcourt les clés du dictionnaire
for k in civilite.keys():
    print(k)

# Par défaut, on parcourt les clés du dictionnaire
for k in civilite:
    print(k)

# On parcourt les valeurs du dictionnaire
for v in civilite.values():
    print(v)

# On parcourt les clés et les valeurs du dictionnaire
for k, v in civilite.items():
    print(k, v)

