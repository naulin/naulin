---
author: Jean-Marc Naulin
title: Pi
tags:
  - Difficulté *
---

!!! info "π, un nombre univers"

    Le but de ce projet est d'écrire un programme Python qui vérifie si les années comprises entre 1900 et 2017 sont présentes dans les 20.000 premiières décimales de π.

## Travail à faire : 



!!! warning "Cahier des charges"

    VVotre programme demande à l'utilisateur de saisir une année comprise entre 1900 et 2017. Il affiche ensuite le rang auquel cette année apparait dans les 20.000 premières décimales de π (si elle apparait) et un message indiquant qu'elle n'est pas présente sinon.

    Pour calculer les décimales de π, vous pouvez utiliser le bout de programme suivant :

    Ce programme calcule les 10 000 premières décimales de Pi et les stocke dans
    une liste (algorithme de Spigot).  
    Auteur : P. SALLIOT (académie de Lyon)  
    Inspiré par https://stackoverflow.com/questions/9004789/1000-digits-of-pi-in-python

    def fabriquer_pi():  
      q, r, t, k, m, x = 1, 0, 1, 1, 3, 3  
      for j in range(43214):  
          if 4 * q + r - t < m * t:  
              yield m  
              q, r, t, k, m, x = 10*q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10*m, x  
          else:  
              q, r, t, k, m, x = q*k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x+2  

    liste_entiers_pi = []  

    for i in fabriquer_pi():  
        liste_entiers_pi.append(i)  

    print(liste_entiers_pi)  



!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Indiquer une année : 1970
    L'année 1970 est présente entre la 8252ème et la 8257ème décimale de π.




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Demander des années en boucle jusqu'à ce que l'utilisateur entre le nombre 0.
  * Mémoriser les résultats pour éviter de refaire les calculs à chaque fois.
  * Trouver un algorithme de calcul de π plus rapide que celui proposé.







