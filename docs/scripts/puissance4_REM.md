---
author: Jean-Marc Naulin
title: Puissance 4
tags:
  - Difficulté ***
---

!!! info "Puissance 4"
    Nous allons programmer un jeu de puissance 4 en ligne de commande. Les plus rapides et motivés pourront éventuellement s'amuser à créer une interface graphique.  

    Le jeu de puissance 4 se joue à deux joueurs avec une grille de 7 colonnes et 6 rangées. Un joueur possède des jetons rouges et l'autre des jetons jaunes. Les joueurs mettent chacun leur tour un jeton dans une colonne. Le but est d'être le premier à aligner 4 jetons verticalement, horizontalement ou en diagonale. Si personne n'arrive à aligner 4 jetons avant que la grille ne soit remplie il y a match nul.

![morse](../images/p4.jpg){ width=50%; : .center }

## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  


## Travail à faire : 


!!! warning "Cahier des charges"
    
     

    C'est un projet complexe que l'on peut décomposer en différentes étapes correspondant à des fonctions Python réalisant chacune une action. Nous allons détailler ces étapes ici. La grille du jeu sera représenté par un tableau à deux dimensions. Les 0 correspondent à des cases vides, les 1 aux jetons du joueur 1 et les 2 aux jetons du joueur 2.    

    \(grille\)_\(init()\) : fontion qui renvoie un tableau de 6 lignes et 7 colonnes remplies de zéros ;    
    \(affiche\)_\(grille(tab)\) : fonction qui affiche la grille du jeu dans la console de la façon la plus esthétique possible (voir la suggestion ci-dessous à améliorer) ;        
    \(colonne\)_\(libre(tab, colonne)\) : fonction qui renvoie un booléen indiquant s'il est possible de mettre un jeton dans la colonne (indique si la colonne n'est pas pleine) ;  
    \(place\)_\(jeton(tab, colonne, joueur)\) : fonction qui place un jeton du joueur (1 ou 2) dans la colonne. Elle renvoie la grille modifiée ;   
    \(horizontale(tab, joueur)\) : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une ligne ;    
    \(verticale(tab, joueur)\) : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une colonne ;    
    \(diagonale(tab, joueur)\) : fonction qui renvoie True si le joueur a au moins 4 jetons alignés dans une diagonale ;    
    \(gagne(tab, joueur)\) : fonction qui renvoie True si le joueur a gagné ;    
    \(tour\)_\(joueur(tab, joueur)\) : fonction qui permet au joueur de placer un jeton dans la colonne choisie. Elle indique si la colonne est pleine et permet alors au joueur de choisir une autre colonne ;    
    \(egalite(tab)\) : fonction qui renvoie True s'il y a égalité et False sinon ;    
    \(jouer(tab)\) : fonction qui permet aux deux joueurs de jouer chacun leur tour. Elle vérifie que les joueurs n'ont pas gagné à la fin de leur tour. Si l'un des deux à gagné ou s'il y a égalité, elle donne le résultat ;    





!!! example "Exemple d'exécution"

    +---+---+---+---+---+---+---+  
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  
    | 0 | 0 | 0 | 0 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  
    | 0 | 0 | 2 | 1 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  
    | 0 | 0 | 1 | 2 | 0 | 0 | 0 |  
    +---+---+---+---+---+---+---+  


 







