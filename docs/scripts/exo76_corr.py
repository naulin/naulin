# Version sans les listes


# On initialise les premiers termes

premier = 0
second  = 1
print(premier, end=" ")
print(second, end=" ")

# On calcule les suivants et on les affiche directement

for i in range(13) :
    premier, second = second, premier + second
    # on utilise la double affectation a, b = c, d
    # cela est identique à a=c et b=d
    print(second, end=" ")


# version avec des listes
# On déclare une liste de 15 éléments
L=[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0]

for i in range(2,15) :
    L[i] = L[i-1] + L[i-2]

print()
print(L)
