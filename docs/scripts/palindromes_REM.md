---
author: Jean-Marc Naulin
title: Palindromes
tags:
  - Difficulté *
---

!!! info "Détection de palindromes"

    Un palindrome est un mot ou une phrase qui se lit de la même façon dans les deux sens. Par exemple, « radar » est un mot palindrome, et « Ésope reste ici et se repose » est une phrase palindrome.

    Le but de ce projet est de créer un programme qui détecte si un mot ou une phrase est un palindrome.

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme demandera à l'utilisateur de saisir un mot ou une phrase. Il indiquera ensuite si le mot ou la phrase est un palindrome ou non selon différents niveaux de sensibilité :

    palindrome strict - la chaîne de caractères est l'exact miroir d'elle-même (sans tenir compte des majuscules et des minuscules) :  
      * Été est un palindrome strict  
      * Radar est un palindrome strict  
      * Esope reste ici et se repose n'est pas un palindrome strict  
    palindrome sans espaces - si on enlève les espaces, la chaîne est un palindrome strict :  
      * Karine alla en Irak est un palindrome sans espaces  
      * Esope reste ici et se repose est un palindrome sans espaces  
      * Ésope reste ici et se repose n'est pas un palindrome sans espaces (à cause de l'accent sur le « E »)  
    palindrome flexibles - si on enlève les espaces, les cédilles, les accents et la ponctuation, la chaîne est un palindrome strict :  
      * Ésope reste ici et se repose. est un palindrome flexible  
      * La mère Gide digère mal. est un palindrome flexible  
      * Eh ! ça va la vache ? est un palindrome flexible  
      * Ésope reste ici et se repose est un palindrome flexible  





!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Pas un palindrome :

    Entrez un mot ou une phrase : Bonjour  
    Pas un palindrome !  
    Palindrome strict :  

    Entrez un mot ou une phrase : Kayak  
    Palindrome strict !  
    Palindrome sans espaces :  

    Entrez un mot ou une phrase : Karine alla en Irak  
    Palindrome sans espaces !  
    Palindrome flexible :  

    Entrez un mot ou une phrase : Eh ! ça va la vache ?  
    Palindrome flexible !  




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Indiquer quand un mot est presque un palindrome (indiquer s'il y a une lettre de différence, deux lettres de différence, etc.).
  * Ajouter une fonctionnalité pour détecter les palindromes dans le dictionnaire (en s'aidant du fichier suivant : dictionnaire). Votre programme génère alors une liste de palindromes stricts, une liste de palindromes sans espaces et une liste de palindromes flexibles.








