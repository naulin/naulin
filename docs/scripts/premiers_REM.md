---
author: Jean-Marc Naulin
title: Nombres Premiers
tags:
  - Difficulté **
---

!!! info "Liste des nombres premiers"

    On rappelle qu'un nombre premier est un nombre qui a exactement 2 diviseurs qui sont 1 et lui-même. Ainsi 2, 3, 5, 7 et 11 sont des nombres premiers mais 4, 6 et 9 n'en sont pas.

    Le but de ce projet est de créer une liste des nombres premiers.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme demande un nombre à l'utilisateur et affiche la liste des nombres premiers inférieurs ou égaux à ce nombre.

    Pour cela, vous pouvez tester tous les nombres de 2 au nombre donné par l'utilisateur les uns après les autres.


!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Saisir une limite : 30  
    Les nombres premiers de 2 à 30 sont : 2, 3, 5, 7, 11, 13, 17, 19, 23, 29.




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Utiliser la méthode du crible d'Ératosthène pour trouver les nombres premiers. Cette méthode consiste à supprimer tous les multiples d'un nombre premier. Par exemple, si l'on sait que 2 est un nombre premier, on peut supprimer tous les multiples de 2 (4, 6, 8, 10, etc.) puis chercher le prochain nombre premier (3) et supprimer tous ses multiples (9, 15, 21, etc.) et ainsi de suite. Cette méthode permet de trouver tous les nombres premiers inférieurs à une certaine limite en un temps raisonnable.






