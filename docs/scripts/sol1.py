def verifie(tableau) :
    if len(tableau) < 2 :
        return True
    for i in range(1, len(tableau)) :
        if tableau[i] < tableau [i-1] :
            return False
    return True