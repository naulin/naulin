def division_euclidienne(a,b):
    return None # Il va falloir écrire quelque chose


if __name__ == '__main__':
    assert division_euclidienne(10, 2) == (5, 0)
    assert division_euclidienne(2, 10) == (0, 2)
    assert division_euclidienne(37, 3) == (12, 1)
    assert division_euclidienne(-10, 7) == -1
    assert division_euclidienne(10, -7) == -1
    assert division_euclidienne(10.3, 4) == -1
    assert division_euclidienne(11, 3.5) == -1
    assert division_euclidienne(3, 0) == -1
    assert division_euclidienne(0, 3) == (0, 0)
    assert division_euclidienne(0, 0) == -1
