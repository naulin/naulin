---
author: Jean-Marc Naulin
title: Mot de passe
tags:
  - Difficulté *
---

!!! info "Analyseur de mots de passe"

    Le but de ce projet est de créer un assistant qui analyse la robustesse d'un mot de passe.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme demande d'abord à l'utilisateur de saisir un mot de passe. Si le mot de passe ne suit pas les règles, le programme indique pourquoi à l'utilisateur et l'invite à en saisir un nouveau. Un mot de passe ne suit pas les règles dans les cas suivants :

    il est trop court (4 caractères ou moins) ;
    il est trop long (plus de 20 caractères) ;
    il ne contient pas de majuscule ;
    il ne contient pas de minuscule ;
    il ne contient pas de chiffres.

    Si le mot de passe suit les règles, le programme affiche un message indiquant que le mot de passe est valide et demande à l'utilisateur de l'entrer à nouveau. Si les deux mots de passe sont identiques, le programme affiche un message de confirmation. Sinon, le programme recommence depuis le début.





!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Saisissez votre mot de passe : Top NSI  
    Ce mot de passe n'est pas sécurisé : il ne contient pas de chiffres.  
    Saisissez votre mot de passe : T0p NS1  
    Ce mot de passe est sécurisé.  
    Saisissez à nouveau votre mot de passe : Tob NS1  
    Erreur : les mots de passe ne sont pas identiques. Merci de recommencer.  

    Saisissez votre mot de passe : t0p ns1  
    Ce mot de passe n'est pas sécurisé : il ne contient pas de majuscule.  
    Saisissez votre mot de passe : T0p NS1  
    Ce mot de passe est sécurisé.  
    Saisissez à nouveau votre mot de passe : T0p NS1  
    Le mot de passe est correct !





!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Demander d'insérer un ou plusieurs caractères spéciaux.
  * Demander plusieurs mots de passe à l'utilisateur en associant à chaque fois un nom de site (par exemple, "Facebook", "Twitter", "Instagram", etc.). Le programme interdit alors à l'utilisateur d'entrer deux fois le même mot de passe pour des services différents. Il lui propose aussi, au lieu de saisir un mot de passe, d'afficher un mot de passe déjà enregistré pour un service donné.








