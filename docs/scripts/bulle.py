# Programme Python pour l'implémentation du Tri à bulle
 
def tri_bulle(tab):
    # Traverser tous les éléments du tableau
 
    for k in range(len(tab)-1, -1, -1):
        j = 0
        while j < k :
            # échanger si l'élément trouvé est plus grand que le suivant
            if tab[j] > tab[j+1] :
                tab[j], tab[j+1] = tab[j+1], tab[j]
                j=j+1
    return tab

# Programme principale pour tester le code ci-dessus
tab = [98, 22, 15, 32, 2, 74, 63, 70]
 

print(tab)
print ("Le tableau trié est:")
print(tri_bulle(tab))

