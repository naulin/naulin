# -*- coding: utf-8 -*-
"""
Created on Thu Mar  6 11:08:09 2025

@author: nsi
"""
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np

def evaluate(img_fname):
    img = image.load_img(img_fname, target_size=(256, 256))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    preds = model.predict(x)
    # print the probability and category name for the 5 categories 
    # with highest probability: 
    print(f"Probabilité pour la classe 1 chien : {preds[0][0] * 100:.2f}%")
    print(f"Probabilité pour la classe 2 chat : {preds[0][1] * 100:.2f}%")
    plt.imshow(img)
    
evaluate('dogs/dog.0.jpg')