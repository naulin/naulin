---
author: Jean-Marc Naulin
title: Frequence
tags:
  - Difficulté *
---

!!! info "Fréquence d'apparition d'une lettre dans un texte"

    Le but de ce projet est de créer un programme qui donne la fréquence d'apparition de chaque lettre dans un texte.

    Une des applications possibles de ce programme est de casser le code de César   

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme demande à l'utilisateur de saisir un texte. Il affiche ensuite la fréquence d'apparition de chaque lettre de l'alphabet dans le texte.

    Votre programme doit comporter les deux fonctions suivantes :

       * compter(lettre, texte) - prend une chaîne de longueur 1 lettre et une chaîne texte en paramètres et compte le nombre d'apparitions de lettre dans texte ;
       * frequences(texte) - prend une chaîne texte en paramètres et renvoie un tableau de 26 float contenant les fréquences d'apparition de chaque lettre de l'alphabet dans texte.

    Votre programme doit utiliser ces deux fonctions pour calculer les fréquences d'apparition de chaque lettre de l'alphabet dans le texte saisi par l'utilisateur.

    Les minuscules et les majuscules sont considérées comme étant la même lettre. Par exemple, la lettre a et la lettre A sont considérées comme étant la même lettre. De plus, les caractères accentués sont considérés comme étant la même lettre sans accent. Par exemple, la lettre é et la lettre e sont considérées comme étant la même lettre.




!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Saisir une phrase : Bonjour, comment ça va ?  

    Nombre total de lettres : 18  

    A : 2 (11.11111111111111 %)  
    B : 1 (5.555555555555555 %)  
    C : 2 (11.11111111111111 %)  
    D : 0 (0.0 %)  
    E : 1 (5.555555555555555 %)  
    F : 0 (0.0 %)  
    G : 0 (0.0 %)  
    H : 0 (0.0 %)  
    I : 0 (0.0 %)  
    J : 1 (5.555555555555555 %)  
    K : 0 (0.0 %)  
    L : 0 (0.0 %)  
    M : 2 (11.11111111111111 %)  
    N : 2 (11.11111111111111 %)  
    O : 3 (16.666666666666664 %)  
    P : 0 (0.0 %)  
    Q : 0 (0.0 %)  
    R : 1 (5.555555555555555 %)  
    S : 0 (0.0 %)  
    T : 1 (5.555555555555555 %)  
    U : 1 (5.555555555555555 %)  
    V : 1 (5.555555555555555 %)  
    W : 0 (0.0 %)  
    X : 0 (0.0 %)    
    Y : 0 (0.0 %)  
    Z : 0 (0.0 %)    




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Généraliser à la fréquence d'apparition des mots dans un texte. Il faut d'abord détecter les mots dans le texte.
  * Casser le code de César (voir le projet F-5). Pour cela, il faut calculer la fréquence d'apparition de chaque lettre dans le texte chiffré. Ensuite, il faut comparer ces fréquences avec les fréquences d'apparition des lettres dans la langue française. Enfin, il faut déterminer la substitution la plus probable.








