#!/usr/bin/env python
# coding: utf-8



import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras import *


# Définir l'entrée et la sortie sous forme de tableaux numpy
entree = np.array([1, 2, 3, 4, 5]).reshape(-1, 1)  # Redimensionne pour que chaque valeur soit une "ligne"
sortie = np.array([10, 20, 30, 40, 50])

#model=Sequential()

# Création du modèle
model = keras.Sequential([
    layers.Input(shape=(1,)),  # Définir la forme de l'entrée avec Input(shape=(1,))
    layers.Dense(units=64),     # Ajout d'une couche Dense
    layers.Dense(units=1)      # Ajout d'une autre couche Dense de sortie
])

# Compilation du modèle
model.compile(optimizer='adam', loss='mean_squared_error')

# Entraîner le modèle
model.fit(entree, sortie, epochs=2500)


while True:
    x = int(input("Nombre : "))  # Demander à l'utilisateur d'entrer un nombre
    # Transformer 'x' en un tableau numpy avec une forme correcte pour la prédiction
    x_input = np.array([[x]])  # Redimensionner pour que cela soit une entrée de forme (1, 1)
    prediction = model.predict(x_input)  # Faire la prédiction avec l'entrée
    print("prédiction : " + str(prediction))  # Afficher la prédiction





