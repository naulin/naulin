def division_euclidienne(a,b):
    """(int,int) -> tuple(int,int)
    Preconditions: a >= 0 et b > 0 avec a et b entiers.
    Si les préconditions ne sont pas respectées, doit renvoyer -1.
    Fonction effectuant la division euclidienne de a par b en utilisant
    l'algorithme par soustraction. Renvoie un doublet (quotient,reste), de
    sorte que a = quotient*b + reste avec 0 <= reste < b.

    Cas "normaux" de division euclidienne
    >>> division_euclidienne(10, 2)
    (5, 0)
    >>> division_euclidienne(2, 10)
    (0, 2)
    >>> division_euclidienne(37, 3)
    (12, 1)

    Si les arguments sont négatifs -> Erreur
    >>> division_euclidienne(-10, 7)
    -1
    >>> division_euclidienne(10, -7)
    -1

    Si les arguments ne sont pas entiers -> Erreur
    >>> division_euclidienne(10.3, 4)
    -1
    >>> division_euclidienne(11, 3.5)
    -1

    Division de 0
    >>> division_euclidienne(0, 3)
    (0, 0)

    Division par 0
    >>> division_euclidienne(3, 0)
    -1
    >>> division_euclidienne(0, 0)
    -1
    """
return None # Il va falloir écrire quelque chose


if __name__ == '__main__':
    import doctest
    doctest.testmod()