---
author: Jean-Marc Naulin
title: Nim
tags:
  - Difficulté *
---

!!! info "Le jeu de Nim"

    Le but de ce projet est d'écrire un programme qui permet de jouer au jeu de Nim. Le jeu de Nim est un jeu de stratégie pour deux joueurs. Au départ il y a 21 allumettes. Les joueurs jouent à tour de rôle. À chaque tour, un joueur doit enlever 1, 2 ou 3 allumettes. Le joueur qui prend la dernière allumette a perdu.

## Travail à faire : 



!!! warning "Cahier des charges"

    Votre programme a deux modes de jeu :

    mode joueur contre joueur - les deux joueurs jouent à tour de rôle sur le même ordinateur ;  
    mode joueur contre ordinateur - le joueur joue contre l'ordinateur.  

    Dans les deux modes de jeu, le programme doit d'abord demander au(x) joueur(s) leur nom. Ensuite, à chaque tour, il doit afficher le nombre d'allumettes restantes et le nom du joueur qui doit jouer. À la fin de la partie, le programme doit aussi afficher le nom du joueur qui a gagné.

    Dans le mode joueur contre ordinateur, l'ordinateur doit jouer de manière intelligente, au moins au dernier tour : il doit toujours éviter de perdre si c'est possible.


!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Contre un humain :

    Vous voulez jouer :  
    1. Contre un humain  
    2. Contre l'ordinateur  
    Choix : 1  
    Nom du joueur 1 : Alice  
    Nom du joueur 2 : Bob  
    Il reste 21 allumettes :  
    |||||||||||||||||||||  
    C'est au tour d'Alice.  
    Enlever (1-3) : 3  
    Il reste 18 allumettes :  
    ||||||||||||||||||  
    C'est au tour de Bob.  
    Enlever (1-3) : 2  
    Il reste 16 allumettes :  
    ||||||||||||||||  
    C'est au tour d'Alice.  
    ...  
    Il reste 2 allumettes :  
    ||  
    C'est au tour d'Alice.  
    Enlever (1-2) : 1  
    Alice a gagné !  

    Contre un ordinateur :

    Vous voulez jouer :  
    1. Contre un humain  
    2. Contre l'ordinateur  
    Choix : 2  
    Nom du joueur : Alice  
    Il reste 21 allumettes :  
    |||||||||||||||||||||  
    C'est au tour d'Alice.  
    Enlever (1-3) : 3  
    Il reste 18 allumettes :  
    ||||||||||||||||||  
    C'est au tour de l'ordinateur.  
    Il enlève 2 allumettes.  
    Il reste 16 allumettes :  
    ||||||||||||||||  
    ...  
    Il reste 2 allumettes :  
    ||  
    C'est au tour d'Alice.  
    Enlever (1-2) : 1  
    Alice a gagné !  





!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Proposer de faire varier les paramètres du jeu (nombre d'allumettes initial, nombre d'allumettes maximum à enlever par tour, etc.).
  * Choisir un niveau d'intelligence pour l'ordinateur (facile, moyen, difficile). En mode difficile, l'ordinateur doit toujours gagner si c'est possible.
  * Proposer de jouer à plus de deux joueurs.








