#La déclaration if ...

if 10 > 5 :
    print("10 is greater than 5")
    print("Program ended")

#La déclaration if ... else ...

x = 3
if x == 4 :
    print("Yes")
else :
    print("No")

# La déclaration if ... elif ...

num = 10
if num > 15 :
    print("Bigger than 15")
elif num <= 15 :
    print("Between 0 and 15")


