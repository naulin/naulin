---
author: Jean-Marc Naulin
title: JeuPlateforme
tags:
  - Difficulté **
---

!!! info "Jeu de Plateforme"
    Je vous propose de créer un petit jeu de plateforme en utilisant la bibliothèque Pygame.   

Lien pour apprendre et progresser : [Lien vers site Pygame](https://glassus.github.io/terminale_nsi/T7_Divers/6_Pygame/05_Initiation_Pygame/)

## Prérequis

!!! info "A savoir faire :"
    structures de contrôle ;  
    boucles ;  
    tableaux à deux dimensions ;  
    fonctions ;  
    modules ;  



## Travail à faire : 


!!! warning "Cahier des charges"
    Le défi de la création de ce jeu consiste principalement à se familiariser avec le positionnement dans le plan. Il faudra
    inclure dans votre gestion des ennemis (en utilisant des listes, tuples ou dictionnaires).

      


!!! example "Exemple d'exécution"

![morse](../images/Plateforme.png){ width=50%; : .center }


 







