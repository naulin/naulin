---
author: Jean-Marc Naulin
title: Cesar
tags:
  - Difficulté *
---

!!! info "Le code de César"

    Le code de César est une manière de chiffrer un message de manière simple : On choisit un nombre (appelé clé de chiffrement) et on décale toutes les lettres de notre message du nombre choisi.

    Par exemple, si on choisit comme clé le nombre 4 alors la lettre A devient E, le B devient F, ... et le Z devient D. Ainsi, le mot ZEBRE est chiffré en DIFVI.

    On remarque que pour décoder un message chiffré avec le code de César, il suffit de choisir la clé inverse (ici -2) et de réappliquer le décalage.

    Le but de ce projet est de créer un programme qui permet de chiffrer et de déchiffrer des messages avec le code de César.

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme demande à l'utilisateur de saisir une phrase à chiffrer ou déchiffrer, ainsi que la clé (un nombre entier, positif ou négatif). Il affiche ensuite le message chiffré ou déchiffré.

    Votre programme doit pouvoir gérer les majuscules et les minuscules. Par exemple, si l'utilisateur saisit Bonjour et la clé 2, votre programme doit afficher Dpncqpr.

    De plus, votre programme doit pouvoir gérer les caractères spéciaux (espaces, ponctuation, ...). Par exemple, si l'utilisateur saisit Bonjour, comment ca va ? et la clé 2, votre programme doit afficher Dpncqpr, eqoovg ec xc ?.




!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    Exemple 1 : 

    Entrez un message : ZEBRE  
    Entrez la clé : 4  
    DIFVI  
    Exemple 2 :  

    Entrez un message : Dpncqpr  
    Entrez la clé : -2  
    Bonjour  
    Exemple 3 :  

    Entrez un message : Bonjour, comment ca va ?  
    Entrez la clé : 15  
    Qdcydjg, rdbbtci rp kp ?  




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Ajouter la gestion des caractères accentués (é, è, à, ç, ...). Avant de chiffrer un tel caractère, il faut lui enlever son accent.
  * Chiffrement polyalphabétique : le chiffre de Vigenère. Cette méthode de chiffrement est plus complexe que le chiffre de César. Elle consiste à utiliser un mot ou une phrase au lieu d'un nombre pour chiffrer le message. On associe à chaque lettre de la clé un décalage (A=1, B=2, C=3, ...). On chiffre ensuite la première lettre du message avec la première lettre de la clé, la deuxième lettre du message avec la deuxième lettre de la clé, etc. Si la clé est plus courte que le message, on recommence à la première lettre de la clé. Par exemple, si le message est Bonjour et la clé ABC, on chiffre B avec A, o avec B, n avec C, j avec A, o avec B, u avec C, r avec A. On obtient donc le message chiffré Copkps. Pensez à permettre le déchiffrement avec la même clé.








