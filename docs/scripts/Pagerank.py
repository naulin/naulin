# -*- coding: utf-8 -*-
"""
Created on Thu Nov 23 16:18:16 2023

@author: jeanmarc.naulin
"""

from random import *

def hasard(PagePrec):
    if PagePrec=="A":
        choix=choice(["B","C","D"])
    if PagePrec=="B":
        choix="A"
    if PagePrec=="C":
        choix=choice(["A","D"])
    if PagePrec=="D":
        choix="B"
    return choix


L=[]
NewPage="A"
print(hasard(NewPage))
L.append(NewPage)


for i in range(100): 
    NewPage=hasard(NewPage)
    print(NewPage)
    L.append(NewPage)
    
print(L)
print(L.count("A"))
print(L.count("B"))
print(L.count("C"))
print(L.count("D"))
  




