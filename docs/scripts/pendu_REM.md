---
author: Jean-Marc Naulin
title: Pendu
tags:
  - Difficulté **
---

!!! info "Le jeu du pendu"

    Le but de ce projet est de créer le jeu du pendu. Le jeu du pendu est un jeu de devinette consistant à trouver un mot en devinant les lettres qui le composent. Le joueur a un nombre limité de tentatives pour trouver le mot. À chaque tentative infructueuse, une partie du corps du pendu est dessinée. Le jeu se termine quand le joueur a trouvé le mot ou quand le pendu est complètement dessiné.

## Travail à faire : 


!!! warning "Cahier des charges"

    Votre programme choisit un mot au hasard parmi une liste de mots prédéfinie. Il affiche alors le mot avec toutes les lettres remplacées par le caractère _. Il demande alors à l'utilisateur de saisir une lettre, en indiquant le nombre de tentatives restantes. Si la lettre est présente dans le mot, le programme remplace les _ dissimulant cette lettre dans le mot par la lettre. Si la lettre n'est pas présente dans le mot, le programme affiche le pendu avec une partie de plus dessinée. Le programme continue ainsi jusqu'à ce que le joueur ait trouvé le mot ou que le pendu soit complètement dessiné.



!!! example "Exemple d'exécution"

    Voici un exemple d'exécution de votre programme :

    _______  
    7 tentatives restantes.  
    Lettre : B  
    B______  
    7 tentatives restantes.  
    Lettre : A  
          +-------+  
      |  
      |  
      |  
      |  
      |  
    ==============  
    B______  
    6 tentatives restantes.  
    Lettre : E  
          +-------+  
      |       |  
      |       O  
      |  
      |  
      |  
    ==============  
    B______  
    5 tentatives restantes.  
    Lettre : O  
    BO__O__  
    5 tentatives restantes.  
    Lettre : U  
    BO__OU_  
    5 tentatives restantes.  
    Lettre : T  
          +-------+  
      |       |  
      |       O  
      |       |  
      |  
      |  
    ==============  
    BO__OU_  
    4 tentatives restantes.  
    Lettre : R  
    BO__OUR  
    4 tentatives restantes.  
    Lettre : N  
    BON_OUR  
    4 tentatives restantes.  
    Lettre : J  
    BONJOUR  
    Félicitations, vous avez trouvé en faisant 3 erreurs !  




!!! info "Pour aller plus loin :"

  Une fois que vous avez atteint le dernier palier des fonctionnalités, voici quelques idées pour aller plus loin :

  * Mémoriser les lettres déjà proposées par l'utilisateur et ne pas compter les tentatives infructueuses sur ces lettres.
  * Choisir un mot parmi la liste des mots contenue dans le fichier suivant : dictionnaire.

!!! warning " Aide"

    Vous pouvez utiliser la constante DESSINS_PENDU suivante (de type tableau de chaînes de caractère) pour obtenir une chaîne de caractère représentant le pendu à différentes étapes :

    DESSINS_PENDU = [  
    """  
      +-------+  
      |  
      |  
      |  
      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |   
      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |       |  
      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |      -|  
      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |      -|-  
      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |      -|-  
      |      |  
      |  
    ==============  
    """,  
    """  
      +-------+  
      |       |  
      |       O  
      |      -|-  
      |      | |  
      |  
    ==============  
    """  
    ]  








