# Programme Python pour l'implémentation du tri par insertion
def tri_insertion(t):
    for i in range(len(t)) :
        k = i
        while k > 0 and t[k-1] > t[k] :
            t[k-1], t[k] = t[k], t[k-1]
            k =  k - 1
        print(t)
    return t
        

# Programme principale pour tester le code ci-dessus
tab = [98, 22, 15, 32, 2, 74, 63, 70]

print(tab)
print ("Le tableau trié est:")
print(tri_insertion(tab))

