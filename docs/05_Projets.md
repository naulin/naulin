# Projets

## Liste des projets 1 Python

!!! warning "Choisir, faire et rendre un projet"
    Vous devez vous mettre par groupes de 2. Vous pouvez aussi faire le projet tout(e) seul(e) mais les exigences sont les mêmes que si vous étiez en groupe !


    Pour réserver un projet, envoyez-moi un message sur Pronote avec le code du projet et la personne avec qui vous souhaitez travailler. 
    Evitez de prendre tous le même!


Barème

Le barème décompose en 4 parties :

Fonctionnalités (8 points)

* Palier 1 (0~2/8) : Le programme correspond à peu près aux spécifications demandées

* Palier 2 (2~4/8) : Le programme correspond aux spécifications demandées

* Palier 3 (4~6/8) : Le programme ne présente aucun bug et correspond aux spécifications demandées

* Palier 4 (6~8/8) : Des fonctionnalités supplémentaires ont été ajoutées (des suggestions sont données sur la fiche de chaque projet)


Algorithmes et code (6 points)

* Utilisation pertinente de structures conditionnelles, de boucles, de variables, etc.

* Utilisation correcte des différents types de valeurs et des fonctions de conversion quand nécessaire

* Décomposition fonctionnelle pertinente (chaque fonction est suffisamment petite, on évite la répétition de code)


Style du code (3 points)

* Respect des règles de style vues en cours

* Utilisation de noms pertinents pour les variables et les fonctions

* Documentation pertinente des fonctions grâce à des docstrings

* Utilisation pertinente des commentaires


Interface utilisateur (3 points)

* Bonne organisation visuelle de l'interface

* Cohérence de l'interface

* Communication d'informations à l'utilisateur efficace et compréhensible

* Adaptation à l'utilisateur et gestion des erreurs de saisie


Pour partager le projet entre vous, vous pouvez utiliser :

[codeskulptor](https://py3.codeskulptor.org/)

[codeshare](https://codeshare.io/)

[codesandbox](https://codesandbox.io/)


**Lien pour déposer une version à corriger**

Pensez à indiquer le problème rencontré.

[Lien dépôt version](https://nuage04.apps.education.fr/index.php/s/ofc7Bsn62NPTMb6)


Projets :

Voici une liste de projets : 


* Logiciel d'apprentissage des tables de multiplication [details](../scripts/multiplication_REM/)

* Analyseur de mots de passe [details](../scripts/motdepasse_REM/)

* Le jeu du nombre mystère [details](../scripts/nombremystere_REM/)

* Détection de palindromes [details](../scripts/palindromes_REM/)

* Le code de César [details](../scripts/cesar_REM)

* Calculatrice [details](../scripts/calculatrice_REM)

* Répertoire téléphonique [details](../scripts/repertoire_REM)
* Le jeu du pendu [details](../scripts/pendu_REM)
* Générateur de QCM [details](../scripts/qcm_REM)
* Le jeu de Nim [details](../scripts/nim_REM)
* π, un nombre univers [details](../scripts/pi_REM)
* Calcul mental [details](../scripts/calculmental_REM)
* Palindromes numériques [details](../scripts/palinnum_REM)
* Fréquence d'apparition d'une lettre dans un texte [details](../scripts/frequence_REM)
* Calendrier [details](../scripts/date_REM)
* Liste des nombres premiers [details](../scripts/premiers_REM)
* Morse [details](../scripts/morse_REM) 


## Liste des projets 2 Python

!!! warning "Choisir, faire et rendre un projet"
    Pour ce projet vous devez vous mettre par groupes de 2. La difficulté étant plus grande et la durée aussi, il vous
    faudra apprendre à partager et communiquer votre travail (Un rapport devra être fait sur qui fait quoi et chacun
    devra être capable d'expliquer sa partie de travail). Certains projets auront une partie graphique, pour cela une aide sera donnée.


    Pour réserver un projet, envoyez-moi un message sur Pronote avec le code du projet et la personne avec qui vous souhaitez travailler. 
   
Projets :

Voici une liste de projets (description en cours).

Dossier projet à faire [details](../scripts/CahierdesCharges.odt) 


* Morpion (Version console) [details](../scripts/morpion_REM)
* Puissance4 (Version console) [details](../scripts/puissance4_REM)
* Bataille Navale (Version Console) [details](../scripts/bataille_REM)
* Jeu de la vie [details](../scripts/jeudelavie_REM)
* Filtre image [details](../scripts/filtre_REM)
* Jeu de plateforme (Pygame ou Pyxel) [details](../scripts/jeuplateforme_REM)
* Générateur d'arbres (L-system) [details](../scripts/lsystem_REM)

Aide pour Pygame (scrolling à améliorer) :

[prog Python](defilement.py)
[image 1 mario](02_chapitre_2/images/mario.png)
[image 1 background](02_chapitre_2/images/background1.png)



## Liste des projets Terminale

* [Lemmings](https://info.blaisepascal.fr/lemmings/)
* [Jeu 2D Pixel POO](https://www.nsirenoir.fr/premiere/projet_jeu.html)
* [Labyrinthe](https://nguyen.univ-tln.fr/projet-labyrinthe.html)
* [Rpg](https://www.pixel-maniac.com/article/Les-Premiers-RPG/)