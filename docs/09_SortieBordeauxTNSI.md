# Les NSI à Bordeaux

"Quand la NSI nous ouvre les portes de l'avenir numérique !"

[Lien Linkedln](https://www.linkedin.com/posts/doriane-debomy-879736a7_semainensi-daezveloppementweb-webdesign-activity-7269748472596103168-vpJN?utm_source=share&utm_medium=member_desktop)

![morse](../images/groupe.jpg){ width=50%; : .center }

Dans le cadre de la [Semaine de la NSI](https://www.semaine-nsi.fr/) qui se déroule du 2 au 9 décembre 2024, les terminales NSI
se sont déplacés dans la belle ville de Bordeaux accompagnés de leur Professeur (M. Naulin) dans 
le but de visiter une école d'informatique (Nexa digital School).
Le but était de plonger au cœur de l’école d’informatique pour comprendre ses approches pédagogiques 
et explorer les multiples voies d’accès aux métiers de l'informatique.  Une présentation de l'I.A était
prévue en matinée suivie d'une présentation du métier de désigner web l'après midi.




<!DOCTYPE html>
<div class="slideshow-container">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diaporama</title>
    <style>
        /* Style général pour le conteneur */
        .slideshow-container {
            position: relative;
            max-width: 300px;
            margin: auto;
            overflow: hidden;
        }

        /* Chaque slide (image) */
        .slide {
          
            display: flex; /* Flexbox pour centrer */
            justify-content: center; /* Centre horizontalement */
            align-items: center; /* Centre verticalement */
            width: 100%; /* Prend toute la largeur disponible */
            height: 300px; /* Définissez une hauteur fixe pour les slides */
        }

        /* Images */
        .slide img {
            width: 100%;
            border-radius: 10px;
        }

        /* Flèches de navigation */
        .prev, .next {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            color: white;
            font-size: 18px;
            padding: 10px;
            cursor: pointer;
            border-radius: 50%;
            background: rgba(0, 0, 0, 0.5);
        }

        .prev { left: 10px; }
        .next { right: 10px; }

        /* Points indicateurs */
        .dots {
            text-align: center;
            margin-top: 10px;
        }

        .dot {
            height: 10px;
            width: 10px;
            margin: 0 5px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            cursor: pointer;
        }

        .dot.active {
            background-color: #717171;
        }
    </style>
</head>
<body>

<div class="slideshow-container">
    <!-- Slide 1 -->
    <div class="slide">
        <img src="../images/groupe.jpg" alt="Photo 1">
    </div>
    <!-- Slide 1 -->
    <div class="slide">
        <img src="../images/photo1.jpg" alt="Photo 1">
    </div>
    <!-- Slide 2 -->
    <div class="slide">
        <img src="../images/photo2.jpg" alt="Photo 2">
    </div>
    <!-- Slide 3 -->
    <div class="slide">
        <img src="../images/photo3.jpg" alt="Photo 3">
    </div>
    <!-- Slide 3 -->
    <div class="slide">
        <img src="../images/photo4.jpg" alt="Photo 3">
    </div>

    <!-- Flèches -->
    <a class="prev">&#10094;</a>
    <a class="next">&#10095;</a>
</div>

<!-- Points indicateurs -->
<div class="dots">
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
</div>

<script>
    // Variables
    let slideIndex = 0;
    const slides = document.querySelectorAll(".slide");
    const dots = document.querySelectorAll(".dot");

    // Fonction pour afficher les slides
    function showSlides(index) {
        // Boucle pour cacher toutes les slides
        slides.forEach((slide, i) => {
            slide.style.display = (i === index) ? "block" : "none";
            dots[i].classList.toggle("active", i === index);
        });
    }

    // Fonction pour faire avancer automatiquement les slides
    function autoSlide() {
        slideIndex = (slideIndex + 1) % slides.length; // Boucle après la dernière slide
        showSlides(slideIndex);
    }

    // Navigation avec flèches
    document.querySelector(".prev").addEventListener("click", () => {
        slideIndex = (slideIndex - 1 + slides.length) % slides.length;
        showSlides(slideIndex);
    });
    document.querySelector(".next").addEventListener("click", () => {
        slideIndex = (slideIndex + 1) % slides.length;
        showSlides(slideIndex);
    });

    // Navigation avec les points
    dots.forEach((dot, i) => {
        dot.addEventListener("click", () => {
            slideIndex = i;
            showSlides(slideIndex);
        });
    });

    // Démarrer le diaporama
    showSlides(slideIndex);
    setInterval(autoSlide, 3000); // Défiler toutes les 3 secondes
</script>

</body>
</html>

</div>


## 11h : Accueil et présentation des participants autour d'une collation

![morse](../images/photo5.jpg){ width=30%; : .center }

## 11h30 : 1er atelier – Découverte de l’IA

La coordinatrice de l’école Marie-Maï et Tom un étudiant en master nous ont présenté l’IA.  
Les différentes sortes d’IA, les jeux de données, comment l’IA peut être biaisé.  
Les différentes sortes d’IA pouvant être analytique, cognitive, générative, décisionnelle ou adaptative, ils nous
expliqué les différents fonctionnements et les applications associées.  

Nous avons ensuite abordé brièvement le côté polémique de l’intelligence artificielle notamment sur les problèmes de droits d’auteur mais également de la possible suppression de postes de développeurs à cause de l’IA. Nous avons aussi évoqué le problème écologique de cette technologie.


| ![morse](../images/photo6.jpg){ width=80%; : .center } |![morse](../images/photo7.jpg){ width=80%; : .center } |
|:-----------------------------:|:-----------------------------:|
| Présentation d'un programme Python      | Description des I.A.      |

 

## 14h30 : 2ème atelier – Découverte du design web

Après la pause déjeuner nous avons fait plusieurs groupes pour discuter avec des élèves en webdesign pour nous présenter leur projet.  
Chaque groupe nous a présenté son moodboard qui sert à capter au mieux une ambiance recherchée par le client pour son produit.   
Ensuite une présentation de leur design, adapté à plusieurs formats qu’ils ont créé grâce au logiciel collaboratif Figma. Mais ils nous ont également expliqué que la psychologie était essentielle dans ce travail puisque qu’elle permet de définir au mieux quel type de personne est visé par ce produit.  
Un autre logiciel nous a été présenté brièvement : Photoshop

| ![morse](../images/photo8.jpg){ width=80%; : .center } |![morse](../images/photo9.jpg){ width=80%; : .center } |
|:-----------------------------:|:-----------------------------:|
| Discussion sur le web design      | Discussion sur le web design       |

| ![morse](../images/photo10.jpg){ width=80%; : .center } |![morse](../images/photo11.jpg){ width=80%; : .center } |
|:-----------------------------:|:-----------------------------:|
| Discussion sur le web design      | Présentation de Photoshop      |



## 15h30 : Présentation de Nexa, formations et modalités d’accès

Nous avons fait un bilan de la journée en donnant chacun notre ressenti sur la journée avec 
la directrice Doriane Debomy qui nous a ensuite présenté les différents parcours possibles dans cette école et surtout
les modalités d'accès.

![morse](../images/photo12.jpg){ width=30%; : .center }

## 15h30 : Synthèse de la journée et goûter

Discussions autour d'un Goûter offert par l'école et très apprécié par les élèves.

![morse](../images/photo13.jpg){ width=30%; : .center }

## Bilan :

Lors de leur visite à Nexa Digital School, les élèves ont exprimé un mélange d'enthousiasme et de curiosité. Pour beaucoup, c'était leur première immersion dans un environnement dédié aux métiers de l'informatique et de la technologie. Ils ont particulièrement apprécié l’accueil chaleureux des étudiants et des enseignants.  
Certains ont trouvé les explications sur les projets en cours très inspirantes, tandis que d'autres ont apprécié les démonstrations pratiques qui les ont aidés à mieux comprendre les concepts de programmation et de gestion de projets. "Nous avons été très surpris par l'ampleur du travail que cela implique, car nous n’avions jamais vu ce qui se cache derrière tout le processus de conception d’un logo", a confié l'un des participants.  
Globalement, cette expérience a renforcé leur intérêt pour les études en informatique et leur a permis d’envisager plus concrètement leur futur dans ce domaine en pleine expansion.

" J’ai beaucoup aimé l’expérience, ça a été une découverte du monde professionnel, des aspects de l’informatique et sa variété. J’invite tout le monde à découvrir et aimer une réalité et profession que Nexa Digital School offre"  

"L’immersion avec des groupes de web
designer était très instructive. En effet
nous nous sommes intégrés dans des
groupes répondant a une réelle demande
et cela faisait que nous nous sentions
vraiment immergés dans leur travaux"  

"Pour conclure, cette journée était intéressante, elle nous a appris des choses sur l’intelligence artificielle même si ils n’en présentaient que la surface. Le problème est que nous n’avons pratiquement pas parlé de programmation ce qui est dommage, pour l’activité sur le webdesign ce sont les designers web qui nous ont présenté leur travail mais les développeurs qui s’occupent de la partie programmation étaient absents. Donc nous n’avons vu que la partie purement graphique des projets ce qui était intéressant mais nous n’étions pas là pour ça."

"Nexa School était une très bonne expérience dus par son atmosphère qui était très chaleureux et par ses interactions humaines"

"Nous avons pu découvrir l’IA ainsi que le métier de web designer. J’avais un a priori sur ce métier, je pensais qu’il était essentiellement constitué de codage, alors que non, il y a aussi une partie graphique. J’ai également beaucoup aimé l’atmosphère au sein de l’école"

"ce qui est ressorti de nos interactions est l’attention énorme portée à la partie artistique du projet, laissant la part d’informatique délaissée , au moins de mon point de vue"

"L’ambiance entre étudiants et personnel est positive et porte un sens de confort. Je ne m’attendais pas à ce que la taille de cet établissement soit aussi petite que ce quelle était. "

Voici un Padlet que nous avons créé pour partager nos bilans.

<iframe src="https://padlet.com/naulinjeanmarc/tableau-d-affichage-vwukqzx21l18ieyq" width="100%" height="500" frameborder="0" scrolling="no"></iframe>