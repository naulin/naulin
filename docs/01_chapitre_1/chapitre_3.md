---
author: Votre nom
title: Les réseaux sociaux
---

[![texte alternatif de l'image](http://img.youtube.com/vi/nn1mIqW9oYQ/0.jpg){: .center}](https://youtu.be/nn1mIqW9oYQ "Titre de la video")


## I. Créer la carte d’identité d’un réseau social :

Choisissez un réseau social à étudier.  


Après avoir rappelé le principe de fonctionnement du réseau social que vous aurez choisi, vous essayerez de trouver sur le net son nombre d'abonnées actuel (et si possible sa "dynamique de croissance" : est-ce que son nombre d'abonnées est plutôt en hausse, plutôt en baisse ou stable).  

Vous répondrez ensuite à cette série de questions :  

Les utilisateurs de ce réseau social ont-ils la possibilité de "régler" des "paramètres de confidentialité" afin de pouvoir préserver le respect de leur vie privée ? Si, oui, ces "paramètres de confidentialité" sont-ils facilement accessibles ?  
Est-il facile de se désinscrire de ce réseau social ?  
Le réseau social propose-t-il une procédure simple afin qu'un utilisateur puisse obtenir une copie des données qui auront été "récoltées" sur lui ? Si oui, la procédure est-elle simple à mettre en oeuvre ?  
Le réseau social efface-t-il toutes traces des données personnelles d'un utilisateur qui s'est désinscrit ?  
Enfin, vous vous intéresserez aux sources de revenus du réseau social.   

Indiquer	les	critères	suivants	dans	la	fiche	d’identité	du	réseau	social :  
Logo	de	la	plate-forme ;  
Nom	du	réseau	social ;  
Date	de	sa	création ;  
Nom	du	(des)	créateur(s) ;  
Slogan ;  
Nombre	de	membres	ou	abonnés	(à	la	date	de	la	recherche)	;  
Adresse	(ou	pays)	du	siège	social ;  
Pays	d’origine ;  
Modèle	économique	(gratuit,	financé	par	la	pub	ou	payant,	avec	abonnements) ;  
Type	de	réseaux/média	social	(à	choisir	entre :	Publication	de	contenus	personnels,	Partage	de	
contenus,	Messageries	instantanées,	Discussion	en	ligne,	Réseautage,	Autres) ;  
A	savoir	(Controverses,	particularités,	anecdotes,	etc.)	;  

![morse](https://i.ibb.co/ygX6q0g/Reseausocial.png){ width=100%; : .center }


## II. Modélisation d’un réseau social :

Le but de l’activité est de modéliser les relations d'un réseau social à l'aide de graphes, et d'introduire les notions de matrice d'adjacence et de diamètre d'un graphe.

[Lien vers le fichier de travail](https://capytale2.ac-paris.fr/web/c/b555-2912986)


