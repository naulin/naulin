---
author: Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: Web
---
 
**auteur: Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac**

## I. Présentation

### Modèle client-serveur

<div class="centre"><center><iframe src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

### Page WEB
<div class="centre"><center>
<iframe src="https://player.vimeo.com/video/138623756?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

### HTML, CSS et JS
<div class="centre"><center><iframe src="https://player.vimeo.com/video/138623826?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></center></div>

!!! abstract "Contenu et style"

    &#128083; Une page **HTML** est écrite en caractères lisibles mais codée.  
    Il n'est pas question d'étudier l'ensemble des possibilités du langage HTML, seulement les bases.

    &#128087;&#128086; / &#128221; On sépare le **style**(mise en page, couleurs, taille, type de polices de caractère, couleur de fond de page, ou des blocs,
    etc... du **contenu**.  
    &#128073; Le style est généralement défini dans un fichier séparé (ce n'est pas obligatoire, mais c'est très recommandé, et c'est ainsi que nous allons travailler).

    &#127797; Encore faut-il bien comprendre ce qu'on entend par le style et le contenu ! 
   


## II. Structure générale d'une page HTML

!!! abstract "Un début"

    ```html 
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
        </body>
    </html>
    ```


!!! abstract "Structure"

    Un document **HTML** est constitué d'un **document** contenant une **en-tête**, suivi d'un **corps**. 

    Repérer les balises permettant de délimiter :  

    * Le document

    ??? success "Solution"

        ```html
        <html>
        </html>
        ```

    * L'en-tête

    ??? success "Solution"

        ```html
        <head>
        </head>
        ```

    * Le corps

    ??? success "Solution"

        ```html
        <body>
        </body>
        ```

## III. Un premier exemple : 

Suivre ce lien, puis recopier dans la partie HTML le code ci-dessous.
Pour visualiser le rendu, cliquer sur Run.

[jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

!!! example "Exemple à recopier"

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       Il peut évidement y en avoir plusieurs.
                </p>         
        </body>
        </html>
        ```

Nous allons étudier ceci plus en détail.

😥 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." n'est pas écrite à la ligne comme sur le code.

👉 HTML ne prend pas en compte les espaces ou les sauts de lignes. Pour créer un saut de ligne, il faudra par exemple utiliser une balise `<br>`

## IV. Le concept de balises ouvrantes et fermantes, ou orphelines

### 1. Balises en paires

!!! abstract "Blocs"

    Comme vous pouvez commencer à le voir, dans l'exemple ci-dessus, une page <b>html</b> est organisée en blocs, 
    imbriqués les uns dans les autres. Le <b class="blue">header</b> est inclus dans le <b class="blue">html</b >, et le <b class="blue">title</b> est inclus dans
    le <b class="blue">header</b>


    Les blocs sont délimités par deux **balises**.  
    Par exemple :
    
    * **`<head>`** indique le début du header. C'est une **balise ouvrante**.
    * **`</head>`** indique la fin du header. C'est une **balise fermante**.


!!! abstract "Paires"

    On parle ici de balises en paire, avec une <b>balise ouvrante</b> et une <b>balise fermante</b>. 

!!! warning "&#128546; Et si on oublie une balise ?"

    
    Les navigateurs sont en général très tolérants, et une balise incorrecte (par exemple, il manque une fermante, ou il y a une fermante mais pas d'ouvrante...)
    sera ignorée. Dans les cas des balises ouvrantes sans fermante, cela engendrera le plus souvent un affichage incorrect et peu lisible. Dans
    le cas des balises fermantes sans ouvrante, l'effet est en général simplement nul : le navigateur ignore cette balise qui n'est 
    pas compréhensible. Toutefois, certains navigateurs, ou certaines balises, provoqueront des affichages totalement différents
    de ce qui est attendu et très souvent illisibles. C'est la responsabilité de l'auteur du document de veiller à respecter la syntaxe 
    du html.

??? tip "Astuce"

    &#128161; Il est conseillé, lorsqu'on écrit une balise ouvrante, d'instantanément écrire la balise fermante correspondante, et d'écrire ce que l'on souhaite entre les deux.

    😊 Pour s'y retrouver, il est recommandé d'utiliser des indentations.

!!! danger "Important"

    &#128073; Toutes ces balises indiquent **seulement** ce que contiennent les blocs. 
    Cela ne précise en aucun cas comment il faut les afficher dans la page. 
    Toutes les indications concernant **le style** seront
    fournies dans un fichier à part : la feuille de style **CSS**


### 2. Balises orphelines

Certaines balises ne servent pas à désigner un contenu, mais servent à ajouter un élément.

!!! example ""Exemples"

    * <b class="blue">&lt;br></b> indique un saut de ligne.  
    * La balise <b class="blue">&lt;hr></b> introduit une ligne horizontale dans toute la largeur de la page.  
    * <b class="blue">La balise &lt;img></b> qui sert à insérer une image est également une <b>orpheline</b>, mais 
    son utilisation est un peu différente et nous en reparlerons.

!!! example "Exemple à recopier"

    Lien pour visualiser : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>I. Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       <br> Il peut évidement y en avoir plusieurs.
                </p> 
                <hr>
                <h1>II. Titre de niveau 1 : </h1>
                    
        </body>
        </html>
        ```

😊 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." est bien écrite avec un saut de ligne grâce à la balise `<br>`


### 3. Quelques balises usuelles 

Aller sur ce site : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

#### a) Ecrire un nouveau document HTML complet. Dans le corps du document, c'est à dire entre les balises `<body>` et `</body>`, insérer le contenu suivant :

!!! abstract "Ma recette de cuisine"

    ```html linenums="1"
    <h1> La recette du carry de poulet </h1>

        <h2> Les ingrédients </h2>
            <ul>
                <li> un poulet découpé en morceaux </li>
                <li> 3 oignons </li>
                <li> 1 tomate </li>
                <li> 5 gousses d'ail </li>
            </ul>
        
        <h2> La préparation </h2>
            <p>Dans de l'huile chaude, faire revenir le poulet</p>
    ```

    ??? success "Solution"

        ```html 
        <!DOCTYPE html>
        <html>
            <head>
                <title>Titre de la page</title>
            </head>


            <body>
                <h1> La recette du carry de poulet </h1>

                <h2> Les ingrédients </h2>
                    <ul>
                        <li> un poulet découpé en morceaux </li>
                        <li> 3 oignons </li>
                        <li> 1 tomate </li>
                        <li> 5 gousses d'ail </li>
                    </ul>
        
                <h2> La préparation </h2>
                    <p>Dans de l'huile chaude, faire revenir le poulet</p>
            </body>
        </html>
        ```


#### b) Observer le résultat obtenu dans l'affichage et en déduire le rôle des balises suivantes :  

* `<ul>` et `</ul>`
* `<li>` et `</li>`

??? success "Solution"

    * `<ul>` représente une liste d'éléments non numérotés. Elle est souvent représenté par une liste à puces.
    * `<li>` est utilisé pour représenter un élément dans une liste. Celui-ci doit être contenu dans un élément parent : une liste ordonnée (`<ol>`), une liste non ordonnée (`<ul>`) ou un menu (`<menu>`). 

#### c) Faites les modifications suivantes :

* Ajouter un sous-titre 'Accompagnements' (titre de niveau 2) dans la page Web.
* Dans le sous-titre créer une liste à puces avec deux éléments : "riz blanc et grains", "riz jaune".
* Ajouter un paragraphe au début de la recette dans lequel on écrira "Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion"

??? success "Solution"

    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>

            <h1> La recette du carry de poulet</h1>

                <p>
                    Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion
                </p>

            <h2> Les ingrédients </h2>
                <ul>
                    <li> un poulet découpé en morceaux </li>
                    <li> 3 oignons </li>
                    <li> 1 tomate </li>
                    <li> 5 gousses d'ail </li>
                </ul>

            <h2> La préparation </h2>
                <p> Dans de l'huile chaude, faire revenir le poulet</p>

            <h2> Accompagnements</h2>
                <ul>
                    <li> riz blanc et grains </li>
                    <li> riz jaune </li>
                </ul>
        </body>
    </html>


    ```

* Ajouter le paragraphe suivant : 

??? success "A ajouter"

    ```html
    <p>
    <a href="https://fr.wikipedia.org/wiki/La_R%C3%A9union">île de la Réunion</a>
    </p>
    ```

Que s'est-il passé ?

??? success "Solution"

    Si l'on clique sur "île de la Réunion", on ouvre le lien donné sur l'île de la réunion.

* Ajouter un lien dans votre page Web sur les mots "3 oignons" qui permet d'accéder à l'adresse :  
 `https://fr.wikipedia.org/wiki/Oignon`

??? success "Solution"

    ```html
    <li> <a href=" https://fr.wikipedia.org/wiki/Oignon">3 oignons</a> </li>
    ```

!!! Important
    Les balises HTML permettent de **structurer** le contenu d'une page web, en définissant les titres, les paragraphes, ...

    &#127797; Pour modifier l'**apparence** d'une page, on a recours au **css** (**c**ascadind **s**tyle **s**heet) qui permet de modifier l'apparence du contenu de la page.
    
    😊 Nous verrons cela dans la leçon suivante.

## V. Insérer un lien

Nous avons vu un premier exemple avec la recette de cuisine.

!!! info "Insérer un lien"

    Voyons deux types de liens :

    * Les liens vers une pages externe :  
    Par exemple :  
    `<a href="https://fr.wikipedia.org/wiki/Anatomie_des_l%C3%A9pidopt%C3%A8res"> anatomie des lépidoptères.</a>`.

    * Les liens vers une autre page du même site :  
    Par exemple  :  
    `<a href="page2.html">Les lépidoptères</a>`  
    Dans ce cas le fichier peut être indiqué par un chemin relatif ou absolu. Nous verrons un exemple dans le paragraphe VII.


## VI. Insérer une image


!!! info "Insérer une image"

    La syntaxe pour insérer une image est la suivante :

    `<img src="source de l'image" alt="descritption de l'image">`

    👉 Les deux attributs src et alt sont obligatoires dans la norme HTML5.

!!! info "Source de l'image"

    Le source de l'image peut être :

    * Un nom de fichier : papillon.jpg. Dans ce cas le fichier contenant l'image doit se trouver dans le même répertoire que le fichier html
    * Un nom de fichier avec chemin relatif : ../images/papillon.jpg.
    * Un nom de fichier avec chemin absolu : /images/papillon.jpg. Dans ce cas, le fichier contenant l'image doit se trouver à l'endroit spécifié en partant de la racine du site.
    * Une adresse web : http://data.ba-bamail.com/Images/2014/9/10/934e8182-ea3f-4b3d-b1bf-dbda4fba9c3c.jpg

    👉 La première image est un fichier image, enregistré dans le même dossier que la page html. La seconde image est une image située sur le web, en l’occurrence, dans une page de wikipédia.

    👉 Notez bien qu'à ce stade on ne s'intéresse qu'au contenu. La façon dont l'image doit être présentée sera traitée ailleurs (centrée ou non, dans le texte ou séparée du texte, taille de l'image, etc..). Toutefois, si on veux des retours à la ligne avant et après l'image, on pourra placer la balise <img..> dans une paragraphe, comme ceci :

    ```html
    <p>
        <img src="image_locale.jpg" alt="une chenille">
    </p>
    ```

???+ note dépliée "Les chemins"

    Si l'image n'est pas dans le même dossier que le fichier html, il faudra utiliser un "chemin".

    👉 Voir le paragraphe "compléments"

## VII. Ecrire et visualiser une page écrite en HTML

!!! info "Editeur de code HTML"

    Il existe de nombreux éditeurs de code HTML, comme Notepad++, ou Sublime Text. Ils vous aideront à ne pas faire de faute de syntaxe.

???+ question "Créer un fichier HTML"

    Ouvrir l'éditeur dont vous disposez, copiez-collez le code ci-dessous, puis enregistrez votre fichier sous le nom : `essai_HTML.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Apprendre</title>
        </head>


        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```



!!! info "Visualiser du code HTML"

    * Pour le moment, nous avons utilisé le site en ligne `https://jsfiddle.net/`, très pratique pour de petits essais.  
    * 👉 A partir de maintenant nous allons tout simplement ouvrir un fichier HTML avec un navigateur.


???+ question "Visualiser un fichier HTML"

    * Utiliser votre explorateur de fichier, pour retrouver votre fichier.
    * Faire un clic droit sur votre fichier, puis "ouvrir avec", puis sélectionner "Firefox", ou "Google Chrome", ou "Microsoft Edge" par exemple.
    * Faire de même avec "Internet explorer" s'il est encore installé sur votre ordinateur.

    Que se passe-t-il ?

    ??? success "Vous pouvez obtenir quelque chose qui ressemble à ceci :"

        * Avec un navigateur "récent" : 

        ![avec Firefox](images/firefox.png){ width=35% .center}

        * Avec Internet Explorer : 

        ![avec Internet Explorer](images/intern_explor.png){ width=35% .center}


!!! info "Importance du `<head>`"

    😥 Comme vous le voyez, les navigateurs n'affichent pas toujours la même chose. C'est pourtant le même ficher, mais chaque navigateur peut interpréter les choses à sa façon.
    
    Ici c'est un problème d'encodage des accents qui provoque cette différence, et pour assurer un affichage correct dans tous les navigateurs, il faut ajouter une information dans le head.

    👉 Nous ajouterons dorénavant toujours dans `<head>` cette ligne : `<meta charset="UTF-8">`.

    😊 Ceci nous évitera le problème des accents, nous indiquons au navigateur que le texte est codé dans la norme utf-8 qui permet d'intégrer des accents dans le texte.


???+ question "Visualiser un fichier HTML avec le `head` complété"

    Recommencer l'expérience précédante avec le code suivant : 

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Apprendre</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```


## VIII. Un site à plusieurs pages

Nous allons voir comment cela fonctionne avec un site composé d'une page d'accueil et de deux autres pages.

!!! info "index"

    Il faut commencer par créer la page **obligatoirement** nommée `index.html`

    Voici un exemple plus détaillé que ce que nous avons déjà étudié.  
    Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `index.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <meta charset="utf-8">
                <title> Home </title>
            </head>
            <body>
                <header>
                    <h1> Mon en-tête : Accueil </h1>
                    <nav> 
                        <a href="page1.html"> page 1</a>
                        <br>
                        <a href="page2.html"> page 2</a>
                    </nav>
                </header>
                <main>
                    <section>
                    <h2> Section 1 </h2>
                    <p> Bla bla bla </p>
                    </section>

                    <section>
                    <h2> Section 2 </h2>
                    <p> Blo blo blo </p>
                    </section>
                </main>
                <footer> 
                    Mon pied de page
                </footer>
            </body>
        </html>
        ```


!!! info "Page 1"

    Vous devez maintenant écrire votre page 1.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page1.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 1</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 1</p>       
        </body>
        </html>
        ```


!!! info "Page 2 "

    Vous devez maintenant écrire votre page 2.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page2.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 2</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 2</p>       
        </body>
        </html>
        ```

!!! info "Visualiser votre tout petit site"

    Les trois fichiers doivent se trouver dans le même dossier.  
    Ouvrir le fichier `index.html` avec votre navigateur.



## IX. Le fichier CSS : principe général

!!! info

	Un document **html** est presque toujours accompagné d'au moins un fichier **CSS** qui décrit la mise en page,
	les formats (polices, couleurs, styles des éléments...). Un même fichier **CSS** peut être utilisé dans plusieurs pages d'un
	même site, ce qui permet, en le modifiant, de changer la présentation de toutes les pages (sinon il faudrait les modifier
	une à une). Mais ceci n'est qu'un aspect de l'intérêt des feuilles de styles. 

	👉 Essentiellement, elles ont pour rôle de permettre
	de séparer le contenu (qui est dans le html) de la forme (qui est dans le css)
	


### Sélecteur / Propriété / Valeur


Le CSS permet d'appliquer des styles sur différents éléments sélectionnés dans un document HTML. Par exemple, 
on peut sélectionner tous les éléments d'une page HTML qui sont paragraphes et afficher leurs textes en rouge avec 
ce code CSS :

!!! example "Exemple"

	```html
	p {
  		color: red;
	}
	```

!!! info

	Il y a 3 éléments dans la syntaxe :

	* **`p`** est le **sélecteur** : indique à quels éléments doit on applique le style. Ici, ce seront tout les éléments **`p`**, donc tous les paragraphes.
	* **`color`**est la **propriété** : indique (pour les éléments sélectionnés), quelle propriété on veut modifier.
	* **`red`** est la **valeur** qu'on attribue à la propriété pour les éléments sélectionnés.


### Où mettre le code CSS ?

!!! abstract "En bref"

    Il est possible de l'inclure dans la page html mais il est recommandé de mettre le code CSS dans un autre fichier,
    ou même parfois dans plusieurs fichiers, chacun contenant des instructions de formatage spécifiques. 


### Comment inclure le CSS dans la page ?

!!! info "`<link>`"

	  Etant donné qu'on place les instructions de style dans un fichier annexe, il est nécessaire de l'indiquer dans la page html.  
	  On utilise pour cela l'instruction `<link>` entre `<head>` et `<\head>`

!!! abstract "Un exemple"

    ```html 
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="mon_styles.css" type="text/css">
        <title>Titre de l'onglet</title>
    </head>

    <body>

    </body>
    </html>
    ```


???+ note "Précisions"

    * **`rel=`** définit le type de relation, ici stylesheet indique un lien vers une feuille de style.
	* **`href=`** donne le nom du fichier, qui peut être un chemin absolu, relatif, ou une adresse web.
	* **`type=`** définit le type de contenu, pour les feuilles de styles, c'est text/css.</li>


## X. Quelques propriétés fréquentes

Nous allons donner ici quelques syntaxes usuelles.
Il en existe beaucoup d'autres.  
Vous pourrez approfondir plus tard en suivant ce lien (on peut choisir comme langue le français) : [w3 schools](https://www.w3schools.com/css/default.asp){ .md-button target="_blank" rel="noopener" }

### 1. Pour les textes

!!! info "color : couleur du texte "

    Pour mettre les titres de niveau 1 en rouge

    ![titre rouge](images/titre_rouge.png){ width=100% .center}


!!! info "font-family : pour utiliser différentes polices "

    Pour mettre les paragraphes en police Monotype Corsiva .

    ![police](images/police.png){ width=100% .center}


!!! info "font-size : pour modifier la taille du texte"

    La taille en `1em` est la taille normale utilisée sur le client. `2em` sera **2 fois** plus grand.

    ![taille caractères](images/taille_font.png){ width=100% .center}


!!! info "font-style : pour mettre par exemple en italique"

    La valeur peut être : normal, italic ou oblique

    ![font style](images/font_style.png){ width=100% .center}


!!! info "text-align : pour centrer/justifier à gauche ou droite"

    La valeur peut être : center, left ou right

    ![alignement](images/alignement.png){ width=100% .center}


### 2. Pour la la page

!!! info "background : couleur du fond"

    Pour mettre un fond gris :

    ![fond](images/fond.png){ width=100% .center}


### 3. Pour une image

!!! info "width : largeur de l'image (en % ou en px)"

    Dans l'exemple suvant l'image occupe 10 % de la largeur de la page. Elle est donc très petite !
    
    ![taille image](images/taille.png){ width=100% .center}


### 4. Pour un lien ou un menu

!!! abstract "Un exemple"

    Considérons le code html suivant :

    ```html
    <nav>
      <ul>
      <li><a href="accueil.html">Accueil<a></li>
      <li><a href="Projets.html">Projets<a></li>
      <li><a href="quisommesnous.html">qui sommes nous</a></li>
      </ul>
    </nav>
    ```

L'apparence sans CSS est la suivante : 

![menu sans css](images/menu_sans_css.png){ width=15% .center}

!!! abstract "Ajoutons ceci dans notre fichier css :"

    ```html
    nav {
      width: 200px;
      list-style: none;
      margin: 0;
      padding: 0;
    }
    nav li {
      background: #c00 ;
      color: #fff ;
      border: 1px solid #600 ;
      margin-bottom: 1px ;
    }
    nav li a {
      display: block ;
      background: #c00 ;
      color: #fff ;
      font: 1em "Trebuchet MS",Arial,sans-serif ;
      line-height: 1em ;
      text-align: center ;
      text-decoration: none ;
      padding: 4px 0 ;
    }
    ```

L'apparence **avec** CSS est la suivante : 

![menu avec css](images/avec_css.png){ width=30% .center}

## XI. Exercice

???+ question "Modifier le style"

    Enregistrer le fichier suivant : `Le rougail de saucisses` : ["Clic droit", puis "Enregistrer la cible du lien sous"](telechargement/rougailsaucisses.html)

    1. Créer une feuille de style dans un fichier séparé, modifier l'apparence de cette page web de sorte que:

        * Le fond de la page soit de couleur bleu ciel (lightblue).
        * Les caractères de la page en bleu
        * Les titres de niveau 1 soient en vert.
        * Les titres de niveau 2 soient en violet.

        ??? success "Le fichier mon_style.css"

            ```html
            body {
	            background : lightblue;
	            color : blue;
            }

            h1 {
	            color : green;
            }

            h2 {
	            color : purple;
            }
            ```

    2. Créer le lien dans le fichier html vers la feuille de style.

        ??? success "Le lien"

            Début du fichier : 

            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8">
                <link rel="stylesheet" href="mon_style.css" type="text/css">
                <title>A la gloire du rougail saucisses</title>
            </head>
            ```

    3. Vérifiez le rendu avec votre navigateur.



???+ question "2. Réaliser un mini-site"

    Réaliser  un mini site *Web*, en utilisant des fichiers html et **une** feuille de style CSS. Le sujet du site est au choix, par exemple : vos films préférés, un site de recette de cuisines, un site sur un sport ou une de vos passions, ou sur une célébrité (sportif, acteur, chanteur ...) . 
    
    👉 Respecter le cahier des charges suivant :

    * au moins 5 pages reliées entre elles par des liens internes.
    * au moins 2 images, attention à utiliser des images **libres de droits** ou à créer vos propres illustrations pour votre site.
    * Il y aura un lien vers un site extérieur.
    * L'apparence du site sera uniformisée (c'est à dire que d'une page à l'autre on retrouvera les mêmes couleurs et la même présentation). Vous devrez pour cela utiliser **obligatoirement** une feuille de style css dans un fichier séparé.


## XII. Les moteurs de recherche

### a. Introduction

Il existe actuellement environ deux milliards de sites web et des dizaines de milliards de pages web. Pour chercher une information dans cette toile d'araignée géante (toile = web en anglais) que constitue littéralement le Web, des applications Web (qui s'exécutent dans un navigateur) appelés moteurs de recherche sont nécessaires.

On lance un moteur de recherche à l'aide d'un navigateur web, dont les plus répandus sont les suivants :

* Mozilla Firefox, le plus respectueux de votre vie privée, il est open-source et libre,

* Google Chrome, le navigateur de Google présent par défaut sur Android et qui envoie de nombreuses informations personnelles aux serveurs de Google,

* Microsoft Edge (anciennement Internet Explorer) présent par défaut sur Windows© ; il collecte par défaut les données de vos recherches (source),

* Safari, le navigateur présent sur les appareils Apple,

* Opera, Brave, Vivaldi... moins connus que les précédents mais qui ont chacun leurs spécificités.

![morse](../images/Navs.png){ width=100%; : .center }

Les réponses à l'activité seront à compléter sur le document LibreOffice ci-joint :

Sujet élève pour l'activité 2 : Moteur de Recherche [Télécharger le TP](scripts/Moteurs_de_Recherche.odt)

### b. Fonctionnement des moteurs de recherche

[![texte alternatif de l'image](http://img.youtube.com/vi/iKMm6SXO0wA/0.jpg){: .center}](https://youtu.be/iKMm6SXO0wA "Titre de la video")

Après avoir visionné cette courte vidéo, répondre à la question suivante :

Question 1 : Compléter les 3 cases du tableau suivant qui décrit le fonctionnement général d'un moteur de recherche ? Ne pas oublier le terme anglais sur le ligne 1 du tableau.


![morse](images/tab1.png){ width=100%; : .center }

### c. Utiliser les moteurs de recherche dans le navigateur Chrome

On compte de nombreux moteurs de recherche disponibles et certains sont disponibles nativement dans les navigateurs comme Mozilla Firefox.

Ces manipulations sont à réaliser sur Chrome mais sont quasi identiques sur d'autres navigateurs web.

Voyons cela dans le détail.

Moteur de recherche par défaut
Ouvrir le menu d'options de Chrome en cliquant sur l'icône rayée en haut à droite de la barre de menu, sélectionner Paramètres dans le menu déroulant :

![morse](images/prefs.png){ width=50%; : .center }

Puis, sélectionner le menu Moteur de recherche dans le menu de gauche :

![morse](images/prefs1.png){ width=50%; : .center }

Changer le moteur de recherche par défaut en sélectionnant une des propositions de la liste déroulante (DuckDuckGo ou Qwant sont une bonne alternative). Tester le bon fonctionnement en utilisant le champ où l'on tape des adresses web ou URL.

Utiliser différents moteurs de recherche dans Chrome.

Validation enseignant
Préparez-vous à :

montrer la liste des moteurs de recherche de votre navigateur


### d. Requêtes et résultats dans les moteurs de recherche

Avec la domination de Google, des moteurs de recherche alternatifs essaient de se distinguer par le choix d'un modèle plus respectueux de la vie privée de ses utilisateurs. On peut citer l'américain DuckDuckGo et le français / européen Qwant.

Remarque
Premier lancement des moteurs de recherche Google et Qwant
Au premier lancement de Google, vous devez dire si vous acceptez ou pas l'usage de cookies (nous verrons en détail ce que sont les cookies lors de l'activité suivante).


**Question 2 :**

Ouvrir une fenêtre de navigation privée (menu contextuel de Chrome haut à gauche) puis

Aller sur le moteur de recherche Google et réaliser une capture d'écran de cette fenêtre du premier lancement du moteur de recherche Google (la fenêtre comprend 2 boutons Tout refuser et Tout accepter).

Faites un choix :

Tout accepter autorise Google à analyser votre navigation que vous allez faire sur le domaine Google.com

Tout refuser n'autorise pas Google à analyser votre navigation, en vertu des règles européennes sur la protection des données personnelles.

**Question 3 :**

Toujours dans la fenêtre privée, ouvrir un nouvel onglet et lancer le moteur de recherche qwant.com : https://www.qwant.com/

Avez-vous du accepter ou refuser des cookies ?


Requête dans 2 moteurs

**Question 4 :**

Soumettre la requête « achat trotinette électrique » (la faute d'orthographe sur trotinette est volontaire, comparez la gestion de la correction automatique par les deux moteurs) aux moteurs de recherche Google et Qwant (en désactivant les éventuels bloqueurs de publicité, mais cela ne doit pas être le cas au lycée).

Obtenez-vous les mêmes résultats avec les deux moteurs ?

Quels sont les 2 premiers sites web hors annonces publicitaires.

**Question 5 :**

Dans les résultats du moteur de recherche Google, on remarque immédiatement une carte.

Que recense cette carte ?

Comment sait-il que vous vous situez dans ces environs ? Vous pourrez trouver la solution en fouillant le bas de la page...

Avons-nous la même chose avec Qwant ?

Financement des moteurs de recherche
Ce n'est un secret pour personne, financer un moteur de recherche coûte cher. Il faut donc trouver des sources de financement. La seule source de revenus actuellement est la publicité mais il y a de grandes différences entre les moteurs de recherches.

Prenons l'exemple de Google et Qwant.

Avec le moteur de recherche Google, repérez les résultats qui sont des annonces publicitaires (par exemple sur la requête achat trottinette électrique) et cliquer sur le petit triangle vers le bas▼situé à proximité de l'annonce.

Dans la fenêtre modale qui s'affiche, développer le menu...

**Question 6 :**

D'après la notice d'informations qui s'affiche, pourquoi voyez-vous cette annonce ? Est-ce que seuls les termes recherchés permettent à une annonce de s'afficher ?

**Question 7 :**

Il est possible de personnaliser ou pas les annonces affichées. Personne ne devrait accepter cette personnalisation qui fait de vous un objet publicitaire.

Toujours dans la fenêtre, en dessous, vous pouvez mettre à jour vos choix concernant les annonces de Google.

Quel est le paramétrage pour ces annonces ? À présent rappelez-vous sur quel bouton bleu vous avez cliqué au lancement de Google et expliquer ce paramétrage...

![morse](images/prefs2.png){ width=50%; : .center }

**Question 8 :**

Accéder à la page d'URL https://about.qwant.com/fr/legal/confidentialite/ puis au paragraphe « Donc Qwant n'affiche pas de publicité ciblée ? ».

Quelle est la politique de Qwant en matière d'affichage publicitaire ?

Politique de confidentialité

**Question 9 :**

À l'aide des manipulations précédentes et en parcourant les politiques de confidentialité de Google et de Qwant, cochez, sur votre document LibreOffice, le moteur qui répond aux phrases suivantes :

![morse](images/prefs3.png){ width=50%; : .center }


### e. Quizz sur les moteurs de recherche...

**Question 10**
Il existe de nombreux moteurs de recherche sur le Web, mais connaissez-vous les principaux ?

Cocher les moteurs de recherche existants (et pas les méta-moteurs). Refaites jusqu'à trouver la liste complète et notez-le sur votre document réponses.

Vous avez bien sûr le droit de vous aider de Wikipédia (pour information, dans cette liste, il y a des moteurs de recherche et des navigateurs web).

**Question 11 :** Origine de 2 gros moteurs de recherche méconnus
Yandex est un moteur de recherche d'origine 
 alors que Baidu est un moteur de recherche 
.

**Question 12 :** À propos de Lilo ou Écosia...
Concernant les méta-moteurs "écolos", Lilo ou Ecosia, consomment-ils plus d'énergie qu'un moteur classique (Google, Qwant...)


Oui, Non, Je ne sais pas...

### f. Bonus : et son compte Google ?

Si vous possédez un compte Google (ne pas créer de compte Google pour ces questions), connectez-vous pour constater qu'en mode connecté, la personnalisation va beaucoup plus loin :

Accédez à la page de paramétrage https://myaccount.google.com/data-and-personalization, activez l'Activité sur le Web et les applications, puis dans les paramètres de recherche sélectionnez les Résultats privés.

Testez alors une recherche Google du type le prénom et le nom d'un contact de votre téléphone, vous devriez voir apparaître des résultats de recherche lié à ce contact.

Accédez à la page de personnalisation des annonces https://adssettings.google.com/ , vous pourrez juger de la pertinence et filtrer les Facteurs de personnalisation de vos annonces :

![morse](images/prefs4.png){ width=50%; : .center }


### g. Pour aller plus loin... Histoire des moteurs de recherche

Historique des moteurs de recherche
A l'aide de ces paragraphes empruntés à ces 3 articles Wikipédia, répondre aux questions ci-dessous sur votre document LibreOffice joint.

**Yahoo !**


À l'origine, Yahoo! est un annuaire Web, le Jerry and David's Guide to the World Wide Web to Yahoo! (« Guide de David et de Jerry pour le World Wide Web ») créé en janvier 1994 par Yang et Filo, deux étudiants en ingénierie informatique à l'université Stanford qui passent leur temps à surfer sur le Web naissant, possédant ainsi une collection de sites intéressants qu'ils ont de plus en plus de mal à répertorier. Dans le cadre de leurs études, ils décident alors d'utiliser les infrastructures de l'université pour répertorier et catégoriser les autres sites web à travers un annuaire organisé par thèmes dans une logique d'index comme dans les encyclopédies. Ils facilitent également la recherche des utilisateurs en créant un moteur de recherche qui permet de rechercher des sites par mots-clés.

Le rétroacronyme « Yet Another Hierarchical Officious Oracle » créé en avril 1994 est souvent cité comme signification, mais ses créateurs Jerry Yang et David Filo, qui devaient choisir un nom de projet commençant par un « Y » pour s'inscrire dans la nomenclature des projets informatiques de l'université Stanford, affirment avoir choisi le nom à cause des « Yahoos », nom donné aux humains dans le dernier des Voyages de Gulliver de Jonathan Swift. En moins d'un an, le trafic sur Yahoo est tel qu'il sature le réseau de l'université et les autorités universitaires leur demandent de quitter le campus. Yang et Filo fondent alors leur propre entreprise le 2 mars 1995 et acceptent la proposition de Marc Andreessen (co-fondateur de Netscape Communications à Mountain View, en Californie) d'héberger leur site sur les serveurs de Netscape.

**AltaVista**



AltaVista ou Alta Vista (littéralement « vue haute » en espagnol) était un moteur de recherche du World Wide Web. Il fut mis en ligne à l'adresse web altavista.digital.com en décembre 1995 et développé par des chercheurs de Digital Equipment Corporation. Il fut le plus important moteur de recherche textuelle utilisé avant son rachat.

Bien qu'il y ait une polémique concernant l'auteur de l'idée originale, on s'accorde à dire que les deux principaux contributeurs ont été Louis Monier, qui a écrit le Robot d'indexation, et Michael Burrows, qui a écrit l'indexeur.

AltaVista a été le premier moteur de recherche capable d'indexer rapidement une bonne partie des pages web existantes et devint immédiatement très populaire. Il fut également le premier moteur de recherche multilingue (la version française fut ouverte le 15 février 2000), ainsi qu'à lancer la recherche d'images, de fichiers audio et de vidéos. Le site offrait aussi un service internet gratuit au public.

AltaVista fonctionnait, en 1998, sur 20 serveurs multiprocesseurs 64 bits Digital Alpha. Au total ces machines étaient dotées de 130 gigaoctets de mémoire vive, de 500 gigaoctets d'espace de disque, et répondaient à 13 millions de requêtes par jour.

**Google**


Les fondements de l'histoire de l'entreprise Google commencent par la rencontre de deux étudiants de l'université Stanford en 1995. Sergey Brin alors âgé de vingt-trois ans et Larry Page de vingt-quatre ans sont « pratiquement en désaccord sur tout ». Cela ne les empêche pourtant pas, en janvier 1996, de commencer à travailler sur un nouveau moteur de recherche.

Les fondateurs de Google, alors doctorants, observent la façon dont sont classés les résultats lorsqu'ils effectuent des recherches dans les bases de données scientifiques. Ils constatent que l'exploitation des informations contenues dans la structure hypertextuelle dépend de la nature des liens entre ces documents. Ainsi l'idée d'analyser les liens inter-documents reposait sur l'observation d'une caractéristique de la littérature scientifique et aux modes d'organisation du Sciences Citation Index (SCI), qui consiste à attribuer une valeur à une publication scientifique proportionnellement au nombre de publications qui la cite. Ce principe revient aux travaux de Jon Kleinberg qui avait mis au point pour IBM l'algorithme HITS (Hyperlink –Induced Topic Search) qui prenait en considération l'autorité d'une page en fonction du nombre de liens pointant vers elle. C'est donc en s'inspirant des travaux de Jon Kleinberg que les deux étudiants mettent au point l'algorithme de classement des pages web appelé Pagerank. Cet algorithme prend en considération les liens pointant vers une page comme un vote pour cette page. Plus une page recevrait de vote, plus elle serait considérée comme étant pertinente et plus le vote de cette page, lorsqu'elle pointerait elle-même vers d'autres pages, aurait de la valeur.

Ils nomment leur projet BackRub. Ils imaginent un logiciel qui analyserait les relations entre les sites web afin d'offrir des meilleurs résultats que ceux donnés par leurs concurrents de l'époque, AltaVista notamment.

Une fois leurs travaux terminés, les deux étudiants commencent à concrétiser leur projet de moteur de recherche et achètent à cet effet un téraoctet de disques durs d'occasion, afin de créer une base de données. Cette installation sommaire a pris place dans la chambre de Larry Page. Sergueï loue un bureau et se met en quête de financements. David Filo, fondateur de Yahoo!, convient de l'intérêt de leurs recherches, mais les encourage à créer leur propre moteur de recherche plutôt que vendre leurs travaux.

Question : Quels sont les 2 modèles de recherche d'information dans le Web qui sont apparus à la fin des années 90 ?

Question : Comment AltaVista puis Google ont-ils pu imposer leur moteur de recherche ?

**LE PAGERANK :**

[Lien vers le TP PAGERANK](https://capytale2.ac-paris.fr/web/c/4970-2255257)

Correction possible du programme Python :

???+ question "Tester le programme" 

    {{ IDE('Pagerank') }}        

