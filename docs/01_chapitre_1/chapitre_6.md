---
author: Votre nom
title: Informatique embarquée et objets connectés
---

[![texte alternatif de l'image](http://img.youtube.com/vi/DOECi_ZKaYI/0.jpg){: .center}](https://youtu.be/DOECi_ZKaYI "Titre de la video")



Repères historiques :  

-1967 : premier système embarqué de guidage lors de la mission lunaire Apollo ;  
-1971 : premier processeur produit par Intel ;  
-1984 : sortie de l’Airbus 320, premier avion équipé de commandes électriques
informatisées ;  
-1998 : mise en service du métro informatisé sans conducteur Météor (ligne 14 à
Paris) ;  
-1999 : introduction de l’expression « internet des objets » par Kevin Ashton ;  
-2007 : arrivée du smartphone.  
On estime à 50 milliards le nombre d’objets connectés en 2020.  


## I. Créez votre propre application pour smartphone :

Dans ce didacticiel, vous allez découvrir comment faire une application avec App Inventor :   
Faire rebondir une balle (un sprite) à l'écran.

### 1. Ouvrir la page https://appinventor.mit.edu/

![morse](./images/appinventor.png){ width=100%; : .center } 

### 2. Démarrer une app  en cliquant sur « Create Apps » et basculer en langue « française »

Il faut se connecter avec un compte google.

### 3. Démarrer un nouveau projet :

![morse](./images/newprojet.png){ width=100%; : .center } 

Donner un nom au projet :

![morse](./images/nomprojet.png){ width=50%; : .center } 

Ajouter un cadre :

![morse](./images/cadre.png){ width=100%; : .center } 

Réglez l'écran pour qu'il ne défile pas :

![morse](./images/reglageecran.png){  : .center } 

Modifiez la hauteur et la largeur du cadre pour remplir l’écran :

![morse](./images/hauteurecran.png){ width=50%; : .center } 


Ajouter une balle :

![morse](./images/ajoutballe.png){ width=100%; : .center } 


Enfin ouvrez l’éditeur de blocs : (comme dans Scratch!)

![morse](./images/editeurblocs.png){ width=50%; : .center } 

Cliquer sur la Balle pour ouvrir les blocs disponibles.

![morse](./images/blocs.png){ width=50%; : .center } 




Choisir le bloc balle.Lancer

![morse](./images/bloclancer.png){ width=50%; : .center } 

puis ces deux blocs

![morse](./images/blocslancer.png){ width=50%; : .center } 

Insérer les deux blocs comme ci-dessous :

![morse](./images/blocsinserer.png){ width=50%; : .center } 

Il faut maintenant capturer la variable « vitesse » et « orientation » :

![morse](./images/blocsvitesse.png){ width=50%; : .center } 

déplacer la souris sur « vitesse » deux blocs s’affichent, prendre « obtenir vitesse »
et le placer au bon endroit. Faire de même pour orientation.

![morse](./images/blocsorientation.png){ width=50%; : .center } 

Nous allons tester cette application avec notre smartphone :

1ere Etape : installer MIT AI2 Companion sur votre smartphone 

![morse](./images/appsmartphone.png){ width=50%; : .center } 

2eme Etape : se connecter avec AI2 Companion 

![morse](./images/compagnonAI.png){ width=50%; : .center } 

![morse](./images/codebarre.png){ width=50%; : .center }   

![morse](./images/scancode.png){ width=50%; : .center } 

![morse](./images/ballesmartphone.png){ width=50%; : .center } 

Si cela fonctionne vous pouvez remarquer que la balle ne rebondit pas sur l’écran, nous devons maintenant le programmer. Rajouter ces blocs dans le programme.

![morse](./images/ballerebond.png){ width=50%; : .center } 

A vous de jouer et d’améliorer cette application, on peut aussi installer l’application sur le smartphone en construisant un APK.

## II Activité : Réalisation d'une IHM simple d'un objet connecté

### Partie 1 - Utiliser un service de messagerie dédié à l’internet des objets

Le protocole MQTT permet à divers objets de publier des informations (par exemple une
sonde de température peut publier des données). Certains objets peuvent être abonnés à ces
publications et donc recevoir les données. Les objets communiquent avec un serveur nommé
« Broker ». 

Ils peuvent être :  
* « publisher » c’est-à-dire qu’ils peuvent publier des données sur des sujets ;  
* « subscriber » c’est-à-dire abonnés à des sujets.


![morse](https://i.ibb.co/C9Ngps6/broker.png){ width=50%; : .center }



### Partie 2

Pour différencier les différents services, les objets publient leurs résultats dans des « topics ».
Les topics s’écrivent en utilisant un format permettant plusieurs niveaux, chaque niveau est
séparé par un slash « / ».  

Par exemple :  
* smarthome/chambre/temperature  
* smarthome/chambre/light

![morse](https://i.ibb.co/2tJzrYw/broker1.png){ width=50%; : .center }


### Partie 3 - Création d’un IHM entre 2 téléphones utilisant le protocole MQTT

Pour celui qui publie  

1. Installation de l’application « IoT MQTT » sur son smartphone.  

![morse](https://i.ibb.co/S73pGvF/imageIoT.png){ width=50%; : .center } 

2. Réglage sur le smartphone et création d’une connexion à un serveur pour Élève 1.

<div>
<table border="0">
  <tr>
    <td><img src="https://i.ibb.co/sF7mKhF/Iot1a.jpg" width=50% alt="Iot1a" border="0" /></td>
    <td><img src="https://i.ibb.co/yfzsx6s/Iot1b.jpg" width=50% alt="Iot1b" border="0" /></td>
 
  </tr>
</table>
</div>
<td><img src="https://i.ibb.co/p0Gpg4f/Iot1c.jpg" width=50% alt="Iot1c" border="0" /></td>


 


3.  Cliquer sur votre connexion et ajouter un panel comme un curseur (slider)

<div>
<table border="0" >
  <tr>
    <td><img src="https://i.ibb.co/F6Dkmft/Iot1f.jpg" width=50% alt="Iot1f" border="0" /></td>
    <td><img src="https://i.ibb.co/wwkTSg7/Iot1d.jpg"  width=50% alt="Iot1d" border="0" /></td>
  
  </tr>
</table>
</div>
<td><img src="https://i.ibb.co/ctyqXKx/Iot1e.jpg" width=50% alt="Iot1e" border="0" /></td>


Régler le panel avec notamment le topic : monLycee/maClasse/Eleve1


On peut modifier la valeur du curseur.   

Pour celui qui souscrit  
Les étapes 1 et 2 précédantes sont communes.  

4. Création d’une connexion à un serveur pour Élève 2.  

<div>
<table border="0" >
  <tr>
    <td><img src="https://i.ibb.co/NLmZB0r/Iot1g.jpg" width=50% alt="Iot1g" border="0"></td>
    <td><img src="https://i.ibb.co/5swGRQ4/Iot1h.jpg" width=50% alt="Iot1h" border="0"></td>
  </tr>
</table>
</div>





5. Cliquer sur votre connexion et ajouter un panel comme une jauge (gauge)

<img src="https://i.ibb.co/WW312tD/iot1.jpg" width=30% alt="iot1" border="0">

Régler le panel avec notamment le topic : monLycee/maClasse/Eleve1

<img src="https://i.ibb.co/806Fnj6/Iot1j.jpg" width=30% alt="Iot1j" border="0">

Connexion entre Élève 1 et Élève 2.

Lorsque Élève 1 modifie la valeur de son curseur, Élève 2 voit sa jauge se modifier
automatiquement.  
Comme sous l'image ci-dessous :

<img src="https://i.ibb.co/sJHxn1B/Iot1k.jpg" width=50% alt="Iot1k" border="0">

### Partie 4  

Enrichir l’application.  
• Élève 1 : rajouter un panel avec un « switch » qui publie sur le topic au format monLycee/
maClasse/nomEleve1/switch.    
• Écrire votre topic :  
le paramètre « Payload on » sera réglé avec on et le paramètre « Payload off » avec off.  
• Élève 2 : rajouter un panel avec une « led » qui est abonnée au topic écrit précédemment.  
Le paramètre « Payload on » sera réglé avec on et le paramètre « Payload off » avec off.  
La couleur de la « led on » sera réglée sur #FF0000, la couleur de la « led off » sera réglée
sur #00FF00.  
• Vérifier que la commutation du switch fait changer la couleur de la led sur l’autre
smartphone.  

