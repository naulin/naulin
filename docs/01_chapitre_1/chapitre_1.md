---
author: Naulin Jean-Marc
title: Internet
---

[Lien du site à télécharger pour la classe](scripts/NSI.url)

## I Histoire d'Internet

![morse](../images/Frise.png){ width=100%; : .center }

### Objectif

L'objectif est de comprendre l'évolution d'Internet au fil du temps.

### Matériel

- Accès à Internet
- Feuilles de papier et stylos  



### Étapes

### Étape 1 : Introduction à Internet    
1. Commencez par une discussion en classe sur ce que les élèves savent déjà sur Internet. Quelles utilisations en font-ils au quotidien ?
2. Expliquez brièvement ce qu'est Internet, son rôle dans la société et comment il a évolué au fil du temps.

### Étape 2 : Recherche et Collecte d'Informations (60 minutes ou +)

Recherche d'informations notamment sur le site ecotechno.fr (vidéos, documents)
 


## II TP TCP/IP

Vidéo à regarder pour ce TP :

[![texte alternatif de l'image](http://img.youtube.com/vi/s18KtOLpCg4/0.jpg){: .center}](https://youtu.be/s18KtOLpCg4 "Titre de la video")


[Télécharger le TP](scripts/TP.pdf)

Ce TP est à rendre à la fin de l'heure sous format numérique sur le lien indiqué ci-dessous.

Liens pour déposer le travail:

<div class="grid cards" markdown>

-   :material-clock-fast: __Seconde3 dépôt__ [lien dépôt](https://nuage04.apps.education.fr/index.php/s/CpnsKSEwXcXd87k).

-   :material-clock-fast: __Seconde2 dépôt__ [lien dépôt](https://nuage04.apps.education.fr/index.php/s/pBwP2bRrpseX2Eo).

-   :material-clock-fast: __Seconde7 dépôt__ [lien dépôt](https://nuage04.apps.education.fr/index.php/s/woS4iQLsSXfKijb).

</div>

## III TP Réseaux (avec filius)

[Télécharger le TP](https://nuage04.apps.education.fr/index.php/s/Y85Bg8HgmtW8Dsy)
[Reseau-de-Reseaux](https://nuage04.apps.education.fr/index.php/s/QQnj6YZzMBwcMqT)

Suivez les consignes et explications ci-dessous afin de réussir le TP sur Filius qui vous aidera à mieux
comprendre comment est construit un réseau d’ordinateurs et comment dialoguent entre eux des ordinateurs
d’un même réseau.  
En cas de difficultés, n’hésitez pas à appeler votre enseignant.  

**Pour commencer :**  
Lancer le logiciel Filius. (dans le répertoire SNTNSI du bureau)
Filius permet de construire "virtuellement" des réseaux.

Normalement, en suivant le TP, vous devez pouvoir construire et utiliser votre réseau. 

**Convention :**  
Pour les clients, vous prendrez des Portables et pour les serveurs vous prendrez des Ordinateurs.

**Pour voir la configuration d'un poste :**  
Double-cliquer ou effectuer un clic droit sur l'objet puis cliquer sur configure.

### 1. Un réseau (très simple) avec 2 machines et un switch.

Créer 2 ordinateurs (portable ou de bureau, peu importe) nommés M1 et M2, un Switch et relier le tout par un câble.  
Faire un double-clic sur chaque poste pour le renommer et paramétrer son adresse IP.  
Pour M1, donner l'adresse IP 192.168.1.1 et pour M2 l'adresse IP 192.168.1.2  
Laisser le masque sous-réseau à 255.255.255.0.  

![morse](images/image1.png){ width=40%; : .center }

Passer en mode simulation (cliquer sur le triangle vert) pour vérifier que les 2 machines peuvent communiquer.

![morse](images/image2.png){ width=40%; : .center }

Cliquer sur M1 : une nouvelle fenêtre s'ouvre avec une icône qui permet d'installer des logiciels dans les machines :  

![morse](images/image3.png){ width=40%; : .center }

Installer alors "ligne de commande" (le 1er choix) en double-cliquant puis en cliquant sur "Appliquer les
modifications". On a alors accès à une console pour la machine M1 :  

![morse](images/image4.png){ width=40%; : .center }

Lancer cette console, vous pouvez voir afficher les différentes commandes acceptées par cette console.  



On va maintenant revoir quelques commandes réseau (à connaître).  
ipconfig : elle permet de connaître l'adresse IP de la machine.  
Remarque : Filius utilise la syntaxe Windows qui n'est pas la même que la syntaxe MacOs/Linux, sous Linux et
MacOs, il faut la remplacer par ifconfig (ou aussi "ip addr show" sous Linux).  
Vous pouvez constater que c'est bien l'IP que vous avez choisie qui est affichée.  

![morse](images/image5.png){ width=40%; : .center }

ping : permet de savoir si une machine est joignable dans un réseau en envoyant des paquets vers celleci (4 par défaut). ping affiche aussi le temps mis pour recevoir une (éventuelle) réponse et indique qu'un paquet est perdu en cas de non-réponse.  

Faire un ping vers la machine M2 en tapant : ping 192.169.1.2 (qui est l'adresse IP de M2).  

![morse](images/image6.png){ width=40%; : .center }

On constate qu'aucun paquet n'a été perdu, la connexion fonctionne bien.  
Remarques :  
* Si vous regardez bien vous devez voir un changement de couleur du câble réseau lors des échanges de
paquets.
* On peut aussi faire un clic-droit sur les machines et "afficher les échanges de données" pour vérifier que les
échanges de paquets ont bien eu lieu.

### 2. Un réseau de 4 machines.
Repasser en mode conception (icône marteau).


a) Créer un réseau avec 4 machines M1, M2, M3 et M4.  
M1 → IP : 192.168.1.1  
M2 → IP : 192.168.1.2  
M3 → IP : 192.168.1.3  
M4 → IP : 192.168.2.1  
Laisser le masque sous réseau à 255.255.255.0.  


b) Passer en mode simulation.  
Sur la machine M1, que faut-il saisir dans la console pour vérifier qu'il peut joindre M3 ?  
Faire un ping sur la machine M4, quel est le résultat de ce ping ? À votre avis, pourquoi ?  

!!! warning "À retenir :"
    Une adresse IP est constituée de deux parties : l'adresse du réseau et l'adresse de la machine qui permet donc
    de distinguer une machine sur un réseau. Deux machines se trouvant sur un même réseau possèdent la même
    adresse réseau mais pas la même adresse machine.

Dans notre cas :  
Le masque sous réseau 255.255.255.0 indique que les trois premiers octets sont utilisés pour adresser le réseau et le
dernier octet pour la machine.  
Les machines M1, M2 et M3 sont dans le réseau dont l'adresse est : 192.168.1.0  
M4 est dans le réseau dont l'adresse est : 192.168.2.0  

### 3. Un internet : deux réseaux de 3 machines.
Nous allons maintenant construire 2 réseaux de 3 machines.

Le réseau des machines M1, M2, M3 a pour adresse 192.168.1.0. Compléter les IP de chaque machine de ce réseau.  
Le réseau des machines M4, M5, M6 a pour adresse 192.168.2.0. Compléter les IP de chaque machine de ce réseau.  
Pour relier les 2 réseaux, il faut ajouter un routeur (avec au moins 2 interfaces réseaux puisqu'il y a 2 réseaux).  

![morse](images/image7.png){ width=70%; : .center }

Il faut maintenant paramétrer 2 adresses pour le routeur puisqu'une adresse doit correspondre au premier
réseau et l'autre au 2ème réseau.

![morse](images/image8.png){ width=70%; : .center }

Dans l'onglet Général, cocher Routage automatique pour que le routeur gère automatiquement les tables de
routage

![morse](images/image10.png){ width=70%; : .center }

Il faut maintenant ajouter l'adresse IP du routeur dans le champ Passerelle (Gateway en anglais) de chaque
machine de chaque réseau.

![morse](images/image9.png){ width=70%; : .center }


Quand un ordinateur tente de communiquer avec un autre périphérique via le protocole TCP/IP, il effectue une
comparaison entre le masque sous-réseau et l'adresse IP de destination d'une part et le masque sous-réseau et sa
propre adresse IP d'autre part ; le résultat de cette comparaison indique à l'ordinateur si la machine de destination
appartient au même réseau ou pas.  
Si le résultat de ce test détermine qu'il s'agit d'une machine du réseau local alors l'ordinateur envoie le paquet
sur le sous-réseau local en transitant par le switch.  
Si le résultat indique qu'il s'agit d'une machine qui n'est pas dans le même réseau alors l'ordinateur transmet
le paquet à la passerelle (le routeur) définie dans ses propriétés TCP/IP.  
Le routeur est alors chargé de transmettre le paquet au sous-réseau correct à l'aide des tables de routage.  

Une fois que tout est bien complété, passez en mode simulation.  
• Installer une console sur les machines M1, M3 et M5.  
• Depuis M1, faites un ping sur M3, puis sur M5.  
• Depuis M5 faites un ping sur M4 puis sur M1.  
Si tout a bien été configuré, vous devez constater que les machines des 2 réseaux peuvent communiquer entre-elles
aussi bien dans le réseau local qu'entre les 2 réseaux. Vous avez correctement configuré un internet !  
Utiliser la commande traceroute pour obtenir le chemin parcouru pour arriver jusqu’à une machine (définie par son
IP ou son url).  

![morse](images/image11.png){ width=40%; : .center }


On constate que pour aller de la machine M1 à la machine M5, il a fallu passer par le routeur identifié (pour M1)
par 192.168.1.254.  


Ce sont les 3 commandes utilisées précédement sur votre propre ordinateur (donc avec un vrai
réseau).  
Pour cela :  
• ouvrir une console (cmd)  
• taper les commandes dans sa console. Pour ping et traceroute, vous pouvez les tester avec différentes IP
(comme 194.199.224.42 ou 216.58.204.99, par exemple).  
Il est possible que parfois traceroute ou ping n'aboutissent pas alors qu'une url ou une ip sont valides, dans ce cas
c'est qu'un administrateur du réseau a bloqué la possibilité de faire un ping sur les machines du réseau.  

### 4. Un serveur DHCP.

Dans les grands réseaux, il est pratique de configurer un serveur DHCP qui assigne automatiquement les adresses
IP à tous les ordinateurs (comme ce qui est fait au lycée ou généralement chez vous avec votre box Internet).
Nous allons voir comment ajouter et configurer un serveur DHCP dans un réseau.  

Exemple :  
On commence par ajouter une nouvelle machine qui servira de serveur DHCP et lui attribuer une adresse IP de
notre réseau, par exemple : 192.168.2.10.  

![morse](images/image12.png){ width=40%; : .center }

Le serveur est maintenant ajouté, mais il reste à le configurer.  
Pour configurer le serveur DHCP :  
Cliquer sur le serveur et choisir : Configuration du service DHCP (à droite) :  

![morse](images/image13.png){ width=40%; : .center }

Vous devez choisir le début et la fin de la plage des adresses IP à attribuer et n'oubliez pas de configurer la passerelle
(IP du routeur) en cliquant sur Configuration manuelle.  

Par exemple :  

![morse](images/image14.png){ width=40%; : .center }

Une fois le serveur DHCP configuré, vous devez indiquer à toutes les autres machines du réseau de l'utiliser en
cliquant sur "Adressage automatique par serveur DHCP" sur ces machines.  

Exercice :  
• Reprendre l'exercice de la partie 3 et ajouter un serveur DHCP à chaque sous-réseau.  
• Configurer les serveurs DHCP et les ordinateurs de chaque sous-réseau pour pouvoir utiliser ces serveurs.  
• Ajouter un nouvel ordinateur dans chaque sous réseau et le configurer (c'est maintenant immédiat)  
• Une fois le travail fait pour les 2 sous-réseaux, passer en mode simulation et tester :  
• Les adresses IP doivent avoir changé et être attribuées automatiquement  
• Les machines des 2 réseaux doivent toujours communiquer entre-elles et entre les sous-réseaux (vérifier
avec ping).  
Sauvegardez votre travail…  

### 5.Serveur Web et serveur DNS.

L'utilisation la plus visible d'Internet est la consultation de pages web. Nous allons voir les processus de base
impliqués dans la communication entre un navigateur web installé sur une machine et un serveur web.  


En repartant du travail précédent :

![morse](images/image15.png){ width=70%; : .center }

Ajouter une machine nommée "Serveur Web" dans le 1er sous-réseau et configurer-la.  
En mode simulation, sur cette machine, installer un éditeur de texte et un serveur web.  

![morse](images/image16.png){ width=70%; : .center }

Lancer l'application Server web et démarrer le serveur web (virtuel) en cliquant sur le bouton Démarrer.  
Ajouter un navigateur à l'une des machines du réseau (M1 par exemple) afin de consulter des pages web.  

![morse](images/image17.png){ width=40%; : .center }

Lancer Navigateur web et saisir l'adresse IP du serveur web (192.168.1.201 dans mon cas). Vous pouvez
observer le trafic sur le réseau et une page d'accueil qui s'ouvre :  

![morse](images/image18.png){ width=40%; : .center }


Une connexion à un serveur web a bien été réalisée mais ce n'est pas ce qu'on fait habituellement pour naviguer sur
le Web puisqu'on utilise normalement des URL (Uniform Resource Locator) et non des adresses IP.  
La résolution entre l'URL et l'adresse IP correspondante est effectuée par un serveur de noms de domaine,
ce service dénommé DNS (Domain Name System) permet de traduire les adresses IP (exemple "128.93.162.163")
en adresse symbolique (exemple "nsi.ravel.free.fr") et réciproquement. Nous allons maintenant le créer et le
configurer.  
Ajouter un nouvel ordinateur qui va servir de serveur DNS avec l'adresse IP 192.168.3.1 et la passerelle 192.168.3.254.  

![morse](images/image19.png){ width=70%; : .center }

Changer le nombre d'interfaces du routeur pour le passer à trois (Gérer les connexions, "+" en bas d'Interfaces
locales)  
Configurer la passerelle du serveur DNS (serveur lui-même et routeur) pour qu'il puisse communiquer avec les
autres réseaux.  
Pour permettre à tous les ordinateurs d'utiliser les services du serveur DNS, nous devons ajouter l'adresse IP du
serveur DNS à la configuration de chaque ordinateur (ou plus simple à chaque serveur DHCP).  
Passer en mode simulation et ajouter un Serveur DNS à la machine DNS. Il faut maintenant paramétrer et
démarrer ce serveur DNS.  
Choisir un nom de domaine (par exemple www.snt2nde.fr) et indiquer l'adresse IP du serveur web (192.168.1.201
dans mon cas).  

![morse](images/image20.png){ width=40%; : .center }

Démarrer le serveur.

![morse](images/image21.png){ width=40%; : .center }

Installer un serveur web sur une machine de chaque sous-réseau et vérifier que vous pouvez accéder à la page web
dont l'url est http://www.snt2nde.fr  

Remarque : Vous pouvez changer la page web affichée par défaut et la remplacer par votre propre page html.  
Pour cela :  
• Lancer l'éditeur de texte installé sur le serveur Web  
• Ouvrir la page d'accueil du site qui se nomme index.html et qui se trouve dans le répertoire racine / webserver  
• Modifier les informations souhaitées.  



