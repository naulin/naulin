---
author: Votre nom
title: Notions de programmation
---


[![texte alternatif de l'image](http://img.youtube.com/vi/Og847HVwRSI/0.jpg){: .center}](https://youtu.be/Og847HVwRSI "Titre de la video")

## Initiation à Python : 

[Python découverte](https://e-nsi.forge.aeif.fr/init_python/)

Python est un langage de programmation de haut-niveau, largement employé et populaire.  
Il est utilisé dans le développement web, dans les applications de machine learning, IA, programmation de jeu ...  
Les programmes Python sont généralement plus petits que dans les autres langages.  
Les programmeurs ont relativement moins à taper et l'indentation requise dans ce langage rend le code lisible.  
La syntaxe est très simple et facile à apprendre.  

Exemple :

???+ question "Tester le programme"

    {{ IDE('print') }}

Le langage Python est portable sur de nombreuses plateformes, ce qui en fait un langage populaire.  
Des applications comme Youtube, Netflix, Instagram, Quora, Dropbox sont programmées avec Python.  

## 1. La fonction print et les affectations de variables

La fonction **print()** :  
Une fonction est une suite d'instructions qui peut prendre des paramètres
en entrée, elle fait ensuite des "calculs" avec et renvoie un résultat.  

Exemple :

???+ question "Tester le programme"

    {{ IDE('printexemples') }}


Les paramètres **sep** et **end** dans la fonction **print()** :

Le paramètre **end** permet de créer un espace et force l'affichage du print suivant sur la même ligne.  
Le paramètre **sep** permet de séparer plusieurs plusieurs paramètres d'entrée par un caractère de son choix.
(par défaut il y a un espace).


Exemple :

???+ question "Tester le programme"

    {{ IDE('endsep') }}


Les **variables** en Python :

Une variable est une zone de la mémoire de l'ordinateur dans laquelle une valeur est stockée. Aux yeux du programmeur, cette variable est définie par un nom, alors que pour l'ordinateur, il s'agit en fait d'une adresse, c'est-à-dire d'une zone particulière de la mémoire.  
En Python on n'a pas besoin de spécifier le type de la variable (entier, réel, etc ...) car Python est un langage au typage dynamique.  

Exemple :


???+ question "Tester le programme"

    {{ IDE('variables') }}


!!! warning "Nommage :"
    Le nom des variables en Python peut être constitué de lettres minuscules (a à z), de lettres majuscules (A à Z), de nombres (0 à 9) ou du caractère souligné (_). Vous ne pouvez pas utiliser d'espace dans un nom de variable.

    Par ailleurs, un nom de variable ne doit pas débuter par un chiffre et il n'est pas recommandé de le faire débuter par le caractère _ (sauf cas très particuliers).

    De plus, il faut absolument éviter d'utiliser un mot « réservé » par Python comme nom de variable (par exemple : print, range, for, from, etc.).

    Enfin, Python est sensible à la casse, ce qui signifie que les variables TesT, test et TEST sont différentes.


Comment fonctionne les variables?  

???+ question "Tester le programme"

    {{ IDE('variablesbis') }}

!!! warning "Attention :"
    utiliser une variable sans l'assigner renvoie une erreur.


???+ question "Tester le programme"

    {{ IDE('erreurvar') }}



## 2. La fontion input()

La fonction **input()** est utilisée pour demander et stocker une entrée de la part d'un utilisateur.  

Exemple :

???+ question "Tester le programme"

    {{ IDE('input') }}

!!! tag "A noter :"
    **input()** renvoie tout le temps une chaîne de caractères, donc pour pouvoir
    faire des calculs on doit convertir en entier, réels ...

## 2.a Les fonctions en Python :

[lien vers Travail sur les fonctions](https://capytale2.ac-paris.fr/web/c/339d-3208497/mlc)


## 3. Les commentaires dans les programmes

Il faudra apprendre à commenter (i.e : indiquer ce que fait votre code) et ceci dans tous les langages
que vous serez amenés à utiliser.  

En Python, on insère un commentaire avec le caractère # (un dièse).  N'utilisez donc de commentaires que lorsque cela s'avère nécessaire, une fois que vous avez travaillé la clarté de votre code au maximum.

Voici un exemple :

???+ question "Tester le programme"

    {{ IDE('commentaires') }}

**Remarque :** Une variante consiste à commencer un commentaire avec trois guillemets """ et à le finir de la même façon. C'est une façon aisée d'écrire un long commentaire qui court sur plusieurs lignes :


Voici un exemple :

???+ question "Tester le programme"

    {{ IDE('commentairesbis') }}

En réalité, il ne s'agit pas d'un commentaire mais d'un texte du programme s'étendant sur plusieurs lignes. Sans instruction pour l'utiliser, il est cependant ignoré à l'exécution.

## 4. Les conditions

Il arrive dans la vie que l'on doive faire certaines tâches et en fonction de certaines conditions et on 
décide ce que l'on doit faire après. (exemple : je vais aller manger et si j'ai le temps je ferai mes devoirs).
En programmation il arrive aussi que certaines tâches soient exécutées si certaines conditions sont vérifiées.  
Dans ce cas une déclaration conditionnelle est utilisée :  

* if
* if ... else
* if ... elif ... 

Exemples :

Voici un exemple :

???+ question "Tester le programme"

    {{ IDE('ifelif') }}



## 5. La boucle bornée for ...

En programmation, on est souvent amené à répéter plusieurs fois une instruction. Incontournables à tout langage de programmation, les boucles vont nous aider à réaliser cette tâche répétitive de manière compacte et efficace.

Imaginez par exemple que vous souhaitiez afficher les éléments d'une liste les uns après les autres. Dans l'état actuel de vos connaissances, il faudrait taper quelque chose du style :

???+ question "Tester le programme"

    {{ IDE('bouclefor1') }}

Si votre liste ne contient que 4 éléments, ceci est encore faisable mais imaginez qu'elle en contienne 100 voire 1000 ! Pour remédier à cela, il faut utiliser les boucles. Regardez l'exemple suivant :



???+ question "Tester le programme"

    {{ IDE('bouclefor2') }}


La variable animal est appelée variable d'itération, elle prend successivement les différentes valeurs de la liste animaux à chaque itération de la boucle. On verra un peu plus loin dans ce chapitre que l'on peut choisir le nom que l'on veut pour cette variable. Celle-ci est créée par Python la première fois que la ligne contenant le for est exécutée (si elle existait déjà son contenu serait écrasé). Une fois la boucle terminée, cette variable d'itération animal n'est pas détruite et conserve la dernière valeur de la liste animaux (ici la chaîne de caractères "souris").

Dans l'exemple suivant, le corps de la boucle contient deux instructions (ligne 2 et ligne 3) car elles sont indentées par rapport à la ligne débutant par for :


???+ question "Tester le programme"

    {{ IDE('bouclefor3') }}


La ligne 4 ne fait pas partie du corps de la boucle car elle est au même niveau que le for (c'est-à-dire non indentée par rapport au for). Notez également que chaque instruction du corps de la boucle doit être indentée de la même manière (ici 4 espaces).

!!! warning "Attention :"
    Si on oublie l'indentation, Python renvoie un message d'erreur !!!

### Utilisation de la fonction range()

Python possède la fonction range() qui est bien commode pour faire une boucle sur une liste d'entiers de manière automatique :



???+ question "Tester le programme"

    {{ IDE('bouclerange1') }}

Dans l'exemple précédent, nous avons choisi le nom i pour la variable d'itération. Ceci est une habitude en informatique et indique en général qu'il s'agit d'un entier (le nom i vient sans doute du mot indice ou index en anglais). Nous vous conseillons de suivre cette convention afin d'éviter les confusions, si vous itérez sur les indices vous pouvez appeler la variable d'itération i (par exemple dans for i in range(4):).

Revenons à notre liste animaux. Nous allons maintenant parcourir cette liste, mais cette fois par une itération sur ses indices :



???+ question "Tester le programme"

    {{ IDE('bouclerange2') }}


La variable i prendra les valeurs successives 0, 1, 2 et 3 et on accèdera à chaque élément de la liste animaux par son indice (i.e. animaux[i]). Notez à nouveau le nom i de la variable d'itération car on itère sur les indices.

Quand utiliser l'une ou l'autre des 2 méthodes ? La plus efficace est celle qui réalise les itérations directement sur les éléments :



???+ question "Tester le programme"

    {{ IDE('bouclerange3') }}




## 6. La boucle non bornée while ...

Une alternative à l'instruction for couramment utilisée en informatique est la boucle while. Avec ce type de boucle, une série d'instructions est exécutée tant qu'une condition est vraie. Par exemple :




???+ question "Tester le programme"

    {{ IDE('bouclewhile1') }}

Remarquez qu'il est encore une fois nécessaire d'indenter le bloc d'instructions correspondant au corps de la boucle (ici, les instructions lignes 3 et 4).

Une boucle while nécessite généralement trois éléments pour fonctionner correctement :

Initialisation de la variable d'itération avant la boucle (ligne 1).
Test de la variable d'itération associée à l'instruction while (ligne 2).
Mise à jour de la variable d'itération dans le corps de la boucle (ligne 4).
Faites bien attention aux tests et à l'incrémentation que vous utilisez car une erreur mène souvent à des « boucles infinies » qui ne s'arrêtent jamais. Vous pouvez néanmoins toujours stopper l'exécution d'un script Python à l'aide de la combinaison de touches Ctrl-C (c'est-à-dire en pressant simultanément les touches Ctrl et C). Par exemple :


???+ question "Tester le programme"

    {{ IDE('bouclewhile2') }}


Ici, nous avons omis de mettre à jour la variable i dans le corps de la boucle. Par conséquent, la boucle ne s'arrêtera jamais (sauf en pressant Ctrl-C) puisque la condition i < 10 sera toujours vraie.

La boucle while combinée à la fonction input() peut s'avérer commode lorsqu'on souhaite demander à l'utilisateur une valeur numérique. Par exemple :



???+ question "Tester le programme"

    {{ IDE('bouclewhile3') }}




La fonction input() prend en argument un message (sous la forme d'une chaîne de caractères), demande à l'utilisateur d'entrer une valeur et renvoie celle-ci sous forme d'une chaîne de caractères. Il faut ensuite convertir cette dernière en entier (avec la fonction int() ligne 4).

## 7 Exercices
Conseil : pour ces exercices, créez des scripts puis exécutez-les dans un shell.

### 7.1 Boucles de base
Soit la liste ["vache", "souris", "levure", "bacterie"]. Affichez l'ensemble des éléments de cette liste (un élément par ligne) de trois façons différentes (deux méthodes avec for et une avec while).


???+ question "Tester le programme"

    {{ IDE('exo71_corr') }}

### 7.2 Boucle et jours de la semaine
Constituez une liste semaine contenant les 7 jours de la semaine.

Écrivez une série d'instructions affichant les jours de la semaine (en utilisant une boucle for), ainsi qu'une autre série d'instructions affichant les jours du week-end (en utilisant une boucle while).

???+ question "Tester le programme"

    {{ IDE('exo72_corr') }}

### 7.3 Nombres de 1 à 10 sur une ligne
Avec une boucle, affichez les nombres de 1 à 10 sur une seule ligne.

Conseil : n'hésitez pas à relire le début du chapitre 3 Affichage qui discute de la fonction print().

???+ question "Tester le programme"

    {{ IDE('exo73_corr') }}

### 7.4 Nombres pairs et impairs
Soit impairs la liste de nombres [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]. Écrivez un programme qui, à partir de la liste impairs, construit une liste pairs dans laquelle tous les éléments de impairs sont incrémentés de 1.

???+ question "Tester le programme"

    {{ IDE('exo74_corr') }}

### 7.5 Calcul de la moyenne
Voici les notes d'un étudiant [14, 9, 6, 8, 12]. Calculez la moyenne de ces notes. 

???+ question "Tester le programme"

    {{ IDE('exo75_corr') }}

### 7.6 Suite de Fibonacci
La suite de Fibonacci est une suite mathématique qui porte le nom de Leonardo Fibonacci, un mathématicien italien du XIII siècle. Initialement, cette suite a été conçue pour décrire la croissance d'une population de lapins, mais elle peut également être utilisée pour décrire certains motifs géométriques retrouvés dans la nature (coquillages, fleurs de tournesol...).

Pour la suite de Fibonacci , le terme au rang n est la somme des nombres aux rangs n-1 et n-2:


Par définition, les deux premiers termes sont et 0 et 1.

À titre d'exemple, les 10 premiers termes de la suite de Fibonacci sont donc 0, 1, 1, 2, 3, 5, 8, 13, 21 et 34.

Créez un script qui construit une liste fibo avec les 15 premiers termes de la suite de Fibonacci puis l'affiche.

???+ question "Tester le programme"

    {{ IDE('exo76_corr') }}


## 8. Définition et appels de fonction

On sait maintenant écrire des programmes Python avancés, mais ils vont vite devenir longs en nombre de lignes de code. De plus, on risque très vite de se retrouver avec des répétitions de codes similaires. Ce chapitre décrit le concept de fonction grâce auquel on va pouvoir écrire du code plus compact, lisible et réutilisable. On a déjà pu en utiliser dans les chapitres précédents, et on va ici voir comment en définir.

### Définition et appel de fonction

Une fonction se définit avec le mot réservé **def**, suivi de son nom, d'une liste de paramètres (qui peut être vide), du caractère deux-points (:) et enfin d'un bloc de code représentant son **corps**. Une fois définie, elle peut être utilisée autant de fois qu'on le souhaite, en l'appelant.

**Fonction sans paramètre :**

Exemple :

???+ question "Tester le programme"

    {{ IDE('fonctionsans') }}

En entrant ces quelques lignes, nous avons défini une fonction très simple qui compte jusqu’à 2. Notez bien les parenthèses, les deux-points, et l’indentation du bloc d’instructions qui suit la ligne d’en-tête (c’est ce bloc d’instructions qui constitue le corps de la fonction proprement dite).

Après la définition de la fonction, on trouve le programme principal qui débute par l’instruction print("bonjour"). Il y a ensuite au sein du programme principal, l’appel de la fonction grâce à compteur3().

**Exemple de fonction qui appelle une autre fonction :**

???+ question "Tester le programme"

    {{ IDE('fonctionappel') }}

Une première fonction peut donc appeler une deuxième fonction, qui elle-même en appelle une troisième, etc.

Créer une nouvelle fonction offre l’opportunité de donner un nom à tout un ensemble d’instructions. De cette manière, on peut simplifier le corps principal d’un programme, en dissimulant un algorithme secondaire complexe sous une commande unique, à laquelle on peut donner un nom explicite.

Une fonction est donc en quelque sorte une nouvelle instruction personnalisée, qu’il est possible d’ajouter librement à notre langage de programmation.

### Fonction avec paramètre :

Exemple :

???+ question "Tester le programme"

    {{ IDE('fonctionavec') }}

Pour tester cette nouvelle fonction, il nous suffit de l’appeler avec un argument.

### Fonction avec plusieurs paramètres :

Exemple :

???+ question "Tester le programme"

    {{ IDE('fonctionplusieurs') }}


!!! tag "A noter :"
    Pour définir une fonction avec plusieurs paramètres, il suffit d’inclure ceux-ci entre les parenthèses qui suivent le nom de la fonction, en les séparant à l’aide de virgules.

    Lors de l’appel de la fonction, les arguments utilisés doivent être fournis dans le même ordre que celui des paramètres correspondants (en les séparant eux aussi à l’aide de virgules). Le premier argument sera affecté au premier paramètre, le second argument sera affecté au second paramètre, et ainsi de suite.

### « Vraies » fonctions et procédures

Pour les puristes, les fonctions que nous avons décrites jusqu’à présent ne sont pas tout à fait des fonctions au sens strict, mais plus exactement des procédures. Une « vraie » fonction (au sens strict) doit en effet renvoyer une valeur lorsqu’elle se termine. Une « vraie » fonction peut s’utiliser à la droite du signe égale dans des expressions telles que y = sin(a). On comprend aisément que dans cette expression, la fonction sin() renvoie une valeur (le sinus de l’argument) qui est directement affectée à la variable y.

Voici un exemple extrêmement simple:

???+ question "Tester le programme"

    {{ IDE('fonctionsimple') }}