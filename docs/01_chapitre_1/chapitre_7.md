---
author: Votre nom
title: La photographie numérique
---
Cette activité est très inspirée du site : https://snt.ababsurdo.fr/la-photographie-numerique/pixels/

[![texte alternatif de l'image](http://img.youtube.com/vi/UnNPNc-F9ks/0.jpg){: .center}](https://youtu.be/UnNPNc-F9ks "Titre de la video")


## I. Pixels :


!!! info "Consignes :"

    Répondez aux questions sur un compte-rendu papier, manuscrit et personnel. Lors du contrôle sur cette partie, vous aurez ce compte-rendu avec vous.  
    Vous pouvez faire ce travail à plusieurs, mais prenez garde à bien comprendre toutes les réponses (même celles apportées par vos camarades) et à ce que votre compte-rendu soit personnel : vous ferez le contrôle seul·e·s, même si vous avez fait cette activité à plusieurs !
    Si votre compte-rendu n'est pas manuscrit (impression ou photocopie), vous n'aurez pas le droit de l'utiliser pendant le devoir.
    Les parties ❤️ À savoir désignent les savoirs ou savoir-faire qui pourront vous être demandés en devoir.



Lorsqu'une question est marquée d'un symbole 🔎, la réponse est à aller chercher sur Internet.

### 1. Pixels, Définition, Résolution

Une photographie numérique (au format jpg, png, tiff, etc.) est un tableau de pixels, chaque pixel étant une case du tableau, d'une couleur bien précise.  

La définition d'un écran est le nombre de pixels qui composent l'écran (généralement données sous la forme 1024×780, signifiant « 1024 pixels de large, et 780 pixels de haut »).  


![morse](images/pixel-example.png){ width=50%; : .center }

La résolution d'un écran (exprimée en dpi (dots per inch), ppi (pixels per inch), ou ppp (pixels par pouce)) est le nombre de pixels disponibles sur une longueur d'un pouce (environ 2,54 cm). Plus ce nombre est élevé, plus la taille des pixels est petite, et plus l'image sera précise.  


![morse](images/cellpixels.jpg){ width=30%; : .center }

1. 🔎 Quelle est l'étymologie du mot pixel ?
2. 🔎 Quelle est la définition (le nombre de pixels en largeur et hauteur) d'un écran HD, Full HD, et 4K ?
3. Écran de téléphone portable. Sur la fiche technique du téléphone portable Fairphone 3 est écrit : « 2160 x 1080 resolution ; 427 ppi pixel densité. » Quelle est la résolution de cet écran (attention : il y a un piège) ?

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>427</p>
</details>


4. Écran de télévision 4K. Une télévision 4K a les caractéristiques techniques suivantes :

    * Taille de l'écran 55"
    * Résolution : Longueur 3840 Pixels
    * Résolution : Largeur 2160 Pixels
    * Longueur du produit 123,06 cm
    * Largeur du produit 79,26 cm
    * Hauteur du produit 23,75 cm

a. 🔎 Convertir la largeur de la télé en pouces (arrondir au dixième).  

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>31.2 pouces</p>
</details>

b. Quelle est la résolution (en largeur) de cet écran (arrondir à l'unité) ? 

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>69</p>
</details>

c. La résolution est-elle plus grande ou plus petite que celle du téléphone portable étudié à la question précédente ?  

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Elle est plus petite, ce qui est normal : puisque l'écran d'un téléphone portable est plus près des yeux que celui d'une télévision, il faut une résolution plus grande pour ne pas distinguer chacun des pixels.</p>
</details>


5. Les imprimantes n'utilisent pas de pixels, mais manipulent de minuscules points d'encre, qui forment des lettres et autres dessins. Il est possible de faire des calculs avec les points d'encre d'une imprimante de la même manière que les calculs faits avec les pixels d'un écran.

Une imprimante a une résolution de 300 ppp (dpi en anglais), soit 300 points d'encre par pouce (un pouce ≈ 2,54 cm). On souhaite imprimer une photo en pleine page sur une feuille A4.

a. 🔎 Quelle est la définition de cette feuille pour cette imprimante (arrondir à l'unité) ?

Hauteur :  

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>3508</p>
</details>

Largeur :  

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>2480</p>
</details>

b. On désire imprimer une photo sur toute la feuille. On souhaite savoir si la définition de 1024 × 768 est suffisante.

i. Les 1024 pixels seront imprimés sur 3508 points d'encre. Combien de points d'encre seront utilisés pour chaque pixel (arrondir à l'unité) ?

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>3</p>
</details>

ii. Quel sera l'effet sur la photo, une fois imprimée ?

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Puisque chaque pixel sera imprimé sur plusieurs points d'encre : l'image risque d'être pixellisée.</p>
</details>

!!! warning "❤️ À savoir :"

    * Je connais la définition de résolution et définition.  
    * Je sais convertir les centimètres en pouces et inversement (exemple : Combien mesure, en pouces, un écran de 5cm ?).  
    * Je sais passer de la résolution à la définition, et inversement (exemple : Un écran de 7cm a une résolution de 30ppp ; quelle est sa définition ?).  

## II. Couleurs :

Dans cette partie nous nous intéresserons à la manière dont sont codées les couleurs.

### Souvenirs d'art plastique

1. Quelles sont les trois couleurs primaires en peinture  ?
2. Comment obtenir les couleurs suivantes à partir des trois couleurs primaires : orange, vert, violet ?

### Modèle RGB

En informatique, les couleurs sont codées en utilisant trois nombres. Nous allons voir dans cette partie comment passer des couleurs (bleu clair, orange, noir, rouge, etc.) aux nombres.  

Vous pouvez faire cette partie, au choix, en utilisant le logiciel GIMP, ou en allant sur un site web en ligne.  

**Si vous utilisez GIMP**  

Ouvrir le logiciel GIMP, puis cliquer sur la couleur de premier plan (le carré noir dans le menu de gauche) pour afficher la palette des couleurs. Vous obtenez alors la fenêtre suivante.  

![morse](images/gimp-palette.png){ width=50%; : .center }

Dans cette partie, nous nous intéressons uniquement aux trois lignes R, G, B (qui ont, sur l'image, les valeurs 88,7, 21,3 et 94,2).

Vous pouvez alors :

en sélectionnant une couleur dans la partie de gauche, voir quels nombres R, G, B codent cette couleur ;  
en écrivant les nombres R, G, B dans la partie de droite, voir à quelle couleur ils correspondent.  
Nous voyons sur la capture d'écran que la couleur rose correspond aux nombres R=88,7, G=21,3, B=94,2.  

**Si vous utilisez le site web**

Allez sur le site web Sélecteur de couleurs CSS [Lien](https://htmlcolorcodes.com/fr/selecteur-de-couleur/) : vous voyez la palette suivante.

Dans cette partie, nous nous intéressons uniquement à la ligne contenant les trois nombres R, G, B (qui ont, sur l'image, les valeurs 245, 106 et 222).

![morse](images/mozilla-palette.png){ width=50%; : .center }

Vous pouvez alors :

en sélectionnant une couleur dans « l'arc en ciel » et la partie de gauche, voir quels nombres R, G, B codent cette couleur ;  
en écrivant les nombres R, G, B dans la partie de droite, voir à quelle couleur ils correspondent.  
Nous voyons sur la capture d'écran que la couleur rose correspond aux nombres R=245, G=106, B=222.  


**Questions**


1. 🔎 Que signifient les initiales RGB ?
2. Quelles sont les valeurs minimales et maximales que peuvent prendre les quantité de rouge, vert, bleu ?

Minimale :

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>0</p>
</details>

Maximale :

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>255</p>
</details>

3. En utilisant la palette (de Gimp ou du site web) utilisée à la question précédente, donner le code des couleurs suivantes (on notera (x, y, z) la couleur composée de x rouge, y vert et z bleu) :

Rouge : (255, 0, 0)  
Vert : (…, …, …)  
Bleu : (…, …, …)  
Noir : (…, …, …)  
Blanc : (…, …, …)  
En utilisant le même outil, avec la même notation, donner les couleurs (en français) correspondant aux codes suivants (un seul mot par couleur) :  

(0, 255, 255)  

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Bleu clair</p>
</details>


(255, 0, 255)

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Violet</p>
</details>


(255, 255, 0)

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Jaune</p>
</details>


(255, 128, 0)

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>orange</p>
</details>


Combien de couleurs différentes est-il possible de coder en utilisant cette méthode ?

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>16777216 = 256 x 256 x 256</p>
</details>

6. 🔎 Quels autres modèles de couleur sont aussi utilisés en informatique ? 

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>TSL, TSV, CMJN…</p>
</details>



!!! warning "❤️ À savoir :"

    Je connais la signification des initiales RGB.  
    Je connais la signification des trois nombres d'un code RGB (exemple : Dans la couleur (27, 150, 42), quelle est la « quantité » de vert, de rouge, de bleu ?).  
    Je connais le code RGB des couleurs suivantes : noir, blanc, rouge, vert, bleu.  
    Je sais calculer le nombre de couleurs, c'est-à-dire, je sais expliquer d'où sort le nombre 16777216.  

## III. Capteurs et Photosites

Lire la page suivante, qui explique le fonctionnement d'un appareil photo numérique : [Capteur photo](https://pixees.fr/informatiquelycee/n_site/snt_photo_capteur.html).

Répondre aux questions suivantes (toutes les réponses se trouvent dans le document qui vient d'être lu).

Compléter la phrase suivante : « Un photosite est un composant électronique ayant la capacité de capter un signal 

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p> lumineux </p>
</details>

pour le convertir en un signal 

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p> électrique (grâce à l'effet photoélectrique)</p>
</details>

, qui sera lui-même converti en un 
 
<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p> numérique (on parle de numérisation). »</p>
</details>


Comment fait-on pour qu'un photosite ne soit sensible qu'à une des trois couleurs (rouge, verte, bleue) ? 

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>On ajoute un filtre de la couleur requise.</p>
</details>

Pourquoi y a-t-il deux fois plus de photosites verts que de photosites des autres couleurs ? 


<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>L'œil humain est plus sensible à la couleur verte.</p>
</details>



Combien de photosites faut-il associer pour créer un seul pixel ?

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>4</p>
</details>

!!! warning "❤️ À savoir :"
    Je suis capable de répondre aux quatre questions ci-dessus.


## IV. Traitement d'image

En manipulant les pixels d'une image, il est possible de la modifier, à plusieurs fins.

Lisez les quelques exemples, et répondez aux questions de la dernière partie.

### IV. a. Exemples de traitement d'image

**Ajout de personnes absentes de la photo originale**

![morse](images/congress-women.jpg){ width=20%; : .center }
[113th congress usa women version altered by office of House Minority Leader.jpg](https://en.wikipedia.org/wiki/File:113th_congress_usa_women_version_altered_by_office_of_House_Minority_Leader.jpg), original de US Congress, modifié par Office of the House Minority Leader, domaine public.

Quatre personnes, qui n'ont pu arriver à temps, ont été ajoutées après la prise de la photo.

**Flou artificiel de l'arrière-plan**

![morse](images/flou.jpg){ width=20%; : .center }
[Faux-bokeh-comparison](https://commons.wikimedia.org/wiki/File:Faux-bokeh-comparison.jpg), par BenFrantzDale, retouché par Cmglee, publié sous licence Creative Commons by-sa 3.0.

La photo originale (1) a été altérée pour flouter l'arrière plan, en utilisant deux méthodes différentes (2 et 3).

**Prise de vue en fourchette**

![morse](images/focus-stacking.jpg){ width=20%; : .center }
[Focus stacking Tachinid fly](https://commons.wikimedia.org/wiki/File:Focus_stacking_Tachinid_fly.jpg), par Muhammad Mahdi Karim, publié sous licence Creative Commons by-sa 3.0.

Série d'images d'une mouche tachinaire montrant l'intérêt de la Prise de vue en fourchette. Les deux première images montrent les problèmes de profondeur de champ rencontrés, la troisième a été obtenue en combinant six clichés réalisés avec six mises au point différentes.

**Assemblage de photos**

![morse](images/panorama.jpg){ width=20%; : .center }
[Rochester NY](https://en.wikipedia.org/wiki/File:Rochester_NY.jpg), par Noso1. The copyright holder of this file allows anyone to use it for any purpose, provided that the copyright holder is properly attributed. Redistribution, derivative work, commercial use, and all other use is permitted.0.

Plusieurs photos différentes sont assemblées pour donner l'illusion d'une seule photo panoramique.

**Correction de la perspective**



![morse](images/remap.jpg){ width=20%; : .center }
[Panotools5618](https://commons.wikimedia.org/wiki/File:Panotools5618.jpg), par Ashley Pomeroy, publié sous licence Creative Commons by 3.0.

La photo prise avec une lentille fisheye (en haut) a été altérée pour corriger la perspective.

**Fausse miniature**


![morse](images/tilt-shift.png){ width=20%; : .center }
[Jodhpur rooftops](https://commons.wikimedia.org/wiki/File:Jodhpur_rooftops.jpg),  par Paul Goyette, publié sous licence Creative Commons by-sa 2.0.

En floutant numériquement la photo de gauche, la photo de droite donne l'impression d'une photo de modèle réduit.

### IV. b. Questions
Classer les six exemples de manipulation d'image dans les trois catégories suivantes (plusieurs réponses sont possibles) :

Algorithmes essayant de reproduire le plus fidèlement possible la réalité.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Assemblage de photo, prise de vue en cascade…</p>
</details>

  

Algorithmes essayant d'imiter ou de créer un effet artistique. 


<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Fausse miniature…</p>
</details>

Algorithmes produisant l'image d'une situation qui n'a jamais existé.

<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Ajout de personnes…</p>
</details>



Citez deux autres exemples de filtres (que vous utilisez peut-être), et :

décrivez-les (je ne les connais pas forcément) ;  
classez-les dans les catégories de la question précédente.  

Une photographie peut-elle être une preuve irréfutable (oui ou non) ?


<details>
  <summary>Cliquez pour afficher la solution</summary>
  <p>Non, Parce qu'il est maintenant possible de modifier des images, ou créer de fausses images, qui soient quasiment indiscernables d'une vraie image.</p>
</details>


!!! warning "❤️ À savoir :"
    Je sais donner et décrire quelques algorithmes de manipulation d'image.


## V. Métadonnées (EXIF)  


Un fichier (.jpg, .png, etc.) d'une photographie numérique comprend non seulement les informations sur les couleurs, mais aussi des méta-données, c'est-à-dire des informations à propos de l'image.

### V. a. Premières manipulations      

1. Télécharger la photo [pelican.jpg](images/pelican.jpg), puis ouvrez-la avec The Gimp.  
2. Allez dans le menu Images > Métadonnées > Afficher les métadonnées. Une fenêtre de ce genre là doit s'ouvrir.   


![morse](images/pelicanm.jpg){ width=20%; : .center } 

Nous voyons sur cette capture que l'altitude de prise de vue de la photo est 169,2 mètres.  

3. En parcourant les données, retrouver :  

la date à laquelle a été prise cette photo ;  
la marque de l'appareil photo ;  
si le flash a été utilisé.  

4. On se demande où a été prise cette photo. Dans les métadonnées, on trouve les coordonnées GPS, avec comme latitude 45deg 46'36.710'' et comme longitude 4deg51'18.280''. On cherche alors sur [Géoportail](https://www.geoportail.gouv.fr/) ces coordonnées :

Cliquer sur OK pour afficher une carte.
Cliquer sur le ➕ pour effectuer une recherche.
Choisissez Coordonnées, puis degrés sexagésimaux.
Recopiez les coordonnées GPS.
Quel est le lieu de la prise de vue ?

### V. b. À vous 

En utilisant la même méthode qu'à la partie précédente, déterminer où a été prise la photo [statue-de-la-liberte.jpg](images/statue-de-la-liberte.jpg) ?

### V. c. Fiabilité

1. Où se trouve le tableau La Joconde, de Léonard de Vinci ?
2. Selon les métadonnées de la photo [joconde.jpg](images/joconde.jpg), où la photo a-t-elle été prise ? Est-ce normal ?
3. Proposer deux explications possibles pour cette anomalie.  *


## VI.  Pixel art

La bibliothèque **Pillow** de Python permet de manipuler, pixel par pixel, des images en Python. Chaque image est considérée comme un tableau, chaque case contenant un triplet de nombres (R, G, B).

Par exemple, l'image ci-dessous représente une image de 3×2 pixels. Deux choses sont à noter sur les coordonnées :

les coordonnées commencent à 0 (comme dans les ascenceurs, ce qui est courant en informatique) ;
l'axe vertical est gradué de haut en bas (alors qu'habituellement en mathématiques, il est gradué de bas en haut).
Ainsi, la couleur du pixel de coordonnées (2, 1) (en haut à droite) est (236, 25, 32) (rouge).

Le programme Python suivant permet de dessiner le drapeau français de l'exemple ci-dessus.

![morse](images/drapeau.png){ width=20%; : .center } 

```python
from PIL import Image

image = Image.new('RGB', (3, 2))

image.putpixel((0, 0), (5, 20, 64))
image.putpixel((0, 1), (5, 20, 64))
image.putpixel((1, 0), (255, 255, 255))
image.putpixel((1, 1), (255, 255, 255))
image.putpixel((2, 0), (236, 25, 32))
image.putpixel((2, 1), (236, 25, 32))

image.save("image.png")
```

Voici l'explication de chaque ligne :  

Chargement de la bibliothèques pillow, qui permet à Python de manipuler des images.

```python
from PIL import Image
```

Création d'une nouvelle image, de 3 pixels de large par 2 pixel de haut.

```python
image = Image.new('RGB', (3, 2))
```

Tracé d'un pixel de couleur (5, 20, 64) aux coordonnées (0, 1).

```python
image.putpixel((0, 1), (5, 20, 64))
```

Écriture de l'image dans le fichier image.png.

```python
image.save("image.png")
```

### VI. a. Pixel par pixel

1. Reproduisez le quadrillage suivant sur une feuille, et faites un dessin (une lettre, un smiley, etc.) en coloriant certaines cases. Votre dessin devra être composé d'au moins trois couleurs (en plus du blanc).

![morse](images/quadrillage.png){ width=20%; : .center } 

2. Téléchargez le fichier [drapeau-france.py](scripts/drapeau-france.py), et renommez-le en dessin-NOM.py (en remplaçant NOM par votre nom de famille).

3. Ouvrez ce fichier avec le logiciel Spyder, puis exécutez-le.

Si vous obtenez une erreur ModuleNotFoundError: No module named 'PIL' : appelez le professeur.

4. Ouvrez le dossier qui contient votre fichier .py : un fichier image.png doit avoir été créé. Ouvrez cette image pour observer le drapeau français.

5. Modifiez le programme pour reproduire votre dessin, pixel par pixel.


### VI. b. Structures de contrôle

Plutôt que de définir les pixels un à un, il est possible d'utiliser des boucles. Voici quelques exemples.

**Quelques bandes**

```python
from PIL import Image

# On définit une image noire de 256 pixels par 256 pixels
image = Image.new("RGB", (256, 256))

# Pour chaque valeur de x, colorier le pixel de coordonnées (x, 7)
# Cela colorie toute la ligne de rang 7.
for x in range(256):
    image.putpixel((x, 7), (255, 0, 0))

# Pour chaque valeur de y, colorier le pixel de coordonnées (10, y)
# Cela colorie toute la ligne de rang 10.
for y in range(256):
    image.putpixel((10, y), (255, 255, 0))

image.save("image.png")
```
![morse](images/bande1.png){ width=20%; : .center } 

**Entrelacs**

```python
from PIL import Image

image = Image.new("RGB", (256, 256))

for y in range(26):
    for x in range(256):
        for h in range(5):
            image.putpixel((x, 10*y+h), (255, 0, 0))

for x in range(26):
    for y in range(256):
        for h in range(5):
            r, g, b = image.getpixel((10*x+h, y))
            image.putpixel((10*x+h, y), (r, 255, 0))

image.save("image.png")
```

![morse](images/entrelacs.png){ width=20%; : .center } 

**Hasard**

On peut aussi définir des coordonnées ou couleurs au hasard. Par exemple, random.randint(0, 255) donnera un nombre aléatoire entre 0 et 255.

Le programme suivant produit une « marche aléatoire » : un pixel se déplace aléatoirement sur le dessin, en laissant une trace colorée.

```python
from PIL import Image
import random

image = Image.new("RGB", (256, 256))

x = round(255 / 2)
y = round(255 / 2)
rouge = round(255 / 2)
vert = round(255 / 2)
bleu = round(255 / 2)

for i in range(1000):
    direction = random.choice(["gauche", "droite", "haut", "bas"])
    if direction == "gauche":
        x = (x - 1) % 256
    elif direction == "droite":
        x = (x + 1) % 256
    elif direction == "haut":
        y = (y - 1) % 256
    else:
        y = (y + 1) % 256

    rouge = (rouge + 2) % 256
    vert = (vert - 1) % 256
    if random.randint(0, 1) == 0:
        bleu = (bleu + 3) % 256
    else:
        bleu = (bleu - 3) % 256
    image.putpixel((x, y), (rouge, vert, bleu))

image.save("image.png")
```

![morse](images/random.png){ width=20%; : .center } 

À vous !

Faites une jolie image en utilisant tous ces outils.  

## VII. Traitement d'images

Une image se décompose en une multitude de points appelés pixels. 

![morse](../images/pixel-example.png){ width=50%; : .center } 

Chaque pixel possède sa propre couleur. Cette couleur résulte de la combinaison des trois couleurs primaires : rouge, vert et bleu (Red, Green et Blue : RGB). Ces niveaux peuvent en général prendre toutes les valeurs entre 0 et 255. On représente ainsi la couleur d’un pixel par trois nombres correspondant aux niveaux de rouge, vert et bleu. Par exemple pour un pixel vert on aura (0, 255, 0). Vous pouvez aller sur ce site pour voir le code RGB de toutes les couleurs affichables sur un écran : https://htmlcolorcodes.com/fr/.

Pillow  
La documentation de Pillow est disponible à l’adresse suivante : https://pillow.readthedocs.io/en/stable/reference/Image.html  

Pour vous faciliter la prise en main de cette bibliothèque, voici les commandes les plus utiles :  

![morse](../images/Pillow.png){ width=50%; : .center } 

###  Premier filtre : rouge

1) Créez un premier filtre qui ne garde que la composante rouge d’une image. C’est à dire qu’il faut pour chaque pixel mettre le vert et le bleu à zéro et ne pas toucher à la composante rouge. Ce filtre se présentera sous la forme d’une fonction prenant un objet Image en paramètre et retournant un nouvel objet image.


```python
from PIL import Image

image = Image.open("maison.jpg")

image.show()

def filtre_rouge(image):
    # On fait une copie de l'image pour ne pas modifier l'original
    nouvelle = image.copy()
    for y in range(image.height):
        for x in range(image.width):
            r, g, b = image.getpixel((x,y))
            nouvelle.putpixel((x,y), (r,0,0))
    return nouvelle



image_filtree = filtre_rouge(image)
image_filtree.show()
```
### Filtres
Créez les filtres suivants :  

2) Un filtre vert et un filtre bleu basés sur le même principe que le filtre rouge ;  

3) Un filtre gris qui transforme une image couleur en nuance de gris. Pour cela, chaque composante d’un pixel prend la moyenne des trois composantes.  