---
author: Votre nom
title: Nuit du Code
---

## I. Entraînement : 


Lien pour s'entraîner avec Pyxel  : [Pyxel](https://www.cahiernum.net/J682W5)

Lien pour s'entraîner avec Scratch  : [Scratch](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/SCRATCH/01-introduction/)
