---
author: Votre nom
title: Processeurs et SOC
---


# Des circuits aux systèmes sur puces
Cette partie est directement inspirée des documents d'accompagnement du programme de NSI.

Nous allons utiliser une vidéo du Collège de France : une conférence enseignement de Gérard Berry « Pourquoi et comment le monde devient numérique (Chaire Innovation technologique — Liliane Bettencourt) ». 

On trouvera avec ce lien le diaporama associé à la conférence.

[VIDEO](https://www.college-de-france.fr/fr/agenda/lecture/why-and-how-the-world-is-going-digital/des-circuits-aux-systemes-sur-puces)

## Questionnaire conférence

Vous répondrez sur feuille aux questions :

Qu’est-ce qui a permis la progression du monde numérique ?  
De quoi est composé un circuit ?  
À ce stade de l’exposé, quel est le facteur limitant à la progression des circuits ?  
Quel est le nom de la loi qui gouverne la densité des circuits ?  
Où travaillait M. Moore ?  
Citez quelques types de circuits autres que les microprocesseurs (CPU).  
Quelle qualité présente le CPU ?  
Quel est son principal défaut ?  
Que font principalement les DSP (Digital Signal Processor) ?  
Les puces peuvent être conçues avec un grand nombre de transistors, quelle est la conséquence sur la fabrication des puces ?  
Quels sont les deux principaux avantages d’une intégration plus grande dans les puces ?  
Que signifie SOC ?  
Comment sont reliés tous les blocs fonctionnels à l’intérieur des puces ?  


![image](data/P1_Img1.png){: .center}


La description des circuits
A partir de 7'48

Quel est le niveau de conception le plus basique décrit par M. Berry ?


![image](data/P1_Img2.png){: .center}

Comment s’appelle le niveau le plus central utilisé pour la description des circuits ?

![image](data/P1_Img3.png){: .center}

Quelle est la partie du signal d’horloge, qui est active, utilisée pour piloter le circuit ?

Dans un calcul logique, que signifie l’expression « chemin critique » ?
Que permet de définir ce chemin critique ?
Quelques réflexions autour de l'addition
A partir de 16'30

Quel est le problème posé par le mécanisme de propagation de la retenue lors du passage à l’échelle pour des additions de deux mots d’un grand nombre de bits ?  
Quelle est la valeur du chemin critique de l’additionneur de Von Neumann pour 
 bits ?  
Quelle méthode d’additionneur est présentée pour accélérer le traitement de l’addition ?
Que se passe-t-il dans ce type de fonctionnement ?  
Quel est le domaine technologique présenté qui réalise un très grand nombre d’opérations de calcul selon un mode pipeline ?  

Source : wikipedia

Le microprocesseur
A partir de 24'20

Que permet le mode de fonctionnement pipeline pour un microprocesseur ?  
Quel est le défaut principal de la mémoire RAM des ordinateurs ?  
Quelle est la solution pour résoudre ce problème ?  
La mémoire cache  
A partir de 34'35  

Quelle est la deuxième technique utilisée par les microprocesseurs pour accélérer les calculs ?  
Au détriment de quelles ressources internes au CPU le gain de temps est-il obtenu par la technique précédente ?  
En quoi consiste le prefetch ?  
Que conclure sur les microprocesseurs ?  
À quel moment ont lieu les pics de consommation dans le fonctionnement du microprocesseur ?  
Peut-on augmenter indéfiniment la fréquence de fonctionnement d’un circuit ?  
Quelle difficulté l’emploi des CPU multi coeurs entraine-t-il ?  
La conception des circuits  
A partir de 42'00  

Décrire succinctement la chaine de conception des circuits.  
Citez les deux jeux de logiciels qui interviennent.  
Quel est le pourcentage du coût dans le design d’un circuit ?  
L'avenir des circuits  
Citez les trois principaux freins à la miniaturisation des circuits.  
Quelle piste client est présentée pour remplacer les nouveaux circuits trop chers ?  
Comment définir un FPGA ?  
Quel est l’avantage d’utiliser des FPGA dans des routeurs par exemple ?  
Questionnaire complémentaire  
Densité d'intégration  
Le schéma ci-dessous est une vue partielle du microprocesseur 4004 d’Intel. Ce premier microprocesseur de l’histoire contenait 2300 transistors gravés avec une finesse de 10 μm. Le schéma complet tenait sur trois pages. En 2017, la finesse de gravure atteint 10 nm avec 10 milliards de transistors.

![image](data/P1_Img4.png){: .center}

Déterminer le nombre de pages nécessaire pour assurer l’impression du schéma d’un tel microprocesseur, en prenant comme base les données du 4004 et le ratio : nombre de transistors/nombre de pages.  
En considérant la surface standard d’une feuille A4, quelle est la surface totale du schéma de notre microprocesseur de technologie 2017 ? Convertir ensuite le résultat en km².  
Lithographie des circuits intégrés  
À partir du site suivant répondre aux questions ci-dessous :  

Combien de transistors sont intégrés dans les super-puces en 2017 ?  
Quel est l’ordre de grandeur de l’investissement nécessaire pour bâtir une usine qui fabriquera des puces avec une finesse de gravure de 3 nanomètres ?  
Quel autre fabricant, concurrent de Samsung, investi dans une usine capable de graver en 5 nanomètres ?  
 
La famille core i9 d’Intel  
À partir des informations données sur la page du constructeur Intel pour son microprocesseur de la famille i9, répondre aux questions ci-dessous :  

Quelle est la technologie de ce processeur ?  
Donner la définition d’un coeur.  
Que représente la fréquence de base de 3.00 GHz ?  
Que représente la PDT ?  
Quelle est la relation entre la fréquence de base et le PDT ?Quelle est la capacité mémoire maximum possible pour ce processeur ?  
Par quel terme désigne-t-on un fabricant de circuits électroniques ?  
Quelle finesse de gravure la société TSMC (Taiwan) prépare-t-elle dans sa nouvelle usine ?  