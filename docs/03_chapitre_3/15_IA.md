---
author: Votre nom
title: IA
---


# IA

Cette première approche d'un réseau de neurones est basé sur la vidéo suivante :

[![texte alternatif de l'image](http://img.youtube.com/vi/rn1EEd6M7fA/0.jpg){: .center}](https://youtu.be/rn1EEd6M7fA "Titre de la video")

Nous allons écrire un premier programme simulant un réseau de neurones en utilisant
les bibliothèques keras et tensorflow.  
Le but va être de faire apprendre à l'ordinateur à partir de données d'entrée et de sortie pour
qu'il puisse ensuite prédire un résultat !  
Vous allez voir c'est impressionnant ...  

???+ question "Tester le programme"

    {{ IDE('neurones') }}


Bon si vous voulez essayer sur des images pour classer des chats et des chiens :

[Entre chats et chiens](https://thedatafrog.com/fr/articles/dogs-vs-cats/)

Voici le code qui peut fonctionner sous spyder avec ces données
[Donnees chiens et chats](https://nuage04.apps.education.fr/index.php/s/Q2bwzRctYcQTsSR):

???+ question "Tester le programme"

    {{ IDE('neuroneschienschats') }}

![morse](data/neurones.png){ width=80%; : .center }

Pour tester le model :

???+ question "Tester le programme"

    {{ IDE('testmodel') }}

Prochaine étape?  
Programmer un réseau de neurones from Scratch ("en partant de rien") !!!!!