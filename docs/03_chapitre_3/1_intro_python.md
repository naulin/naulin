---
author: Votre nom
title: Structures de données
---

## I. Dictionnaires : 

Comme on l’a vu avec les listes et les tuples, à partir des types de base (int, float, etc.) il est possible d’élaborer de nouveaux types qu’on appelle des types construits.
Un nouvel exemple de type construit est le dictionnaire.
Les éléments d’une liste ou d’un tuple sont ordonnés et on accède à un élément grâce à sa position en utilisant un numéro qu’on appelle l’indice de l’élément.
Un dictionnaire en Python va aussi permettre de rassembler des éléments mais ceux-ci seront identifiés par une clé. On peut faire l’analogie avec un dictionnaire de français où on accède à une définition avec un mot.
Contrairement aux listes qui sont délimitées par des crochets, on utilise des accolades pour les dictionnaires.

???+ question ""

    {{ IDEv('dic1')}}


Un élément a été défini ci-dessus dans le dictionnaire en précisant une clé au moyen d’une chaîne de caractères suivie de : puis de la valeur associée    clé: valeur
On accède à une valeur du dictionnaire en utilisant la clé entourée par des crochets avec la syntaxe suivante : 


???+ question ""

    {{ IDEv('dic2')}}


Comment construire une entrée dans un dictionnaire ?
Il est très facile d’ajouter un élément à un dictionnaire. Il suffit d’affecter une valeur pour la nouvelle clé. 

???+ question ""

    {{ IDEv('dic3')}}


Le type d’un dictionnaire est dict. 

???+ question ""

    {{ IDEv('dic4')}}


Il est aussi possible d’utiliser des valeurs d’autres types.
Voici un exemple où les valeurs sont des entiers. 

???+ question ""

    {{ IDEv('dic5')}}

Comment créer un dictionnaire ?
Nous avons vu ci-dessous qu’il était possible de créer un dictionnaire avec des accolades qui entourent les éléments. Une autre approche possible consiste à créer un dictionnaire vide et à ajouter les éléments au fur et à mesure. 


???+ question ""

    {{ IDEv('dic6')}}



Comment parcourir un dictionnaire ?
On utilise items(). 

Exemple pour une boucle for avec un indice i 


???+ question ""

    {{ IDEv('dic7')}}



Autre exemple pour une boucle for avec deux indices : cle et valeur 

???+ question ""

    {{ IDEv('dic8')}}




