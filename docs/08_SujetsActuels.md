# Sujets d'actualités

kesako?

## Blockchain, 

![morse](https://i.ibb.co/WnpjHsX/blockchain.png){ width=80%; : .center }

Saviez-vous que la chaîne de blocs (blockchain) est une technologie qui permet de garder la trace d'un ensemble de transactions, de manière décentralisée, sécurisée et transparente, sous forme d'une chaîne de blocs ? Vous n’y comprenez toujours rien ? Pas de panique, on vous explique tout !

[Lien vers le site](https://www.economie.gouv.fr/entreprises/blockchain-definition-avantage-utilisation-application)

## Ordinateur quantique,


Les ordinateurs quantiques fonctionnent avec des bits quantiques, ou qubits, qui, contrairement aux bits informatiques standard ayant une valeur de soit 0 soit 1, peuvent être à la fois 0 et 1. Cette caractéristique signifie que les ordinateurs quantiques pourraient être beaucoup plus rapides que les ordinateurs classiques pour de nombreuses tâches. Ils pourraient également être utilisés pour résoudre certains problèmes qu’un ordinateur classique ne peut pas résoudre. 

« Un ordinateur quantique manipulera de nombreux qubits dans un état massivement superposé : 0000 plus 1111, par exemple, explique Landry Bretheau. Dans cet état “intriqué”, plusieurs calculs peuvent être effectués en parallèle. Un exemple concret : imaginez que le calcul, le problème, soit de sortir d’un labyrinthe. Comment s’y prendre ? Un être humain ou un programme informatique va tester différents chemins. À chaque fois, il arrivera à une impasse, puis revendra sur ses pas. Il testera ainsi tous les chemins jusqu’à ce qu’il sorte du labyrinthe. Mais un système quantique peut être dans une superposition d’états, c’est-à-dire qu’il peut se trouver à plusieurs endroits en même temps. Il peut donc essayer d’explorer les différents chemins en parallèle et sortir du labyrinthe plus rapidement. »

[![texte alternatif de l'image](http://img.youtube.com/vi/HZoCqyFyH_c/0.jpg){: .center}](https://youtu.be/HZoCqyFyH_c   "Titre de la video")

[Lien vers le site](https://www.polytechnique-insights.com/tribunes/science/lordinateur-quantique-tout-comprendre-en-15-minutes/#:~:text=%C2%AB%20Un%20ordinateur%20quantique%20manipulera%20de,de%20sortir%20d'un%20labyrinthe.)

## Cybersécurité,

Voici quatre vidéos qui peuvent être présentées partiellement ou totalement aux
élèves pour les sensibiliser à la cybersécurité :

[![texte alternatif de l'image](http://img.youtube.com/vi/s9d50jUXV-w/0.jpg){: .center}](https://www.youtube.com/watch?v=s9d50jUXV-w   "Titre de la video")



● Les combattants numériques, le journal de la défense [14’]
Vidéo montrant la réponse de l’état à la problématique de la cybersécurité.
Présentation de jeunes étudiantes et étudiants de BTS de Saint Cyr ainsi que de
jeunes ingénieurs en informatique femmes ou hommes qui s’engagent dans la
réserve des armées dans le domaine du cyber.

[![texte alternatif de l'image](http://img.youtube.com/vi/JyCIasX5J1c/0.jpg){: .center}](https://www.youtube.com/watch?v=JyCIasX5J1c    "Titre de la video")



● Interview de Fériel Bouakkaz enseignante-chercheuse [12’]
Première femme de France à obtenir l’habilitation CEI permettant d’être
instructrice pour la certification CEH (Certified Ethical Hacker).


[![texte alternatif de l'image](http://img.youtube.com/vi/5BzJSvX6nXA/0.jpg){: .center}](https://www.youtube.com/watch?v=5BzJSvX6nXA    "Titre de la video")


 

● La dictature des algorithmes [6’]
L’algorithme fait office de boite noire et les résultats produits d’oracle divin. Ne
faut-il pas se pencher sur la validité des résultats produits? Cathy O’Neil donne
un éclairage sur ce sujet.

## IA,

![morse](https://i.ibb.co/PYxzkq8/IA.jpg){ width=50%; : .center }

L'IA, c'est quoi ?

Une définition complexe à opérer et à partager.  
Si le terme d’« intelligence artificielle » (IA) est entré dans le langage commun et son utilisation devenue banale dans les médias, il n’en existe pas réellement de définition partagée.  

Au sens large, le terme désigne en effet indistinctement des systèmes qui sont du domaine de la pure science-fiction (les IA dites « fortes », dotées d’une forme conscience d’elles-mêmes) et des systèmes déjà opérationnels en capacité d’exécuter des tâches très complexes (reconnaissance de visage, voix, conduite de véhicule, ces systèmes sont qualifiés d’IA « faibles » ou « modérées »).  


[Lien vers le site](https://www.coe.int/fr/web/artificial-intelligence/what-is-ai)



## Deep learning,

Les concepts clés : IA, Machine Learning, et Deep Learning
Depuis quelques années, un nouveau lexique lié à l’émergence de l’intelligence artificielle dans notre société inonde les articles scientifiques, et il est parfois difficile de comprendre de quoi il s’agit. Lorsqu’on parle d’intelligence artificielle, on fait très souvent l’allusion aux technologies associées comme le Machine learning ou le Deep learning. Deux termes extrêmement utilisés avec des applications toujours plus nombreuses, mais pas toujours bien définis. 


![morse](https://i.ibb.co/yyfRXyp/OIP.jpg){ width=50%; : .center }

[Lien vers le site](https://datascientest.com/deep-learning-definition)

## Big Data

Avant de définir le Big Data, ou les mégadonnées, il est important de bien comprendre ce que sont les données. Ce terme définit les quantités, les caractères ou les symboles sur lesquels des opérations sont effectuées par un ordinateur. Les données peuvent être stockées ou transmises sous forme de signaux électriques et enregistrées sur un support mécanique, optique ou magnétique.

![morse](https://i.ibb.co/6PL78fW/BigData.png){ width=50%; : .center }


[Lien vers le site](https://datascientest.com/big-data-tout-savoir)

## 5G

La « 5G » est la cinquième génération de réseaux mobiles, qui succède aux technologies 2G, 3G et 4G.

Les premières technologies ne permettaient que les appels vocaux puis l’envoi de SMS. Les générations suivantes de technologies mobiles ont permis de développer de nouveaux usages : se connecter à internet, accéder à des applications, ou encore passer des appels en vidéo.

![morse](https://i.ibb.co/xLn1Jh1/5g.png){ width=50%; : .center }

[Lien vers le site](https://arcep.fr/nos-sujets/parlons-5g-toutes-vos-questions-sur-la-5g.html)