# Actions

# Activités pour les Élèves de NSI

# Événements NSI

Bienvenue sur la page dédiée aux événements de la NSI (Numérique et Sciences Informatiques). Cette section vous présente quelques-unes des activités passionnantes que nous organisons pour promouvoir la science informatique et encourager la participation des élèves.

## 1. [Semaine de la NSI](https://www.semaine-nsi.fr/)

| | |
|----------------------|-------------------------------|
| La **Semaine de la NSI** est un événement annuel qui met en avant la science informatique à travers une série d'ateliers, de conférences et de discussions. Pendant cette semaine, les élèves ont l'opportunité d'explorer divers domaines de l'informatique, d'interagir avec des professionnels du secteur et de participer à des compétitions stimulantes.| ![Image La nuit du code](./images/NSIWeek.png) |



## 2. [La nuit du code](https://www.nuitducode.net/ndc2023)

| | |
|----------------------|-------------------------------|
| La **Nuit du Code** est une nuit entière dédiée à la programmation, au partage d'idées et à la création. Les élèves se rassemblent pour relever des défis de codage, collaborer sur des projets et apprendre les uns des autres. C'est une occasion unique de plonger dans le monde de l'informatique de manière immersive et créative.| ![Image La nuit du code](./images/NDC.png) |


## 3. [Les Trophées NSI](https://trophees-nsi.fr/)

| | |
|----------------------|-------------------------------|
| Les **Trophées NSI** sont une célébration des réalisations exceptionnelles dans le domaine de la science informatique. Chaque année, nous récompensons les élèves qui ont démontré un engagement exceptionnel, une créativité remarquable et une maîtrise remarquable des concepts informatiques. Cela inspire et encourage les autres élèves à poursuivre l'excellence dans leur parcours informatique.| ![Image La nuit du code](./images/trophees.png) |




## 4. [Une classe, Un scientifique](https://chiche-snt.fr/1-scientifique/)

| | |
|----------------------|-------------------------------|
| "Une classe, Un scientifique" est une initiative visant à rapprocher les élèves de la NSI des professionnels de l'informatique et des scientifiques. Des experts du domaine sont invités à partager leurs expériences, à discuter des avancées récentes et à répondre aux questions des élèves. Cela offre une opportunité précieuse pour les élèves de mieux comprendre les diverses possibilités qu'offre la science informatique. Nous sommes ravis de vous présenter ces événements qui font partie intégrante de notre engagement à promouvoir la science informatique et à inspirer la prochaine génération de scientifiques et d'ingénieurs. Rejoignez-nous lors de nos prochains événements et plongez dans le passionnant monde de la NSI ! Pour plus d'informations et pour rester à jour avec nos événements.| ![Image La nuit du code](./images/classescience.png) |


## 5. [Pass ton Hack d'abord](https://fr.linkedin.com/pulse/passe-ton-hack-dabord-le-challenge-qui)


| | |
|----------------------|-------------------------------|
| « Passe ton hack d’abord » passe à l’échelle nationale dès la rentrée prochaine. Cette première expérimentation en région parisienne est un franc succès. Elle témoigne de la montée en puissance des enjeux de cybersécurité et de cyberdéfense. Fort de ces résultats et de l’intérêt témoigné par l’ensemble des participants, la prochaine édition du challenge sera étendue sur le territoire dès la rentrée scolaire. Les participants vous donnent rendez-vous en 2024 pour une nouvelle aventure cyber !| ![Image La nuit du code](./images/pass.jpg) |


[![texte alternatif de l'image](http://img.youtube.com/vi/nhd6KaLgVxM/0.jpg){: .center}](https://youtu.be/nhd6KaLgVxM    "Titre de la video")

[![texte alternatif de l'image](http://img.youtube.com/vi/RNrGA9mzyl4/0.jpg){: .center}](https://youtu.be/RNrGA9mzyl4   "Titre de la video")
