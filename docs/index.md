

# SNT/NSI

😊 Ce site est en construction, vous retrouverez tous les fichiers utilisés pendant les cours.

![morse](images/stranger.png){ width=20%; : .center }

Vous trouverez ici un lien vers le [Programme SNT](https://eduscol.education.fr/document/23494/download){ .md-button target="_blank" rel="noopener" } et un lien vers un [Programme NSI](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g){ .md-button target="_blank" rel="noopener" }.

Ce site présentera le cours de SNT pour les secondes et de NSI pour les premières, terminales :

* chapitre 1 : Cours de SNT
* chapitre 2 : Cours de NSI première
* chapitre 3 : Cours de NSI terminale
* Des exemples de projets à travailler en NSI
* Des liens vers des actions qui serons menées au cours de l'année en lien avec la NSI


L’enseignement de spécialité de numérique et sciences informatiques du cycle terminal de la
voie générale vise l’appropriation des fondements de l’informatique pour préparer les élèves
à une poursuite d’études dans l’enseignement supérieur, en les formant à la pratique d’une
démarche scientifique et en développant leur appétence pour des activités de recherche.

Un enseignement d’informatique ne saurait se réduire à une présentation de concepts ou de
méthodes sans permettre aux élèves de se les approprier en développant des projets
applicatifs.
Une part de l’horaire de l’enseignement d’au moins un quart du total en classe de première
doit être réservée à la conception et à l’élaboration de projets conduits par des groupes de
deux à quatre élèves.





